import {useState, useEffect} from 'react';
import toast, { Toaster } from 'react-hot-toast';

// use custom fetch -- may save time
// e.g. 
// 1) import useFetch from './useFetch';
// 2) var result = useFetch("https:\\localhost:7198\..", "GET", "application/json");

const useFetch = ({uri, methodType, contentType, body=undefined, authType=undefined, credentials=undefined, timeout=0}) => {
    const [data, setData] = useState([]);
    const [isPending, setIsPending] = useState(true);
    const [error, setError] = useState("");

    // default
    let headers = { 'content-type': contentType }
    let requestInit = { method: methodType, headers: headers}

    // body
    if(body != null || body != undefined) {
        requestInit = { method: methodType, headers: headers, body: JSON.stringify(body)}
    }

    // auth.
    if(authType == "Basic"){
        headers = { 'content-type': contentType, 'Authorization': 'Basic ' + btoa(credentials.username + ":" + credentials.password)}
    }

    useEffect(
        setTimeout(() => {
            fetch(uri, requestInit)
            .then(res => {
                if(!res.ok) {
                    throw Error("Could not fetch data from resource");
                }
                return res.json();
            })
            .then(data => {
                console.log(data); 
                setIsPending(false);
                setData(data);
            })
            .catch(err => {
                console.log(err);
                setError(err);
                setIsPending(false);
                toast.error("There was an error fetching data form resource", {
                    duration: 3000,
                    position: 'bottom-right'
                })
            })
        }, timeout)
    , [uri]);
    return(data, isPending, error);
}
 
export default useFetch;