﻿namespace video_meeting_rating_tool_api.Exceptions
{
    public class UnableToUploadDocumentException : Exception
    {
        public UnableToUploadDocumentException() { }
        public UnableToUploadDocumentException(string message) : base(message) { }
        public UnableToUploadDocumentException(string message, Exception innerException) : base(message, innerException) { }
    }
}
