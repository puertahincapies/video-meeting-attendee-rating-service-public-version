import '../css/quizresults.css';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import toast, { Toaster } from 'react-hot-toast';
import Cookies from 'universal-cookie';
import {USERSESSION, GETQUIZRESULTURL, GETQUIZURL, NAVTOUNAUTHORIZE} from '../helpers/constants';
import { useLocation } from 'react-router-dom';

const QuizResults = ({userSession}) => {
    const [quizResult, setQuizResult] = useState([]);
    const [quiz, setQuiz] = useState([]);
    const navigate = useNavigate();
    const cookies = new Cookies();
    const [currentUserId, setCurrentUserId] = useState(0);
    const [isAnswersTableVisible, setIsAnswersTableVisible] = useState(true);
    const location = useLocation();

    useEffect(() => {
        let user = userSession;
        if(user == null || user == undefined) {
            user = cookies.get(USERSESSION);
            if(user != null || user != undefined){
                setCurrentUserId(user.userId);
            }
        }
        else {
            setCurrentUserId(user.userId);
        }
    
        if(user != null && user != undefined){
            fetch(GETQUIZRESULTURL + "/" + location.state.quizId + "/" + user.userId, {
                method: "GET",
                headers: { 'content-type': 'application/json'},
            })
            .then(res => {
                if(!res.ok) {
                    throw Error("Could not retrieve data from resource");
                }
                return res.json();
            })
            .then(data => {
                // console.log(data); 
                setQuizResult(data);
            })
            .catch(err => {
                console.log(err);
                toast.error("There was an error!", {
                    duration: 3000,
                    position: 'bottom-right'
                })
            })
        }
        else {
            navigate(NAVTOUNAUTHORIZE);
        }
    }, []);

    const handleShowCorrectAnswers = (e) => {

        if(isAnswersTableVisible) {
            setIsAnswersTableVisible(false);
        }
        else {
            setIsAnswersTableVisible(true);
        }

        fetch(GETQUIZURL + location.state.quizId, {
            method: "GET",
            headers: { 'content-type': 'application/json'},
        })
        .then(res => {
            if(!res.ok) {
                throw Error("Could not retrieve data from resource");
            }
            return res.json();
        })
        .then(data => {
            console.log(data); 
            setQuiz(data);
        })
        .catch(err => {
            console.log(err);
            toast.error("There was an error!", {
                duration: 3000,
                position: 'bottom-right'
            })
        })
    }

    return ( 
        <div className="quiz-result-component">
            <label className="component-header">Quiz Result</label>
                {currentUserId != 0 && quizResult != null && 
                    <div>
                        <table>
                            <tbody>
                                <tr>
                                    <th>QuizResultID</th>
                                    <td>{quizResult.quizResultId}</td>
                                </tr>
                                <tr>
                                    <th>QuizId</th>
                                    <td>{quizResult.quizId}</td>
                                </tr>
                                <tr>
                                    <th>MeetingId</th>
                                    <td>{quizResult.meetingId}</td>
                                </tr>
                                <tr>
                                    <th>UserId</th>
                                    <td>{quizResult.userId}</td>
                                </tr>
                                <tr>
                                    <th>Role</th>
                                    <td>{quizResult.role}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{quizResult.email}</td>
                                </tr>
                                <tr>
                                    <th>Question #1 Response</th>
                                    <td>{quizResult.question1Response}</td>
                                </tr>
                                <tr>
                                    <th>Question #2 Response</th>
                                    <td>{quizResult.question2Response}</td>
                                </tr>
                                <tr>
                                    <th>Question #3 Response</th>
                                    <td>{quizResult.question3Response}</td>
                                </tr>
                                <tr>
                                    <th>Question #4 Response</th>
                                    <td>{quizResult.question4Response}</td>
                                </tr>
                                <tr>
                                    <th>Question #5 Response</th>
                                    <td>{quizResult.question5Response}</td>
                                </tr>
                                <tr>
                                    <th>Score</th>
                                    <td>{quizResult.score * 20}%</td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>{quizResult.status}</td>
                                </tr>
                            </tbody>
                        </table><br />
                        <button type="button" className="action-button" onClick={handleShowCorrectAnswers}>Show Correct Answers</button><br /><br />
                        <div className="correct-answers-section" hidden={isAnswersTableVisible}>
                            <label className="component-header">Correct Answers</label>    
                            <table hidden={isAnswersTableVisible}> 
                                <tbody>
                                    <tr>
                                        <th>Question #1 Answer</th>
                                        <td>{quiz.question1CorrectAnswer}</td>
                                    </tr>
                                    <tr>
                                        <th>Question #2 Answer</th>
                                        <td>{quiz.question2CorrectAnswer}</td>
                                    </tr>
                                    <tr>
                                        <th>Question #3 Answer</th>
                                        <td>{quiz.question3CorrectAnswer}</td>
                                    </tr>
                                    <tr>
                                        <th>Question #4 Answer</th>
                                        <td>{quiz.question4CorrectAnswer}</td>
                                    </tr>
                                    <tr>
                                        <th>Question #5 Answer</th>
                                        <td>{quiz.question5CorrectAnswer}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                }
                <Toaster />
        </div>
     );
}
 
export default QuizResults;
