﻿using System.ComponentModel.DataAnnotations.Schema;

namespace video_meeting_rating_tool_api.Models.Interfaces
{
    public interface IMeeting
    {
        public int MeetingId { get; set; }
        public string? MeetingUUID { get; set; }
        public string MeetingName { get; set; }
        public DateTime MeetingDateTime { get; set; }
        public int UserId { get; set; }
        public string? MeetingAttendeeEmails { get; set; }
        public string UUID { get; set; } 
        public DateTime CreateDateTime { get; set; }
    }
}
