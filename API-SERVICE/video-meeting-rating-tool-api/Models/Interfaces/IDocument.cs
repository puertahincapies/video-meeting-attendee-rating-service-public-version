﻿namespace video_meeting_rating_tool_api.Models.Interfaces
{
    public interface IDocument
    {
        public int DocumentId { get; set; }
        public int MeetingId { get; set; }
        public string Name { get; set; }
        public byte[] Content { get; set; }
        public string UUID { get; set; } 
        public DateTime CreateDateTime { get; set; }
    }
}
