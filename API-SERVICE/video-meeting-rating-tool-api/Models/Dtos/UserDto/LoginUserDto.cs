﻿using System.ComponentModel.DataAnnotations;

namespace video_meeting_rating_tool_api.Models.Dtos.UserDto
{
    public class LoginUserDto
    {
        //public string Email { get; set; }

        //public string Password { get; set; }
        [Required]
        public string Token { get; set; }
    }
}
