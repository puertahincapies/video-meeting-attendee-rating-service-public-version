using Microsoft.AspNetCore.Http;
using System.IO;
using video_meeting_rating_tool_api.Models;
using video_meeting_rating_tool_api.Models.Dtos.DocumentDto;
using video_meeting_rating_tool_automation.Helpers;

namespace video_meeting_rating_tool_automation
{
    [TestFixture]
    public class DocumentControllerTests
    {
        Settings settings;
        DocumentApiHelper api;

        [OneTimeSetUp] 
        public void OneTimeSetUp() 
        {
            settings = new Settings();
            api = new DocumentApiHelper(settings);
        }

        [SetUp]
        public void Setup(){ /*Do nothing*/ }

        [TearDown] 
        public void TearDown() { /*Do nothing*/ }

        [OneTimeTearDown]
        public void OneTimeTearDown() { /*Do nothing*/ }

        [Test]
        public void Document_UploadDocument()
        {
            try
            {
                // arrange
                var guid = Guid.NewGuid();
                var name = $"Sample File {guid}";
                var meetingId = new Random().Next(999);

                // action
                var result = api.UploadDocument(name, meetingId);

                // assert
                Assert.That(name == result.Document.Name, "Document names do not match");
                Assert.That(meetingId == result.Document.MeetingId, "MeetingIds do not match");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void Document_DeleteAllDocumentsFromServer()
        {
            Assert.Ignore();
        }

        [Test]
        public void Document_DeleteAllDocumentsFromDatabase()
        {
            Assert.Ignore();
        }

        [Test]
        public void Document_GetDocument()
        {
            Assert.Ignore();
        }

        [Test]
        public void Document_GetDocumentList()
        {
            Assert.Ignore();
        }

        [Test]
        public void Document_UpdateDocument()
        {
            Assert.Ignore();
        }

        [Test]
        public void Document_GetTranscriptDocumentFromZoomCloud()
        {
            Assert.Ignore();
        }
    }
}