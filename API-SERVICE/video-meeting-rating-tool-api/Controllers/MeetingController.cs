﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using video_meeting_rating_tool_api.Exceptions;
using video_meeting_rating_tool_api.Helpers;
using video_meeting_rating_tool_api.Models;
using video_meeting_rating_tool_api.Models.Dtos.MeetingAttendeeEmailDto;
using video_meeting_rating_tool_api.Models.Dtos.MeetingDto;
using video_meeting_rating_tool_api.Repository;

namespace video_meeting_rating_tool_api.Controllers
{
    [ApiController]
    [EnableCors("MeetingQuest_CorsPolicy")]
    [Route("api/v1/meetingquest/[controller]")]
    public class MeetingController : ControllerBase
    {
        private readonly ILogger<MeetingController> _logger;
        private readonly IMeetingQuestRepository _repository;
        private readonly IConfiguration _configuration;
        private AwsCloudWatchClient _cloudWatchClient;

        // using repository pattern
        public MeetingController(ILogger<MeetingController> logger, IMeetingQuestRepository repository, IConfiguration configuration)
        {
            _logger = logger;
            _repository = repository;
            _configuration = configuration;
            _cloudWatchClient = new AwsCloudWatchClient(_configuration);
        }

        /// <summary>
        /// API to store meeting details
        /// </summary>
        /// <param name="addMeeting"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<ActionResult<MeetingDto>> SaveMeetingDetails([FromBody]AddMeetingDto addMeeting)
        {
            try
            {
                if (addMeeting == null)
                {
                    return BadRequest("No meeting details provided");
                }

                // check if meeting already exists
                var existingMeetings = _repository.GetMeetingsByUser(addMeeting.UserId);
                foreach(var meeting in existingMeetings)
                {
                    if(meeting.MeetingUUID == addMeeting.MeetingUUID && meeting.MeetingName == addMeeting.MeetingName && meeting.MeetingDateTime == addMeeting.MeetingDateTime)
                    {
                        return Conflict(new SafeResponse() { Message = "This meeting already exists" } );
                    }
                }

                var newMeeting = new Meeting()
                {
                    MeetingUUID = addMeeting.MeetingUUID,
                    MeetingDateTime = addMeeting.MeetingDateTime,
                    MeetingName = addMeeting.MeetingName,
                    MeetingAttendeeEmails = addMeeting.MeetingAttendeeEmails,
                    UserId = addMeeting.UserId
                };

                var addedMeeting = _repository.AddMeeting(newMeeting);

                // add emails to attendee email table
                if (!string.IsNullOrEmpty(addMeeting.MeetingAttendeeEmails))
                {
                    AddMeetingAttendeeEmails(addMeeting.MeetingAttendeeEmails, addMeeting.UserId);
                }

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"Meeting was successfully saved. Meeting: {JsonConvert.SerializeObject(addedMeeting)}");

                return CreatedAtAction(nameof(GetMeeting), new { meetingId = newMeeting.MeetingId }, newMeeting.AsDto());
            }
            catch(UnableToSaveMeetingException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Conflict(new SafeResponse() { Message = "Unable to save meeting details" });
            }
            catch(Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        [HttpGet("[action]/{meetingId}")]
        public async Task<ActionResult<MeetingDto>> GetMeeting(int meetingId)
        {
            try
            {
                if (meetingId == 0)
                {
                    return BadRequest("MeetingId not provided");
                }

                var meeting = _repository.GetMeeting(meetingId);
                if (meeting == null)
                {
                    return NotFound("Meeting not found");
                }

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"Meeting was successfully retrieved. Meeting: {JsonConvert.SerializeObject(meeting)}");
                return Ok(meeting.AsDto());
            }
            catch(MeetingNotFoundException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return NotFound(new SafeResponse() { Message = "Meeting not found" });
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<IEnumerable<MeetingDto>>> GetMeetings()
        {
            try
            {
                var meetings = _repository.GetMeetings().Select(x => x.AsDto());
                if (meetings == null)
                {
                    return NotFound("Meeting not found");
                }

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"{meetings?.Count()} Meetings were successfully retrieved.");
                return Ok(meetings);
            }
            catch (MeetingNotFoundException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return NotFound(new SafeResponse() { Message = "Meetings not found"});
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        [HttpGet("[action]/{userId}")]
        public async Task<ActionResult<IEnumerable<MeetingAttendeeEmailDto>>> GetAttendeeEmails(int userId)
        {
            try
            {
                var emails = _repository.GetMeetingAttendeeEmails(userId).Select(x => x.AsDto());
                if (emails == null)
                {
                    return NotFound("No meetingss found");
                }

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"{emails?.Count()} attendee emails were successfully retrieved");
                return Ok(emails);
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        // Helpers
        private void AddMeetingAttendeeEmails(string meetingAttendeeEmails, int userId)
        {
            try
            {
                // other validations
                char[] delimiters = { ',', ';' };

                var emails = meetingAttendeeEmails.Replace(",",";").Split(delimiters).ToList();
                if (string.IsNullOrEmpty(emails.LastOrDefault()))
                {
                    emails.RemoveAt(emails.Count - 1);
                }

                foreach (var email in emails)
                {
                    if (!email.Contains("@")) continue;
                    var emailEntry = _repository.GetMeetingAttendeeEmail(email.Trim(), userId);
                    if (emailEntry == null)
                    {
                        var meetingAttendeeEmail = new MeetingAttendeeEmail()
                        {
                            UserId = userId,
                            AttendeeEmail = email
                        };
                        _repository.AddMeetingAttendeeEmail(meetingAttendeeEmail);
                    }
                }
            }
            catch(Exception ex)
            {
                throw;
            }
        }
    }
}
