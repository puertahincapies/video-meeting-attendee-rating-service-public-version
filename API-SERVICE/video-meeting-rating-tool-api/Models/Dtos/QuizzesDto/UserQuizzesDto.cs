﻿namespace video_meeting_rating_tool_api.Models.Dtos.QuizzesDto
{
    public class UserQuizzesDto
    {
        public int UserQuizId { get; set; }
        public int UserId { get; set; }
        public int QuizId { get; set; }
        public string UUID { get; set; }
        public DateTime CreateDateTime { get; set; }
    }
}
