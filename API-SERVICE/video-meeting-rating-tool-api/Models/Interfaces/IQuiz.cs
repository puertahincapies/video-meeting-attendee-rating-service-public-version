﻿using System.ComponentModel.DataAnnotations;

namespace video_meeting_rating_tool_api.Models.Interfaces
{
    public interface IQuiz
    {
        public int QuizId { get; set; }
        public int DocumentId { get; set; }
        public int MeetingId { get; set; }
        public int UserId { get; set; }
        public string Question1 { get; set; }
        public string Question2 { get; set; }
        public string Question3 { get; set; }
        public string Question4 { get; set; }
        public string Question5 { get; set; }
        public string Question1Answer1 { get; set; }
        public string Question1Answer2 { get; set; }
        public string Question1Answer3 { get; set; }
        public string Question2Answer1 { get; set; }
        public string Question2Answer2 { get; set; }
        public string Question2Answer3 { get; set; }
        public string Question3Answer1 { get; set; }
        public string Question3Answer2 { get; set; }
        public string Question3Answer3 { get; set; }
        public string Question4Answer1 { get; set; }
        public string Question4Answer2 { get; set; }
        public string Question4Answer3 { get; set; }
        public string Question5Answer1 { get; set; }
        public string Question5Answer2 { get; set; }
        public string Question5Answer3 { get; set; }
        public string Question1CorrectAnswer { get; set; }
        public string Question2CorrectAnswer { get; set; }
        public string Question3CorrectAnswer { get; set; }
        public string Question4CorrectAnswer { get; set; }
        public string Question5CorrectAnswer { get; set; }
        public string UUID { get; set; } 
        public DateTime CreateDateTime { get; set; }
    }
}
