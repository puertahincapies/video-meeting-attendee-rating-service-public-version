﻿using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Transfer;
using video_meeting_rating_tool_api.Models.Interfaces;

namespace video_meeting_rating_tool_api.Models
{
    public class StorageService : IStorageService
    {
        public async Task<S3Response> UploadFileAsync(S3Object s3Object, AwsCredentials awsCredentials)
        {
            var credentials = new BasicAWSCredentials(awsCredentials.AccessKey, awsCredentials.SecretKey);

            var config = new AmazonS3Config()
            {
                RegionEndpoint = Amazon.RegionEndpoint.USEast1
            };

            var response = new S3Response();
            try
            {
                var uploadRequest = new TransferUtilityUploadRequest()
                {
                    InputStream = s3Object.InputStream,
                    Key = s3Object.Name,
                    BucketName = s3Object.BucketName,
                    CannedACL = S3CannedACL.NoACL
                };

                using var client = new AmazonS3Client(credentials, config);

                var transferUtility = new TransferUtility(client);

                await transferUtility.UploadAsync(uploadRequest);

                response.StatusCode = 200;
                response.Message = "File has been successfully uploaded";
            }
            catch(AmazonS3Exception ex)
            {
                response.StatusCode = (int)response.StatusCode;
                response.Message = response.Message;
            }
            catch(Exception ex)
            {
                response.StatusCode = 500;
                response.Message = response.Message;
            }
            return response;
        }
    }
}
