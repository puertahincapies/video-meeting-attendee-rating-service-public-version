﻿namespace video_meeting_rating_tool_api.Enums
{
    public enum ErrorTypes
    {
        Information = 0,
        Warning = 1,
        Error = 2
    }
}
