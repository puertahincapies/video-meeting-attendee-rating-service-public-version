﻿using Newtonsoft.Json;
using video_meeting_rating_tool_api.Models;
using video_meeting_rating_tool_automation.Helpers.ApiHelpers;

namespace video_meeting_rating_tool_automation.Helpers
{
    public class DocumentApiHelper : HttpBase
    {
        private readonly Settings _settings;

        private const string path = "/api/v1/meetingquest/Document";
        private string uploadDocument = $"{path}/UploadDocument";
        private string deleteAllDocumentsFromServer = $"{path}/DeleteAllDocumentsFromServer";
        private string deleteALlDocumentsFromDatabase = $"{path}/DeleteAllDocumentsFromDatabase";
        private string getDocument = $"{path}/GetDocument/{0}";
        private string getDocumentList = $"{path}/GetDocumentList";
        private string updateDocument = $"{path}/UpdateDocument";
        private string getTranscriptDOcumentFromZoomCloud = $"{path}/GetTranscriptDocumentFromZoomCloud/{0}/{1}";

        public DocumentApiHelper(Settings settings)
        {
            _settings = settings;
        }

        public DocumentResponse UploadDocument(string name, int meetingId)
        {
            MultipartFormDataContent multipartContent = new MultipartFormDataContent();
            var nameContent = new StringContent(name);
            var meetingIdContent = new StringContent(meetingId.ToString());

            string filePath = Path.Combine("../../../Helpers/TestFiles", "test.txt");
            using var stream = File.OpenRead(filePath);
            var fileContent = new StreamContent(stream);

            multipartContent.Add(nameContent, "Name");
            multipartContent.Add(meetingIdContent, "MeetingId");
            multipartContent.Add(fileContent, "Content", "test.txt");

            var response = HttpPostAsync(baseUri: _settings.WebApiBaseUri, requestUri: uploadDocument, contentType: "multipart/form-data", content: multipartContent).Result;
            var responseContent = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<DocumentResponse>(responseContent);
        }

        public void DeleteAllDocumentsFromServer()
        {
            
        }

        public void DeleteAllDocumentsFromDatabase()
        {

        }

        public Document GetDocument(int documentId)
        {
            return null;
        }

        public List<Document> GetDocumentList()
        {
            return null;
        }

        public Document UpdateDocument(Document document)
        {
            return null;
        }

        public Document GetDocumentFromZoomCloud(string clientKey, string secretKey)
        {
            return null;
        }
    }

    public class DocumentResponse
    {
        public string Message { get; set; }
        public Document Document { get; set; }
    }
}
