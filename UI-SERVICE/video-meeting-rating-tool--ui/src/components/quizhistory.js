import '../css/quizhistory.css';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import toast, { Toaster } from 'react-hot-toast';
import Cookies from 'universal-cookie';
import {USERSESSION, GETUSERQUIZHISTORYURL, NAVTOQUIZRESULT, NAVTOUNAUTHORIZE} from '../helpers/constants';

const QuizHistory = ({userSession}) => {
  const [quizzes, setQuizzes] = useState([]);
  const navigate = useNavigate();
  const cookies = new Cookies();
  const [currentUserId, setCurrentUserId] = useState(0);

  useEffect(() => {
    let user = userSession;
    if(user == null || user == undefined) {
        user = cookies.get(USERSESSION);
        if(user != null || user != undefined){
            setCurrentUserId(user.userId);
        }
    }
    else {
        setCurrentUserId(user.userId);
    }

    if(user != null && user != undefined){
        fetch(GETUSERQUIZHISTORYURL + "/" + user.userId, {
            method: "GET",
            headers: { 'content-type': 'application/json'},
        })
        .then(res => {
            if(!res.ok) {
                throw Error("Could not retrieve data from resource");
            }
            return res.json();
        })
        .then(data => {
            // console.log(data); 
            setQuizzes(data);
        })
        .catch(err => {
            console.log(err);
            toast.error("There was an error!", {
                duration: 3000,
                position: 'bottom-right'
            })
        })
    }
    else {
        navigate(NAVTOUNAUTHORIZE);
    }
}, []);

const handleOnQuizRowClick = (quiz) => {
  navigate(NAVTOQUIZRESULT, { state: { quizId: quiz.quizId } });
}

const updateQuizDate = (quiz) => {
    var arr = quiz.createDateTime.toString().split('T');
    arr.pop();
    // console.log(arr);
    return arr[0];
}

  return ( 
    <div className="quiz-history-component">
      <label className="component-header">Quiz History</label>
        {currentUserId != 0 && quizzes != null && <table>
            <thead>
                <tr>
                    <th>MeetingId</th>
                    <th>QuizId</th>
                    <th>Date Submitted</th>
                </tr>
            </thead>
            <tbody>
                {quizzes.map((quiz) => (
                    <tr key={quiz.quizId} className="quiz-row" onClick={() => handleOnQuizRowClick(quiz)}>
                        <td>{quiz.meetingId}</td>
                        <td>{quiz.quizId}</td>
                        <td>{updateQuizDate(quiz)}</td>
                    </tr>
                ))}
            </tbody>
        </table>}
        <Toaster />
    </div>
  );
      
}
 
export default QuizHistory;
