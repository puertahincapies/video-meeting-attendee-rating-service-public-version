﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using video_meeting_rating_tool_api.Models.Interfaces;

namespace video_meeting_rating_tool_api.Models
{
    [Table("Documents")]
    public class Document : IDocument
    {
        [Key]
        public int DocumentId { get; set; }
        public int MeetingId { get; set; }
        public string Name { get; set; }
        public byte[] Content { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();
        public DateTime CreateDateTime { get; set; } = DateTime.Now;
    }
}
