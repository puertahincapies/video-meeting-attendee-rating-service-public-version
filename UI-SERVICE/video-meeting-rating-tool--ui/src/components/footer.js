import '../css/footer.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebook, faLinkedin } from '@fortawesome/free-brands-svg-icons';

const Footer = () => {
    return ( 
        <div className="footer-component">
            <section className="footer-content">
                <p className="footer-content-copyright">© 2023 MeetingQuest</p>
            </section>
            <section className="social-media-links">
                <button className='facebook'>
                <FontAwesomeIcon icon={faFacebook} />
                </button>
                <button className='linkedin'>
                <FontAwesomeIcon icon={faLinkedin} />
                </button>
            </section>

        </div>
     );
}
 
export default Footer;