import '../index.css';
import '../css/header.css';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHouse, faUpload, faBoxArchive, faUser, faAddressCard, faRightToBracket, faUserPlus, faRightFromBracket, faPencil } from '@fortawesome/free-solid-svg-icons';
import { useNavigate } from 'react-router-dom';
import toast, { Toaster } from 'react-hot-toast';
import { useState, useEffect } from 'react';
import Cookies from 'universal-cookie';
import {USERSESSION,USERLOGIN} from '../helpers/constants';

const Header = ({userSession, setUserSession}) => {
    const [isSessionActive, setIsSessionActive] = useState(false);
    const [userName, setUserName] = useState("");
    const navigate = useNavigate();
    const cookies = new Cookies();

    useEffect(() => {
        let user = userSession;
        // console.log("header > user: " + JSON.stringify(user));
        if(user == null || user == undefined) {
            user = cookies.get(USERSESSION);
            // console.log("header > set user using cookie session: " + JSON.stringify(user));
            if(user != null || user != undefined){
                setIsSessionActive(true);
                setUserName(user.name);
                setUserSession(user);
            }
        }
        else {
            setIsSessionActive(true);
            setUserName(user.name);
        }
    })

    const OnClickLogout = (e) => {
        console.log("Logging out...");
        setUserSession(null);
        cookies.remove(USERSESSION, { path: "/"});
        cookies.remove(USERLOGIN, { path: "/"});
        setIsSessionActive(false);
        toast.success("You have successfully logged off", {
            duration: 3000,
            position: 'bottom-right'
        });
        navigate('/meetingquest/login');
    }

    return ( 
        <div className="header-component">
            <section className="header-main header-item">
                <div className="header-title">
                    <Link to="/meetingquest/home" className="header-title-content">Meeting<u>Quest</u></Link>
                </div>    
                <div className="header-navbar-links">
                    <Link to="/meetingquest/home" className="navbar-link-item">Home <FontAwesomeIcon icon={faHouse} /></Link>
                    <Link to="/meetingquest/upload" className="navbar-link-item">Upload <FontAwesomeIcon icon={faUpload} /></Link>
                    {isSessionActive && <Link to="/meetingquest/quizzes" className="navbar-link-item quizzes-link"> Quizzes <FontAwesomeIcon icon={faPencil} /></Link>}
                    {isSessionActive && <Link to="/meetingquest/quizhistory" className="navbar-link-item">Quiz History <FontAwesomeIcon icon={faBoxArchive} /></Link>}
                    {isSessionActive && <Link to="/meetingquest/report" className="navbar-link-item">Report <FontAwesomeIcon icon={faAddressCard} /></Link>}
                    {isSessionActive && <Link to="/meetingquest/useraccount" className="navbar-link-item">User Account <FontAwesomeIcon icon={faUser} /></Link> }
                    <Link to="/meetingquest/aboutus" className="navbar-link-item">About Us <FontAwesomeIcon icon={faAddressCard} /></Link>
                </div>
            </section>
            <section className="header-portal header-item">
                    {!isSessionActive && <Link to="/meetingquest/login" className="action-button">Login <FontAwesomeIcon icon={faRightToBracket} /></Link>}
                    {!isSessionActive && <Link to="/meetingquest/signup" className="action-button">Sign Up <FontAwesomeIcon icon={faUserPlus} /></Link>}
                    {isSessionActive && <label className="btn btn-primary">{userName}</label>}   
                    {isSessionActive && <button type="button" className="action-button" onClick={OnClickLogout}>Logout <FontAwesomeIcon icon={faRightFromBracket} /></button>}          
            </section>
            <Toaster />
        </div>
     );
}
 
export default Header;