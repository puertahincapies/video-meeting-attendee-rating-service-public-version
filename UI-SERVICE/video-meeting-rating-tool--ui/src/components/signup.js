import '../css/signup.css';
import { useState } from 'react';
import toast, { Toaster } from 'react-hot-toast';
import { useNavigate } from 'react-router-dom';
import {NAVTOLOGIN, SIGNUPURL} from '../helpers/constants';

const Signup = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');    
    const [confirmPassword, setConfirmPassword] = useState('');
    const [name, setName] = useState('');
    const [virtualMeetingProvider, setVirtualMeetingProvider] = useState('');
    const [clientKey, setClientKey] = useState('');    
    const [secretKey, setSecretKey] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    const navigate = useNavigate();

    const handleEmailChange = (e) => {
        setEmail(e.target.value);
    };

    const handlePasswordChange = (e) => {
        setPassword(e.target.value);
    };

    const handleConfirmPasswordChange = (e) => {
        setConfirmPassword(e.target.value);
    };

    const handleNameChange = (e) => {
        setName(e.target.value);
    };

    const handleVirtualProviderChange = (e) => {
        setVirtualMeetingProvider(e.target.value);
    };

    const handleClientKeyChange = (e) => {
        setClientKey(e.target.value);
    };

    const handleSecretKeyChange = (e) => {
        setSecretKey(e.target.value);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        console.log("JSON: ") 
        let body = {
            email: email,
            password: password,
            name: name,
            virtualMeetingProvider: virtualMeetingProvider,
            clientKey: clientKey,
            secretKey: secretKey
        };    
        console.log(JSON.stringify(body));
        try {
            fetch(SIGNUPURL, {
                method: "POST",
                headers: { 'content-type': 'application/json' },
                body: JSON.stringify(body)
            })
            .then(res => {
                if(!res.ok) {
                    throw Error("Could not retrieve data from resource: There was an error signing up");
                }
                return res.json();
            })
            .then(data => {
                // console.log(data);

                // show success message 
                toast.success("Signup successful!", {
                    duration: 5000,
                    position: 'bottom-right'
                })

                // navigate to the login
                setTimeout(() => { navigate(NAVTOLOGIN) }, 1000);
            })
            .catch(err => {
                console.log(err);
                toast.error("There was an error signing up, please try again! ", {
                    duration: 3000,
                    position: 'bottom-right'
                })
            })

        } catch (error) {
            console.error('Error during signup:', error);
            console.log(error)
            setErrorMessage('An error occurred during signup.');
        }
    };
    return ( 
        <div className="signup-component">
            {/*Using Bootstrap forms, reference: https://getbootstrap.com/docs/4.0/components/forms/*/}
            <form className="signup-form" onSubmit={handleSubmit}>
                <div className="form-group">
                    <label htmlFor="exampleInputEmail1" className="form-input-label">Email address</label>
                    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" value={email} onChange={handleEmailChange} required></input>
                </div><br />
                <div className="form-group">
                    <label htmlFor="exampleInputName1" className="form-input-label">Name</label>
                    <input type="name" className="form-control" id="exampleInputName1"  placeholder="Name" value={name} onChange={handleNameChange} required></input>
                </div><br />
                <div className="form-group">
                    <label htmlFor="exampleInputVirtualMeetingProvider" className="form-input-label">Virtual Meeting Provider</label>
                    <input type="text" className="form-control" id="exampleInputVirtualMeetingProvider"  placeholder="Virtual Meeting Provider (Optional)" value={virtualMeetingProvider} onChange={handleVirtualProviderChange} ></input>
                </div><br />
                <div className="form-group">
                    <label htmlFor="exampleInputClientKey1" className="form-input-label">Client Key</label>
                    <input type="password" className="form-control" id="exampleInputClientKey1"  placeholder="Client Key (Optional)" value={clientKey} onChange={handleClientKeyChange} ></input>
                </div><br />
                <div className="form-group">
                    <label htmlFor="exampleInputSecretKey1" className="form-input-label">Secret Key</label>
                    <input type="password" className="form-control" id="exampleInputSecretKey1"  placeholder="Secret Key (Optional)" value={secretKey} onChange={handleSecretKeyChange} ></input>
                </div><br />
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1" className="form-input-label">Password</label>
                    <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" value={password} onChange={handlePasswordChange} required></input>
                </div><br />
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1" className="form-input-label">Confirm Password</label>
                    <input type="password" className="form-control" id="exampleInputConfirmPassword1" placeholder="Confirm Password" value={confirmPassword} onChange={handleConfirmPasswordChange} required></input>
                </div><br />
                <p className="alert alert-danger" hidden={password != confirmPassword ? false : true}>PASSWORD DOES NOT MATCH</p>
                <button type="submit" className="action-button" disabled={password != confirmPassword ? true : false}>Submit</button>
            </form>
            {errorMessage && <div className="error-message">{errorMessage}</div>}
            <Toaster />
        </div>
     );
}
 
export default Signup;