﻿namespace video_meeting_rating_tool_api.Models.Dtos.QuizDto
{
    public class AddQuizDto
    {
        public int DocumentId { get; set; }
        public int MeetingId { get; set; }
        public int UserId { get; set; }
    }
}
