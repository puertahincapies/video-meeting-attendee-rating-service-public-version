﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using video_meeting_rating_tool_api.Controllers;
using video_meeting_rating_tool_api.Models;
using video_meeting_rating_tool_api.Models.Dtos.UserDto;
using video_meeting_rating_tool_api.Models.Interfaces;
using video_meeting_rating_tool_api.Repository;

namespace video_meeting_rating_tool_unit_tests
{
    public class UserControllerUnitTests
    {
        private readonly Mock<IMeetingQuestRepository> repositoryStub = new();
        private readonly Mock<ILogger<UserController>> loggerStub = new();
        private readonly Mock<IConfiguration> configurationStub = new();
        private readonly Random rand = new Random();
        private readonly UserController controller;

        public UserControllerUnitTests()
        {
            controller = new UserController(loggerStub.Object, repositoryStub.Object, configurationStub.Object);
        }

        [Fact]
        public void Signup_AddNewUser_ReturnsExpectedUser()
        {
            // Arrange
            var userToAdd = new AddUserDto()
            {
                Email = "test@email.com",
                Password = "test123",
                Name = "test"
            };

            // Act
            var actionResult = controller.SignUp(userToAdd);

            // Assert
            var result = actionResult.Result;
            var value = result?.Value as UserDto;
            result.Should().BeEquivalentTo(value, options => options.ComparingByMembers<User>().ExcludingMissingMembers());
        }

        [Fact]
        public void Login_GetUser_ReturnsExpectedUser()
        {
            // Arrange
            var user = new User()
            {
                Email = "test@email.com",
                Password = "test123",
                Name = "test",
                UserId = 1
            };

            var byteArray = Encoding.ASCII.GetBytes($"{user.Email}:{user.Password}");

            string encodeString = Convert.ToBase64String(byteArray);

            var credentials = new LoginUserDto()
            {
                Token = encodeString
            };


            repositoryStub.Setup(repo => repo.UserLogin(It.IsAny<string>(), It.IsAny<string>())).Returns(user);

            // Act
            var actionResult = controller.Login(credentials);

            // Assert
            var result = actionResult.Result;
            var response = result?.Value;
            result.Should().BeEquivalentTo(response?.Payload, options => options.ComparingByMembers<User>().ExcludingMissingMembers());
        }
    }
}
