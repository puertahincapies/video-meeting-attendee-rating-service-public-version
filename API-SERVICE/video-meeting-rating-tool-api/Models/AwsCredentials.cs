﻿namespace video_meeting_rating_tool_api.Models
{
    public class AwsCredentials
    {
        public string AccessKey { get; set; } = string.Empty;
        public string SecretKey { get; set; } = string.Empty;
    }
}
