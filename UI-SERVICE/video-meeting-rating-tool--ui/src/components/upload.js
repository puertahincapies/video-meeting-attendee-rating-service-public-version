import '../css/upload.css';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import toast, { Toaster } from 'react-hot-toast';
import Cookies from 'universal-cookie';
import {USERSESSION, SAVEMEETINGURL, UPLOADDOCUMENTURL, GENERATEQUIZURL, LOGERRORSURL, NAVTOGENERATEDQUIZ} from '../helpers/constants';
import useFetch from '../hooks/useFetch';

const Upload = ({userSession}) => {

    // states
    const [file, setFile] = useState(undefined);
    const [meetingUUID, setMeetingUUID] = useState("");
    const [meetingId, setMeetingId] = useState(0);
    const [meetingName, setMeetingName] = useState("");
    const [meetingDate, setMeetingDate] = useState("");
    const [attendeeEmailList, setAttendeeEmailList] = useState([]);
    const [documentId, setDocumentId] = useState(0);
    const [isMeetingDetailsPending, setIsMeetingDetailsPending] = useState(false);
    const [isUploadFilePending, setIsUploadFilePending] = useState(false);
    const [isGenerateQuizPending, setIsGenerateQuizPending] = useState(false);
    const [quizId, setQuizId] = useState(0);
    const [currentUserId, setCurrentUserId] = useState(0);
    const [manualUpload, setManualUpload] = useState(true);
    const [automaticRetrieval, setAutomaticRetrieval] = useState(true);
    const [error, setError] = useState("");

    // other declarations 
    const navigate = useNavigate();
    const cookies = new Cookies();

    useEffect(() => {
        let user = userSession;
        if(user == null || user== undefined) {
            user = cookies.get(USERSESSION);
            if(user != null || user != undefined){
                setCurrentUserId(user.userId);
            }
        }
        else {
            setCurrentUserId(user.userId);
        }
    }, []);

    // event handlers
    const handleSubmitMeetingDetails = (e) => {
        console.log("clicked on save meeting details submit button");
        e.preventDefault();

        let meetingDetails = {
            meetingUUID: meetingUUID,
            meetingName: meetingName,
            meetingDateTime: meetingDate,
            meetingAttendeeEmails: attendeeEmailList,
            userId: currentUserId
        }

        setIsMeetingDetailsPending(true);

        setTimeout(() => {
            fetch(SAVEMEETINGURL, {
                method: "POST",
                headers: { 'content-type': 'application/json'},
                body: JSON.stringify(meetingDetails)
            })
            .then(res => {
                if(!res.ok){
                    throw Error("Could not retrieve data from resource: There was error when saving meeting details");
                }
                if(res.body == null) {
                    throw Error("Payload retrieved is null");
                }
                return res.json();
            })
            .then(data => {
                // console.log(data);
                setIsMeetingDetailsPending(false);
                setMeetingId(data.meetingId);
                toast.success("Meeting details were successfully saved!", {
                    duration: 3000,
                    position: 'bottom-right'
                })
            })
            .catch(err => {
                setIsMeetingDetailsPending(false);
                console.log(err);
                setError(err);
                toast.error("There was an error when saving meeting details!", {
                    duration: 3000,
                    position: 'bottom-right'
                })
            })
        }, 2000)
    }

    const onFileChange = (e) => {
        setFile(e.target.files[0]);
    };

    const onFileUpload = (e) => {
        console.log("clicked on file upload button");
        e.preventDefault();

        const formData = new FormData();
        formData.append("Name", file.name);
        formData.append("MeetingId", meetingId);
        formData.append("Content", file);

        setIsUploadFilePending(true);
 
        setTimeout(() => {
            fetch(UPLOADDOCUMENTURL, {
                method: "POST",
                body: formData
            })
            .then(res => {
                if(!res.ok){
                    throw Error("Could not retrieve data from resource: There was an error when uploading the file");
                }
                return res.json();
            })
            .then(data => {
                // console.log(data);
                setIsUploadFilePending(false);
                setDocumentId(data.document.documentId);
                toast.success("File was successfully uploaded!", {
                    duration: 3000,
                    position: 'bottom-right'
                })          
            })
            .catch(err => {
                setIsUploadFilePending(false);
                console.log(err);
                setError(err);
                toast.error("There was an error when uploading the file!", {
                    duration: 3000,
                    position: 'bottom-right'
                })
            })
        }, 2000)
    };

    const handleGenerateQuiz = (e) => {
        console.log("clicked on generate quiz button");
        console.log(documentId);

        let quizDetails = {
            documentId: documentId,
            meetingId: meetingId,
            userId: currentUserId
        }

        setIsGenerateQuizPending(true);

        setTimeout(() => {
            fetch(GENERATEQUIZURL, {
                method: "POST",
                headers: { 'content-type': 'application/json'},
                body: JSON.stringify(quizDetails)
            })
            .then(res => {
                if(!res.ok){
                    throw Error("Could not retrieve data from resource: There was an error when generating the quiz");
                }
                return res.json();
            })
            .then(data => {
                // console.log(data);
                setIsGenerateQuizPending(false);
                setQuizId(data.quizId);
                toast.success("Quiz was successfully created!", {
                    duration: 3000,
                    position: 'bottom-right'
                })    
            })
            .catch(err => {
                setIsGenerateQuizPending(false);
                console.log(err);
                setError(err);
                toast.error("There was an error when generating the quiz!", {
                    duration: 3000,
                    position: 'bottom-right'
                })
            })
        }, 1000)
    }

    const handleGoToQuiz = (e) => {
        setTimeout(() => {
            navigate(NAVTOGENERATEDQUIZ, { state: { quizId: quizId } });
        }, 3000)
    }

    const handleManualUploadClick = (e) => {
        if(manualUpload) setManualUpload(false);
        else setManualUpload(true);
        setAutomaticRetrieval(true);
    }

    const handleAutomaticRetrievalClick = (e) => {
        if(automaticRetrieval) setAutomaticRetrieval(false);
        else setAutomaticRetrieval(true);
        setManualUpload(true);
    }

    return ( 
        <div className="upload-component">
            <label className="component-header">Upload</label>
            <div className = "meeting-details-section">
                <p className="upload-subheader">Part 1: Add Meeting Details</p>
                <form onSubmit={handleSubmitMeetingDetails} >
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Meeting Name: </label>
                        <div className="col-sm-8">
                            <input 
                                type="text" 
                                className="form-control" 
                                id="input-meeting-name" 
                                placeholder="(Required)" 
                                value = {meetingName}
                                onChange={(e) => setMeetingName(e.target.value)}
                                required
                            />
                        </div>
                    </div><br />
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Meeting #: </label>
                        <div className="col-sm-8">
                            <input 
                                type="text" 
                                className="form-control" 
                                id="input-meeting-id" 
                                placeholder="(Required)" 
                                value = {meetingUUID}
                                onChange={(e) => setMeetingUUID(e.target.value)}
                                required
                            />
                        </div>
                    </div><br />
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Meeting Date: </label>
                        <div className="col-sm-8">
                            <input 
                                type="date" 
                                className="form-control" 
                                id="input-meeting-name" 
                                placeholder="Meeting Date"
                                value = {meetingDate}
                                onChange={(e) => setMeetingDate(e.target.value)}
                                required
                            />
                        </div>
                    </div><br />
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Attendee Email List: </label>
                        <div className="col-sm-8">
                            <input 
                                type="text" 
                                className="form-control" 
                                id="input-attendee-email-list" 
                                placeholder="(Optional) semi-colon separated" 
                                value = {attendeeEmailList}
                                onChange={(e) => setAttendeeEmailList(e.target.value)} 
                            />
                        </div>
                    </div>
                    <div className="upload-action-buttons-area">
                    { !isMeetingDetailsPending && <button type="submit" className="action-button">Save</button> }
                    { isMeetingDetailsPending && <button type="submit" className="action-button" disabled>Saving Meeting Details...</button> }
                </div>
                </form>
            </div>
            <div className="add-transcript-section">
                <section className="transcript-action-selection-area ">
                <p className="upload-subheader">Part 2: Add Transcript</p>
                    <button type="button" className="action-button" onClick={handleManualUploadClick}>Manual Transcript Upload</button>
                    <button type="button" className="action-button disabled-button" onClick={handleAutomaticRetrievalClick} disabled>Automatic Transcript Retrieval (disabled)</button>
                </section>
                <section className = "upload-transcript-section" hidden={manualUpload}>
                    <p className="upload-subheader">Upload Meeting Transcript</p>
                    <p className="upload-subheader-file-limit-message">Max Size 25MB</p>
                    <form onSubmit={onFileUpload}>
                        <div className="mb-3">
                            <input 
                                className="form-control" 
                                type="file" id="formFile" 
                                multiple accept=".vtt, .txt" 
                                onChange={onFileChange} required/>
                        </div>
                        <div className="upload-action-buttons-area">
                            {!isUploadFilePending && meetingId !== 0 && <button type="submit" className="action-button">Upload</button>}
                            { isUploadFilePending && <button className="action-button" disabled>Uploading File...</button>}
                            {!isGenerateQuizPending && meetingId !== 0 && documentId !== 0 && <button type="button" className="action-button" onClick={handleGenerateQuiz}>Generate Quiz</button>}
                            {isGenerateQuizPending && <button className="action-button" disabled>Generating Quiz...</button>}
                            {meetingId !== 0 && documentId !== 0 && quizId !== 0 && <button type="button" className="action-button" onClick={handleGoToQuiz}>Show Quiz</button>}
                        </div>
                    </form>
                </section>
                <section className = "automatic-transcript-retrieval-section" hidden={automaticRetrieval}>
                    <p className="upload-subheader">Automatically Retrieve Meeting Transcript (Coming Soon)</p>
                    <p className="upload-subheader-file-limit-message">This feature is only avaible if you have provided virtual meeting provider details on signup or user account</p>
                    <form onSubmit={null}>
                        <div className="upload-action-buttons-area">
                            {!isUploadFilePending && meetingId !== 0 && <button type="submit" className="action-button">Retrieve Transcript</button>}
                            { isUploadFilePending && <button className="action-button" disabled>Retrieving File...</button>}
                            {!isGenerateQuizPending && meetingId !== 0 && documentId !== 0 && <button type="button" className="action-button" onClick={handleGenerateQuiz}>Generate Quiz</button>}
                            {isGenerateQuizPending && <button className="action-button" disabled>Generating Quiz...</button>}
                            {meetingId !== 0 && documentId !== 0 && quizId !== 0 && <button type="button" className="action-button" onClick={handleGoToQuiz}>Show Quiz</button>}
                        </div>
                    </form>
                </section>
            </div>
            <Toaster />
        </div>
     );
}
 
export default Upload;