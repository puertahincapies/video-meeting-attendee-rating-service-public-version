import '../css/quizzes.css';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import toast, { Toaster } from 'react-hot-toast';
import Cookies from 'universal-cookie';
import {USERSESSION, USERLOGIN, GETUSERQUIZZESURL, NAVTOGENERATEDQUIZ, NAVTOUNAUTHORIZE} from '../helpers/constants';

const Quizzes = ({userSession}) => {
    const [quizzes, setQuizzes] = useState([]);
    const navigate = useNavigate();
    const cookies = new Cookies();
    const [currentUserId, setCurrentUserId] = useState(0);

    useEffect(() => {
        let user = userSession;
        // console.log("quizzes > user: " + JSON.stringify(user));
        if(user == null || user == undefined) {
            user = cookies.get(USERSESSION);
            // console.log("quizzes > set user using cookie session: " + JSON.stringify(user));
            if(user != null || user != undefined){
                setCurrentUserId(user.userId);
            }
        }
        else {
            setCurrentUserId(user.userId);
        }

        if(user != null && user != undefined){
            fetch(GETUSERQUIZZESURL + "/" + user.userId, {
                method: "GET",
                headers: { 'content-type': 'application/json'},
            })
            .then(res => {
                if(!res.ok) {
                    throw Error("Could not retrieve data from resource: There was an error when retrieving the quiz");
                }
                return res.json();
            })
            .then(data => {
                // console.log(data); 
                setQuizzes(data);
            })
            .catch(err => {
                console.log(err);
                toast.error("There was an error when retrieving the quiz!", {
                    duration: 3000,
                    position: 'bottom-right'
                })
            })
        }
        else {
            navigate(NAVTOUNAUTHORIZE);
        }
    }, []);

    const handleOnQuizRowClick = (quiz) => {
        let isAttendee = false
        if(quiz.userId != currentUserId){
            isAttendee = true
        }
        navigate(NAVTOGENERATEDQUIZ, { state: { quizId: quiz.quizId, isAttendee: isAttendee } });
    }

    const updateQuizDate = (quiz) => {
        var arr = quiz.createDateTime.toString().split('T');
        arr.pop();
        // console.log(arr);
        return arr[0];
    }

    return ( 
        <div className="quizzes-component">
        <label className="component-header">Quizzes Remaining</label>
            {currentUserId != 0 && quizzes != null && <table>
                <thead>
                    <tr>
                        <th>MeetingId</th>
                        <th>QuizId</th>
                        <th>Date Assigned</th>
                    </tr>
                </thead>
                <tbody>
                    {quizzes.map((quiz) => (
                        <tr key={quiz.quizId} className="quiz-row" onClick={() => handleOnQuizRowClick(quiz)}>
                            <td>{quiz.meetingId}</td>
                            <td>{quiz.quizId}</td>
                            <td>{updateQuizDate(quiz)}</td>
                        </tr>
                    ))}
                </tbody>
            </table>}
            <Toaster />
        </div>
     );
}
 
export default Quizzes;