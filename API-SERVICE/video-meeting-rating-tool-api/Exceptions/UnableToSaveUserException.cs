﻿namespace video_meeting_rating_tool_api.Exceptions
{
    public class UnableToSaveUserException : Exception
    {
        public UnableToSaveUserException() { }
        public UnableToSaveUserException(string message) : base(message) { }
        public UnableToSaveUserException(string message, Exception innerException) : base(message, innerException) { }
    }
}
