﻿namespace video_meeting_rating_tool_api.Exceptions
{
    public class UnableToSaveAccessTokenException : Exception
    {
        public UnableToSaveAccessTokenException() { }
        public UnableToSaveAccessTokenException(string message) : base(message) { }
        public UnableToSaveAccessTokenException(string message, Exception innerException) : base(message, innerException) { }
    }
}
