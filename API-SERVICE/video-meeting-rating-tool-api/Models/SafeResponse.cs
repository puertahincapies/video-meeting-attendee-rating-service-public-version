﻿namespace video_meeting_rating_tool_api.Models
{
    public class SafeResponse<T>
    {
        public string Message { get; set; } = string.Empty;
        public T Payload { get; set; }
    }

    public class SafeResponse
    {
        public string Message { get; set; } = string.Empty;
    }
}
