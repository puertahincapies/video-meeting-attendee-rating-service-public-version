﻿namespace video_meeting_rating_tool_api.Exceptions
{
    public class UserQuizNotFoundException : Exception
    {
        public UserQuizNotFoundException() { }
        public UserQuizNotFoundException(string message) : base(message) { }
        public UserQuizNotFoundException(string message, Exception innerException) : base(message, innerException) { }
    }
}
