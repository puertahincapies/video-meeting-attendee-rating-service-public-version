import '../css/guestaccessportal.css';
import { useState } from 'react';
import toast, { Toaster } from 'react-hot-toast';
import { useNavigate } from 'react-router-dom';
import {VALIDATEACCESSTOKENURL, NAVTOGENERATEDQUIZ} from '../helpers/constants';

const GuestAccessPortal = () => {

    const [token, setToken] = useState("");
    const [quizAccessToken, setQuizAccessToken] = useState(null);
    const navigate = useNavigate();

    const OnClickEnter = (e) => {
        console.log("On click enter");
        e.preventDefault();

        fetch(VALIDATEACCESSTOKENURL + token, {
            method: "GET",
            headers: {"content-type":"application/json"}
        })
        .then(res => {
            if(!res.ok) {
                throw Error("Could not retrieve data from resource");
            }
            return res.json();
        })
        .then(data => {
            console.log(data); 
            setQuizAccessToken(data);
            toast.success("Access token was valid", {
                duration: 3000,
                position: 'bottom-right'
            });
            navigate(NAVTOGENERATEDQUIZ, { state: { quizId: data.quizId, isGuest: true, userId: data.userId } });
        })
        .catch(err => {
            console.log(err);
            toast.error("There was an error!", {
                duration: 3000,
                position: 'bottom-right'
            })
        })
    }

    return (
        <div className="meetingquestquizportal-component">
            <form className="token-access-portal-form" onSubmit={OnClickEnter}>
                <input 
                    type="text" 
                    className="form-control access-token-input" 
                    id="input-access-token" 
                    placeholder="Enter Access Token: 083CA013-ACC7-426B-BB65-C77C30F917B4" 
                    value = {token}
                    onChange={(e) => setToken(e.target.value)}
                    required
                />
                <button type="submit" className="action-button">Enter</button>
            </form>
            <Toaster />
        </div>
    );
}
 
export default GuestAccessPortal;