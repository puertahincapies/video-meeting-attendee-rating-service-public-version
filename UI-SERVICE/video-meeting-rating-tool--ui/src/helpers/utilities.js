import Cookies from 'universal-cookie';

const cookies = new Cookies();

export const setSessionCookie = (cookieName, hour, value) => {
    var date = new Date();
    var month = date.getMonth();
    var day = date.getDay();
    var year = date.getFullYear();
    var hours = date.getHours() + hour;
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    cookies.remove(cookieName);
    cookies.set(cookieName, value, { path: '/' , expires: new Date(year, month, day, hours, minutes, seconds)});
}

export const generateGuid = () => {
    let n = 16
    let guid = "";
    while(n > 0) {
        guid = guid + Math.floor(Math.random() * 10).toString();
        n--;
    }
    return guid;
}