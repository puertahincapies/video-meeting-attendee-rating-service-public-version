-- CREATE DATABASE
CREATE DATABASE MeetingQuest;

USE MeetingQuest;

-- USER ENTITY
CREATE TABLE dbo.Users (
	UserId INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Email VARCHAR(50) NOT NULL,
	Password VARCHAR(50) NOT NULL,
	Name VARCHAR(100) NOT NULL,
	VirtualMeetingProvider VARCHAR(50) NULL,
	ClientKey VARCHAR(50) NULL,
	SecretKey VARCHAR(50) NULL,
	UUID VARCHAR(50) NOT NULL,
	CreateDateTime DATETIME NOT NULL
);

-- MEETING ENTITY
CREATE TABLE dbo.Meetings (
	MeetingId INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	MeetingUUID VARCHAR(50) NULL,
	MeetingName VARCHAR(50) NOT NULL,
	MeetingDateTime DATETIME NOT NULL,
	UserId INT NOT NULL,
	MeetingAttendeeEmails VARCHAR(MAX) NULL,
	UUID VARCHAR(50) NOT NULL,
	CreateDateTime DATETIME NOT NULL
);

-- DOCUMENT ENTITY
CREATE TABLE dbo.Documents (
    DocumentId INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    MeetingId INT NOT NULL,
	Name VARCHAR(50) NOT NULL,
    Content VARBINARY(MAX) NOT NULL,
    UUID VARCHAR(50) NOT NULL,
	CreateDateTime DATETIME NOT NULL
);

-- DOCUMENT SENTIMENT ANALYSIS ENTITY
CREATE TABLE dbo.DocumentsSentimentAnalysis (
    DocumentSentimentAnalysisId INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	DocumentId INT NOT NULL,
    PositiveScore VARCHAR(8) NOT NULL,
	NegativeScore VARCHAR(8) NOT NULL,
	NeutralScore VARCHAR(8) NOT NULL,
	CompoundScore VARCHAR(8) NOT NULL,
	Sentiment VARCHAR(12) NOT NULL,
    UUID VARCHAR(50) NOT NULL,
	CreateDateTime DATETIME NOT NULL
);

-- QUIZ ENTITY
CREATE TABLE dbo.Quizzes (
	QuizId INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	DocumentId INT NOT NULL,
	MeetingId INT NOT NULL,
	UserId INT NOT NULL,
	Question1 VARCHAR(255) NULL,
	Question2 VARCHAR(255) NULL,
	Question3 VARCHAR(255) NULL,
	Question4 VARCHAR(255) NULL,
	Question5 VARCHAR(255) NULL,
	Question1Answer1 VARCHAR(255) NULL,
	Question1Answer2 VARCHAR(255) NULL,
	Question1Answer3 VARCHAR(255) NULL,
	Question2Answer1 VARCHAR(255) NULL,
	Question2Answer2 VARCHAR(255) NULL,
	Question2Answer3 VARCHAR(255) NULL,
	Question3Answer1 VARCHAR(255) NULL,
	Question3Answer2 VARCHAR(255) NULL,
	Question3Answer3 VARCHAR(255) NULL,
	Question4Answer1 VARCHAR(255) NULL,
	Question4Answer2 VARCHAR(255) NULL,
	Question4Answer3 VARCHAR(255) NULL,
	Question5Answer1 VARCHAR(255) NULL,
	Question5Answer2 VARCHAR(255) NULL,
	Question5Answer3 VARCHAR(255) NULL,
	Question1CorrectAnswer VARCHAR(255) NULL,
	Question2CorrectAnswer VARCHAR(255) NULL,
	Question3CorrectAnswer VARCHAR(255) NULL,
	Question4CorrectAnswer VARCHAR(255) NULL,
	Question5CorrectAnswer VARCHAR(255) NULL,
	IsConsistent INT NULL,
	UUID VARCHAR(50) NOT NULL,
	CreateDateTime DATETIME NOT NULL
);

-- QUIZ RESULT ENTITY
CREATE TABLE dbo.QuizResults (
	QuizResultId INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	QuizId INT NOT NULL,
	MeetingId INT NOT NULL,
	UserId INT NOT NULL,
	Role VARCHAR(15) NOT NULL,
	Email VARCHAR(50) NOT NULL,
	Question1Response VARCHAR(255) NOT NULL,
	Question2Response VARCHAR(255) NOT NULL,
	Question3Response VARCHAR(255) NOT NULL,
	Question4Response VARCHAR(255) NOT NULL,
	Question5Response VARCHAR(255) NOT NULL,
	Score INT NOT NULL,
	Status VARCHAR(10) NOT NULL,
	UUID VARCHAR(50) NOT NULL,
	CreateDateTime DATETIME NOT NULL
);

-- ERRORS ENTITY
CREATE TABLE dbo.Errors (
	ErrorId INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	ErrorMessage VARCHAR(255) NOT NULL,
	ErrorType VARCHAR(10) NOT NULL,
	StackTrace VARCHAR(MAX) NOT NULL,
	TimeStamp DATETIME NOT NULL
)

CREATE TABLE dbo.QuizAccessTokens(
	QuizAccessTokenId INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	QuizId INT NOT NULL,
	MeetingId INT NOT NULL,
	UserId INT NOT NULL,
	Email VARCHAR(50) NOT NULL,
	IsUsed INT NOT NULL,
	Token VARCHAR(50) NOT NULL,
	UUID VARCHAR(50) NOT NULL,
	CreateDateTime DateTime NOT NULL
)

CREATE TABLE dbo.UserQuizzes(
	UserQuizId INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	UserId INT NOT NULL,
	QuizId INT NOT NULL,
	UUID VARCHAR(50) NOT NULL,
	CreateDateTime DateTime NOT NULL
)

CREATE TABLE dbo.MeetingAttendeeEmails(
	MeetingAttendeeEmailId INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	UserId INT NOT NULL,
	AttendeeEmail VARCHAR(50) NOT NULL,
	UUID VARCHAR(50) NOT NULL,
	CreateDateTime DateTime NOT NULL
)

-- DROP TABLES
--DROP DATABASE MeetingQuest;
--DROP TABLE MeetingQuest.dbo.Users;
--DROP TABLE MeetingQuest.dbo.Meetings;
--DROP TABLE MeetingQuest.dbo.Documents;
--DROP TABLE MeetingQuest.dbo.Errors;
--DROP TABLE MeetingQuest.dbo.Quizzes;
--DROP TABLE MeetingQuest.dbo.QuizResults;
--DROP TABLE MeetingQuest.dbo.Reports;
--DROP TABLE MeetingQuest.dbo.QuizAccessTokens;
--DROP TABLE MeetingQuest.dbo.UserQuizzes;
--DROP TABLE MeetingQuest.dbo.MeetingAttendeeEmails;
--DROP TABLE MeetingQuest.dbo.DocumentsSentimentAnalysis;

-- ADD FOREIGN KEYS
--ALTER TABLE Meetings
--ADD FOREIGN KEY (DocumentId) REFERENCES Documents(DocumentId);

--ALTER TABLE Documents
--ADD FOREIGN KEY (MeetingId) REFERENCES Meetings(MeetingId);

 --TESTING
--SELECT * FROM MeetingQuest.dbo.Documents
--SELECT * FROM MeetingQuest.dbo.QuizResults
--SELECT * FROM MeetingQuest.dbo.Quizzes
--SELECT * FROM MeetingQuest.dbo.Errors
--SELECT * FROM MeetingQuest.dbo.Users
--SELECT * FROM MeetingQuest.dbo.Meetings
--SELECT * FROM MeetingQuest.dbo.QuizAccessTokens
--SELECT * FROM MeetingQuest.dbo.UserQuizzes
--SELECT * FROM MeetingQuest.dbo.MeetingAttendeeEmails
--SELECT * FROM MeetingQuest.dbo.DocumentsSentimentAnalysis

--DELETE FROM MeetingQuest.dbo.Documents
--DELETE FROM MeetingQuest.dbo.QuizResults
--DELETE FROM MeetingQuest.dbo.Quizzes
--DELETE FROM MeetingQuest.dbo.Meetings
--DELETE FROM MeetingQuest.dbo.QuizAccessTokens
--DELETE FROM MeetingQuest.dbo.UserQuizzes
--DELETE FROM MeetingQuest.dbo.MeetingAttendeeEmails
--DELETE FROM MeetingQuest.dbo.DocumentsSentimentAnalysis
--DELETE FROM MeetingQuest.dbo.Users

--INSERT INTO MeetingQuest.dbo.Users 
--(Email, Password, Name, ClientKey, SecretKey, UUID, CreateDateTime) 
--VALUES ('admin@email.com', 'admin123', 'Admin', NULL, NULL, CONVERT(NVARCHAR(36), NEWID()), GETDATE())