﻿using Microsoft.AspNetCore.Authorization;

namespace BasicAuthentication.Settings
{
    public class BasicAuthorizationAttribute : AuthorizeAttribute
    {
        public BasicAuthorizationAttribute()
        {
            Policy = "BasicAuthentication";
        }
    }
}
