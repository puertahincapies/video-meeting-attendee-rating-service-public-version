﻿using System.ComponentModel.DataAnnotations;

namespace video_meeting_rating_tool_api.Models.Dtos.QuizResultDto
{
    public class AddQuizResultDto
    {
        public int QuizId { get; set; }
        public int MeetingId { get; set; }
        public int UserId { get; set; }
        public string Question1Response { get; set; }
        public string Question2Response { get; set; }
        public string Question3Response { get; set; }
        public string Question4Response { get; set; }
        public string Question5Response { get; set; }
        public bool IsGuest { get; set; }
    }
}
