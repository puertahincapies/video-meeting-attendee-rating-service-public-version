﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using video_meeting_rating_tool_api.Models;

namespace video_meeting_rating_tool_automation.Helpers
{
    public class QuizApiHelper
    {
        private readonly Settings _settings;

        private const string path = "/api/v1/meetingquest/Quiz";

        private string generateQuiz = $"{path}/GenerateQuiz";
        private string getQuiz = $"{path}/GetQuiz/{0}";
        private string getUserQuizzes = $"{path}/GetUserQuizzes/{0}";
        private string downloadQuiz = $"{path}/DownloadQuiz/{0}/{1}";
        private string downloadQuizByFileName = $"{path}/DownloadQuiz/{0}";
        private string emailQuiz = $"{path}/EmailQuiz";
        private string submitQuiz = $"{path}/SubmitQuiz";
        private string getQuizResult = $"{path}/GetQuizResult/{0}/{1}";
        private string getAccessToken = $"{path}/GetAccessToken/{0}/{1}/{2}";
        private string generateAccessToken = $"{path}/DownloadQuiz";
        private string validateAccessToken = $"{path}/ValidateAccessToken/{0}";

        public QuizApiHelper(Settings settings)
        {
            _settings = settings;
        }

        public Quiz GenerateQuiz(int documentId, int meetingId, int userId)
        {
            return null;
        }

        public Quiz GetQuiz(int quizId)
        {
            return null;
        }

        public List<Quiz> GetUserQuizzes(int userId)
        {
            return null;
        }

        public void DownloadQuiz(int meetingId, int quizId)
        {

        }

        public void DownloadQuizByFileName(string filename)
        {
            
        }

        public void EmailQuiz()
        {

        }

        private QuizResult SubmitQuiz(QuizResult quizResult)
        {
            return null;
        }

        private QuizResult GetQuizResult(int quizId, int userId)
        {
            return null;
        }

        private string GetAccessToken(int userId, int quizId, int meetingId)
        {
            return null;
        }

        public string GenerateAccessToken(int userId, int quizId, int meetingId)
        {
            return null;
        }

        public QuizAccessToken ValidateAccessToken(string token)
        {
            return null;
        }
    }
}
