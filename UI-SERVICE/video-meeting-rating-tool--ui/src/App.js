import './App.css';
import './css/header.css';
import './css/footer.css';
import Home from './components/home';
import Upload from './components/upload';
import QuizHistory from './components/quizhistory';
import UserAccount from './components/useraccount';
import AboutUs from './components/aboutus';
import Login from './components/login';
import SignUp from './components/signup';
import Header from './components/header';
import Footer from './components/footer';
import GeneratedQuiz from './components/generatedquiz';
import QuizResults from './components/quizresults';
import Report from './components/report';
import GuestAccessPortal from './components/guestaccessportal';
import Quizzes from './components/quizzes';
import NotFound from './components/notfound';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { useState } from 'react';
import Cookies from 'universal-cookie';
import {useCookies} from 'react-cookie';
import {setSessionCookie} from './helpers/utilities';
import {USERSESSION,USERLOGIN} from './helpers/constants';
import Unauthorized from './components/unauthorized';

function App() {
  const [userSession, setUserSession] = useState(null);
  const [cookies, setCookie] = useCookies([USERSESSION,USERLOGIN]);

  const handleUserLogin = (user, isLoggedIn) => {
    setCookie(USERSESSION, user, { path: "/"})
    setCookie(USERLOGIN, isLoggedIn, { path: "/"})
  }

  const handleUserLogout = () => {
    setCookie(USERSESSION, null, { path: "/"})
  }

  return (
    <Router>
    <div className="App">
      <Header userSession={userSession} setUserSession={setUserSession} />
      <div className="content">
        <Routes>
          <Route path="/meetingquest/" element={ <Home /> }></Route>
          <Route path="/meetingquest/home" element={ <Home userSession={userSession} /> }></Route>
          <Route path="/meetingquest/upload" element={ <Upload userSession={userSession} /> }></Route>
          <Route path="/meetingquest/quizhistory" element={ <QuizHistory userSession={userSession} /> }></Route>
          <Route path="/meetingquest/useraccount" element={ <UserAccount userSession={userSession} /> }></Route>
          <Route path="/meetingquest/aboutus" element={ <AboutUs /> }></Route>
          <Route path="/meetingquest/login" element={ <Login onLogin={handleUserLogin} setUserSession={setUserSession} /> }></Route>
          <Route path="/meetingquest/signup" element={ <SignUp /> }></Route>
          <Route path="/meetingquest/generatedquiz" element={ <GeneratedQuiz userSession={userSession} /> }></Route>
          <Route path="/meetingquest/quizresults" element={ <QuizResults userSession={userSession} /> }></Route>
          <Route path="/meetingquest/report" element={ <Report userSession={userSession} /> }></Route>
          <Route path="/meetingquest/guestaccessportal" element={ <GuestAccessPortal /> }></Route>
          <Route path="/meetingquest/quizzes" element={<Quizzes userSession={userSession} /> }></Route>
          <Route path="/meetingquest/unauthorized" element={<Unauthorized /> }></Route>
          <Route path="*" element={ <NotFound /> }></Route>
        </Routes>
      </div>
      <Footer />
    </div>
    </Router>
  );
}

export default App;
