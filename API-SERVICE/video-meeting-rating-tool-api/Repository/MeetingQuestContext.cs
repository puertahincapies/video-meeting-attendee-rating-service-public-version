﻿using Microsoft.EntityFrameworkCore;
using video_meeting_rating_tool_api.Models;

namespace video_meeting_rating_tool_api.Repository
{
    public class MeetingQuestContext : DbContext
    {
        public MeetingQuestContext(DbContextOptions<MeetingQuestContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Meeting> Meetings { get; set; }
        public DbSet<Quiz> Quizzes { get; set; }
        public DbSet<QuizResult> QuizResults { get; set; }
        public DbSet<Error> Errors { get; set; }
        public DbSet<QuizAccessToken> QuizAccessTokens { get; set; }
        public DbSet<UserQuizzes> UserQuizzes { get; set; }
        public DbSet<MeetingAttendeeEmail> MeetingAttendeeEmails { get; set; }
        public DbSet<DocumentSentimentAnalysis> DocumentSentimentAnalysis { get; set; }
    }
}
