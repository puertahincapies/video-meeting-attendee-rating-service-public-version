﻿using System.ComponentModel.DataAnnotations;

namespace video_meeting_rating_tool_api.Enums
{
    public enum StatusType
    {
        Unknown = 0,
        [Display(Name = "Not Submitted")]
        NotSubmitted = 1,
        Submitted = 2
    }
}
