﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace video_meeting_rating_tool_api.Models
{
    [Table("DocumentsSentimentAnalysis")]
    public class DocumentSentimentAnalysis
    {
        [Key]
        public int DocumentSentimentAnalysisId {  get; set; }
        public int DocumentId { get; set; }
        public string PositiveScore { get; set; }
        public string NegativeScore { get; set; }
        public string NeutralScore { get; set; }
        public string CompoundScore { get; set; }
        public string Sentiment { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();
        public DateTime CreateDateTime { get; set; } = DateTime.Now;
    }
}
