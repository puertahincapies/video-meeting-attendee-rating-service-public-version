﻿using video_meeting_rating_tool_api.Models.Dtos.MeetingDto;
using video_meeting_rating_tool_automation.Helpers;

namespace video_meeting_rating_tool_automation.IntegrationTests
{
    public class MeetingControllerTests
    {
        Settings settings;
        MeetingApiHelper api;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            settings = new Settings();
            api = new MeetingApiHelper(settings);
        }

        [SetUp]
        public void Setup() { /*Do nothing*/ }

        [TearDown]
        public void TearDown() { /*Do nothing*/ }

        [OneTimeTearDown]
        public void OneTimeTearDown() { /*Do nothing*/ }

        [Test]
        public void Meeting_SaveMeetingDetails()
        {
            try
            {
                // arrange
                var guid = Guid.NewGuid(); 
                var addMeetingDto = new AddMeetingDto()
                {
                    MeetingUUID = guid.ToString(),
                    MeetingName = $"Test Meeting",
                    MeetingDateTime = DateTime.Now,
                    MeetingAttendeeEmails = "test@email.com"
                };

                // action
                var result = api.SaveMeetingDetails(addMeetingDto);

                // assert
                Assert.That(result.MeetingUUID, Is.EqualTo(guid.ToString()));

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void Meeting_GetMeeting()
        {
            Assert.Ignore();
        }

        [Test]
        public void Meeting_GetMeetings()
        {
            Assert.Ignore();
        }

        [Test]
        public void Meeting_GetAttendeeEmails()
        {
            Assert.Ignore();
        }
    }
}
