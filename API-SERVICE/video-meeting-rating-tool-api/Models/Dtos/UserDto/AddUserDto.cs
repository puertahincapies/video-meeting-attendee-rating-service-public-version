﻿using System.ComponentModel.DataAnnotations;

namespace video_meeting_rating_tool_api.Models.Dtos.UserDto
{
    public class AddUserDto
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Name { get; set; }
        public string? VirtualMeetingProvider { get; set; }
        public string? ClientKey { get; set; }
        public string? SecretKey { get; set; }
    }
}
