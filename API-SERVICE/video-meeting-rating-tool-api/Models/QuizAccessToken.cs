﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace video_meeting_rating_tool_api.Models
{
    [Table("QuizAccessTokens")]
    public class QuizAccessToken
    {
        [Key]
        public int QuizAccessTokenId { get; set; }
        public int QuizId { get; set; }
        public int MeetingId { get; set; }
        public int UserId { get; set; }
        public string Email { get; set; } = string.Empty;
        public int IsUsed { get; set; } 
        public string Token { get; set; } = Guid.NewGuid().ToString().ToUpper();
        public string UUID { get; set; } = Guid.NewGuid().ToString();
        public DateTime CreateDateTime { get; set; } = DateTime.Now;
    }
}
