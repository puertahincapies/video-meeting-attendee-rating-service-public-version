﻿namespace video_meeting_rating_tool_api.Models
{
    public class DocumentSentimentResult
    {
        public int MeetingId { get; set; }
        public int DocumentId { get; set; }
        public string SentimentResult { get; set; } = "undefined";
    }
}
