﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using video_meeting_rating_tool_api.Models;
using video_meeting_rating_tool_api.Repository;
using video_meeting_rating_tool_api.Helpers;
using video_meeting_rating_tool_api.Models.Dtos.UserDto;
using BasicAuthentication.Settings;
using System.Text;
using Newtonsoft.Json;
using video_meeting_rating_tool_api.Exceptions;

namespace video_meeting_rating_tool_api.Controllers
{   
    [ApiController]
    [EnableCors("MeetingQuest_CorsPolicy")]
    [Route("api/v1/meetingquest/[controller]")]
    public class UserController : ControllerBase
    { //Will be used to pull a list of all quizzes for each user
        private readonly ILogger<UserController> _logger;
        private readonly IMeetingQuestRepository _repository;
        private readonly IConfiguration _configuration;
        private AwsCloudWatchClient _cloudWatchClient;

        public UserController(ILogger<UserController> logger, IMeetingQuestRepository repository, IConfiguration configuration)
        {
            _logger = logger;
            _repository = repository;
            _configuration = configuration;
            _cloudWatchClient = new AwsCloudWatchClient(_configuration);
        }

        [BasicAuthorization]
        [HttpPost("[action]")]
        public async Task<ActionResult<SafeResponse<UserDto>>> Login([FromBody]LoginUserDto credentials)
        {
            try
            {
                if(string.IsNullOrEmpty(credentials.Token))
                {
                    return BadRequest("Incorrect credentials entered");
                }

                var authBase64 = Encoding.UTF8.GetString(Convert.FromBase64String(credentials.Token));
                var authSplit = authBase64.Split(Convert.ToChar(":"), 2);
                var email = authSplit[0];
                var password = authSplit[1];

                var user = _repository.UserLogin(email, password);

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"User has been successfully authenticated. {JsonConvert.SerializeObject(user.AsDto())}");
                return Ok(new SafeResponse<UserDto>() { Message = "User has been successfully authenticated", Payload = user.AsDto()});
            }
            catch(UserNotFoundException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return NotFound(new SafeResponse() { Message = "User has been successfully authenticated"});
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError($"Error retrieving users");
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<IEnumerable<UserDto>>> GetUsers()
        {
            try
            {
                var users = _repository.GetUsers().Select(x => x.AsDto());
                if (users == null)
                {
                    return NotFound("Users not found");
                }

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"{users.Count()} users were successfully retrieved");
                return Ok(users);
            }
            catch (UserNotFoundException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return NotFound(new SafeResponse() { Message = "Users not found" });
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        [HttpGet("[action]/{userId}")]
        public async Task<ActionResult<UserDto>> GetUser(int userId)
        {
            try
            {
                if (userId == 0)
                {
                    return NotFound("UserId provided was not found");
                }

                var user = _repository.GetUser(userId).AsDto();
                if (user == null)
                {
                    return NotFound("User not found");
                }

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"User was successfully retrieved. User: {JsonConvert.SerializeObject(user)}");
                return Ok(user);
            }
            catch (UserNotFoundException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return NotFound(new SafeResponse() { Message = "User not found" });
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError($"Error retrieving user with ID {userId}: {ex.Message}");
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        [HttpPost("[action]")]
        public async Task<ActionResult<UserDto>> SignUp([FromBody] AddUserDto addUserDto)
        {
            try
            {
                // Validate the incoming data
                if (addUserDto == null || string.IsNullOrEmpty(addUserDto.Email) || string.IsNullOrEmpty(addUserDto.Password))
                {
                    return BadRequest(new SafeResponse() { Message = "Invalid user data" });
                }

                // Check if the email is already taken
                if (_repository.GetUserByEmail(addUserDto.Email) != null)
                {
                    return Conflict(new SafeResponse() { Message = "Email is already registered" });
                }                

                // Create a new user entity
                var newUser = new User
                {
                    // Assign properties from createUserDto                    
                    Email = addUserDto.Email,
                    Password = addUserDto.Password,
                    Name = addUserDto.Name,
                    VirtualMeetingProvider = addUserDto.VirtualMeetingProvider,
                    ClientKey = addUserDto.ClientKey,
                    SecretKey = addUserDto.SecretKey                    
                };

                // Save the new user to the repository
                _repository.SignUp(newUser);

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"User sign up was successful: User: {JsonConvert.SerializeObject(newUser)}");
                // Return the created user as DTO
                return CreatedAtAction(nameof(GetUser), new { userId = newUser.UserId}, newUser.AsDto());
            }
            catch(DuplicateEmailException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return NoContent();
            }
            catch(UnableToSaveUserException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return NotFound(new SafeResponse() { Message = "User was not able to sign up" });
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError($"Error creating user: {ex.Message}");
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }
        
        [HttpPut("[action]/{userId}")]
        public async Task<ActionResult<UserDto>> ChangeUser([FromBody]UpdateUserDto newCredentials)
        {
            try
            {
                if(string.IsNullOrEmpty(newCredentials.Token))
                {
                    return BadRequest("Incorrect credentials entered");
                }

                var authBase64 = Encoding.UTF8.GetString(Convert.FromBase64String(newCredentials.Token));
                var authSplit = authBase64.Split(Convert.ToChar(":"), 6);
                var id = Int32.Parse(authSplit[0]);
                var name = authSplit[1];
                var password = authSplit[2];
                var virtualMeetingProvider = authSplit[3];
                var clientKey = authSplit[4];
                var secretKey = authSplit[5];

                var user = _repository.ChangeUser(id, name, password, virtualMeetingProvider, clientKey, secretKey);

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"User details were successfully updated. User: {JsonConvert.SerializeObject(user)}");
                return Ok(new SafeResponse<UserDto>() { Message = "User info has been changed", Payload = user.AsDto()});
            }
            catch(UserNotFoundException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return NotFound(new SafeResponse() { Message = "User not found" });
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError($"Error retrieving users");
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        } 
    }
}
