﻿using System.ComponentModel.DataAnnotations;

namespace video_meeting_rating_tool_api.Models.Dtos.UserDto
{
    public class UpdateUserDto
    {
        
        [Required]
        public string Token { get; set; }
    }
}
