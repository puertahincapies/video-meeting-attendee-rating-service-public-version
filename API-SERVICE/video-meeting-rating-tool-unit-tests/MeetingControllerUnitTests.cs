﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using video_meeting_rating_tool_api.Controllers;
using video_meeting_rating_tool_api.Models;
using video_meeting_rating_tool_api.Models.Dtos.MeetingDto;
using video_meeting_rating_tool_api.Repository;

namespace video_meeting_rating_tool_unit_tests
{
    public class MeetingControllerUnitTests
    {
        private readonly Mock<IMeetingQuestRepository> repositoryStub = new();
        private readonly Mock<ILogger<MeetingController>> loggerStub = new();
        private readonly Random rand = new Random();
        private readonly MeetingController controller;
        private readonly Mock<IConfiguration> configurationStub = new();

        public MeetingControllerUnitTests()
        {
            controller = new MeetingController(loggerStub.Object, repositoryStub.Object, configurationStub.Object);
        }

        [Fact]
        public void SaveMeetingDetails_NoMeetingDetailsProvided_ReturnsBadRequest()
        {
            // Arrange
            var meeting = new Meeting()
            {
                MeetingUUID = string.Empty,
                MeetingDateTime = DateTime.Now,
                MeetingName = "Test Meeting",
                MeetingAttendeeEmails = "test@gmail.com",
                UserId = 0
            };

            repositoryStub.Setup(repo => repo.AddMeeting(meeting)).Returns(meeting);

            // Act
            var actionResult = controller.SaveMeetingDetails(null);

            // Assert
            var result = actionResult.Result;
            result?.Value.Should().Be("No meeting details provided");
        }

        [Fact]
        public void SaveMeetingDetails_MeetingDetailsProvided_ReturnsNotFoundRequest()
        {
            // Arrange
            var meeting = new AddMeetingDto()
            {
                MeetingUUID = string.Empty,
                MeetingDateTime = DateTime.Now,
                MeetingName = "Test Meeting",
                MeetingAttendeeEmails = "test@gmail.com",
                UserId = 0
            };

            repositoryStub.Setup(repo => repo.AddMeeting(It.IsAny<Meeting>())).Returns((Meeting)null);

            // Act
            var actionResult = controller.SaveMeetingDetails(meeting);

            // Assert
            var result = actionResult.Result;
            result?.Value.Should().Be("Meeting not found");
        }

        [Fact]
        public void SaveMeetingDetails_MeetingDetailsProvided_ReturnsExpectedMeeting()
        {
            // Arrange
            var guid = Guid.NewGuid().ToString();

            var addMeeting = new AddMeetingDto()
            {
                MeetingUUID = guid,
                MeetingDateTime = DateTime.Now,
                MeetingName = "Test Meeting",
                MeetingAttendeeEmails = "test@gmail.com",
                UserId = 101
            };

            // Act
            var actionResult = controller.SaveMeetingDetails(addMeeting);

            // Assert
            var insertedEmployee = actionResult.Result.Value;
            actionResult.Should().BeEquivalentTo(insertedEmployee, options => options.ComparingByMembers<Meeting>().ExcludingMissingMembers());
        }
    }
}
