﻿namespace video_meeting_rating_tool_api.Exceptions
{
    public class UnableToSaveMeetingException : Exception
    {
        public UnableToSaveMeetingException() { }
        public UnableToSaveMeetingException(string message) : base(message) { }
        public UnableToSaveMeetingException(string message, Exception innerException) : base(message, innerException) { }
    }
}
