﻿namespace video_meeting_rating_tool_api.Exceptions
{
    public class UnableToSubmitQuizResulException : Exception
    {
        public UnableToSubmitQuizResulException() { }
        public UnableToSubmitQuizResulException(string message) : base(message) { }
        public UnableToSubmitQuizResulException(string message, Exception innerException) : base(message, innerException) { }
    }
}
