﻿namespace video_meeting_rating_tool_api.Exceptions
{
    public class UnableToSaveUserQuizException : Exception
    {
        public UnableToSaveUserQuizException() { }
        public UnableToSaveUserQuizException(string message) : base(message) { }
        public UnableToSaveUserQuizException(string message, Exception innerException) : base(message, innerException) { }
    }
}
