﻿using System.Net.Mail;
using System.Net;
using video_meeting_rating_tool_api.Models;

namespace video_meeting_rating_tool_api.Helpers
{
    public static class EmailClient
    {
        public static void SendGoogleEmailWithAttachments(Email email)
        {
            //    var smtpClient = new SmtpClient("smtp.gmail.com")
            //    {
            //        Port = 587,
            //        Credentials = new NetworkCredential(email.Username, email.Password),
            //        EnableSsl = true,
            //    };
            //    smtpClient.UseDefaultCredentials = false;

            //    var mailMessage = new MailMessage
            //    {
            //        From = new MailAddress(email.FromEmail),
            //        Subject = email.Subject,
            //        Body = email.Body,
            //        IsBodyHtml = email.IsBodyHtml
            //    };

            //    mailMessage.Attachments.Add(email.Attachment);
            //    mailMessage.To.Add(email.ToEmail);

            //    smtpClient.Send(mailMessage);
            //}
            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(email.FromEmail);
                mail.To.Add(email.ToEmail);
                mail.Subject = email.Subject;
                mail.Body = email.Body;
                mail.IsBodyHtml = email.IsBodyHtml;
                mail.Attachments.Add(email.Attachment);

                using (SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587))
                {
                    smtp.Credentials = new NetworkCredential(email.Username, email.Password);
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = false;
                    smtp.Send(mail);
                }
            }
        }

        public static void SendGoogleEmail(Email email)
        {
            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(email.FromEmail);
                mail.To.Add(email.ToEmail);
                mail.Subject = email.Subject;
                mail.Body = email.Body;
                mail.IsBodyHtml = email.IsBodyHtml;

                using (SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587))
                {
                    smtp.Credentials = new NetworkCredential(email.Username, email.Password);
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = false;
                    smtp.Send(mail);
                }
            }
        }
    }
}
