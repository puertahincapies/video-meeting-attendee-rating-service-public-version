import '../css/home.css';
import img4 from '../assets/images/home-image4.jpg'
import img2 from '../assets/images/home-image2.jpg'
import img3 from '../assets/images/home-image3.jpg'

const Home = () => {
    return ( 
        <div className="home-component">
            <div className="home-container">
                <section className="home-section"> 
                    <img className="home-image1" src={img2} alt="home-image2" />
                    <p className="home-section-content"> Enhance your organization meeting experience by rating your user’s engagement  </p>
                </section>
                <section className="home-section"> 
                    <img className="home-image2" src={img4} alt="home-image4" />
                    <p className="home-section-content"> Get AI generated questions to measure user engagement </p>
                </section>
                <section className="home-section"> 
                    <img className="home-image3" src={img3} alt="home-image3" />
                    <p className="home-section-content"> Review previous quiz scores, user report, get tips and recommendations </p>
                </section>                
            </div>
        </div>
     );
}
 
export default Home;