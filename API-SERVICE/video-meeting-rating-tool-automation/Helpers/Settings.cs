﻿using video_meeting_rating_tool_api.Enums;

namespace video_meeting_rating_tool_automation.Helpers
{
    public class Settings
    {
        public const EnvironmentTypes GlobalEnvironment = EnvironmentTypes.Debug;
        public string UserName = Environment.UserName;
        public string UserDomainName = Environment.UserDomainName;
        public string BaseDir = Environment.CurrentDirectory + @"..\..\..\";

        public string WebApiBaseUri = string.Empty;
        public string ConnectionString = string.Empty;

        public Settings()
        {
            switch (GlobalEnvironment)
            {
                case EnvironmentTypes.Debug:
                    WebApiBaseUri = "https://localhost:7198";
                    ConnectionString = "Data Source=(LocalDb)\\MSSQLLocalDB; Initial Catalog=MeetingQuest; Integrated Security=True";
                    break;
                case EnvironmentTypes.Develop:
                    WebApiBaseUri = "";
                    ConnectionString = "";
                    break;
                case EnvironmentTypes.UAT:
                    WebApiBaseUri = "";
                    ConnectionString = "";
                    break;
                case EnvironmentTypes.Production:
                    WebApiBaseUri = "";
                    ConnectionString = "";
                    break;
                default:
                    break;
            }
        }
    }
}
