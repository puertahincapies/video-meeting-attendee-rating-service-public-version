﻿using System.ComponentModel.DataAnnotations;

namespace video_meeting_rating_tool_api.Models.Dtos.AccessTokenDto
{
    public class QuizAddAccessTokenDto
    {
        [Required]
        public int QuizId { get; set; }
        [Required]
        public int MeetingId { get; set; }
        [Required]
        public int UserId { get; set; }
    }
}
