﻿using System.ComponentModel.DataAnnotations;

namespace video_meeting_rating_tool_api.Models.Dtos.DocumentDto
{
    public class UpdateDocumentDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public IFormFile Content { get; set; }
        [Required]
        public int MeetingId { get; set; }
    }
}
