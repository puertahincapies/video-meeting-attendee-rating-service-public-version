﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using video_meeting_rating_tool_api.Enums;

namespace video_meeting_rating_tool_api.Models
{
    [Table("Errors")]
    public class Error
    {
        [Key]
        public int ErrorId { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorType { get; set; }
        public string StackTrace { get; set; }
        public DateTime TimeStamp { get; set; } = DateTime.Now;
    }

    public class LogError
    {
        public string Message { get; set; }
    }
}
