﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using video_meeting_rating_tool_api.Enums;
using video_meeting_rating_tool_api.Exceptions;
using video_meeting_rating_tool_api.Helpers;
using video_meeting_rating_tool_api.Models;
using video_meeting_rating_tool_api.Models.Dtos.AccessTokenDto;
using video_meeting_rating_tool_api.Models.Dtos.QuizDto;
using video_meeting_rating_tool_api.Models.Dtos.QuizResultDto;
using video_meeting_rating_tool_api.Repository;

namespace video_meeting_rating_tool_api.Controllers
{
    [ApiController]
    [EnableCors("MeetingQuest_CorsPolicy")]
    [Route("api/v1/meetingquest/[controller]")]
    public class QuizController : ControllerBase
    {
        private readonly ILogger<QuizController> _logger;
        private readonly IMeetingQuestRepository _repository;
        private readonly IConfiguration _configuration;
        private AwsCloudWatchClient _cloudWatchClient;

        // using repository pattern
        public QuizController(ILogger<QuizController> logger, IMeetingQuestRepository repository, IConfiguration configuration)
        {
            _logger = logger;
            _repository = repository;
            _configuration = configuration;
            _cloudWatchClient = new AwsCloudWatchClient(_configuration);
        }

        /// <summary>
        /// Calling ChatGPT endpoint to generate a multiple choice quiz
        /// 
        /// Response verification:
        /// 1) Created a very specific prompt, requesting only 'main' topic
        /// 2) Perform self consistency check
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<ActionResult<QuizDto>> GenerateQuiz([FromBody]AddQuizDto body)
        {
            try
            {
                if(body == null)
                {
                    return BadRequest("Required meeting and document information not provided");
                }

                var uri = _configuration["ChatGPTAPIUri"];
                var token = _configuration["OpenAIToken"];

                // 1. read from uploaded file
                var document = _repository.GetDocument(body.DocumentId);

                if (document == null)
                {
                    return NotFound("Document not found");
                }

                string text = ReadFromDocument(document.Content, document.Name);

                // 2. send prompt to chatgpt
                var prompt = $@"Based on the following transcript generate 5 multiple choice questions with ONLY 3 potential answers ONLY about the main topic, ignore irrelavent topics 
                                and add 'END' after each multiple choice question 
                                and use 1) 2) 3) 4) 5) format to label each multiple choice question 
                                and use a) b) c) format to label each of the answers 
                                and mark the most accurate answer with '***' right next to it 
                                transcript: '{text}'";

                // 3. create quiz
                var isQuizComplete = false;
                ChatGPTResponse result = null;
                Quiz quizObj = null;
                var limit = 5;
                // loop - to force expected quiz - heavy process (to be revised)
                while(!isQuizComplete && limit > 0)
                {
                    // try at most 3 times
                    result = await ChatGPTGenerateQuiz(prompt, uri, token);

                    // if after 3 tries, there's still an issue then send error message
                    if (result == null)
                    {
                        _logger.LogError("An error occurred when making a request to ChatGPT");
                        return Problem("An error occurred when making a request to ChatGPT");
                    }

                    quizObj = CreateQuiz(result.Choices.FirstOrDefault().Message.Content, body);

                    // final validation -- fix for when question options return as null
                    // Question #1
                    if (string.IsNullOrEmpty(quizObj.Question1Answer1) || string.IsNullOrEmpty(quizObj.Question1Answer2) || string.IsNullOrEmpty(quizObj.Question1Answer3))
                    {
                        isQuizComplete = false;
                        limit--;
                        continue;
                    }
                    // Question #2
                    if (string.IsNullOrEmpty(quizObj.Question2Answer1) || string.IsNullOrEmpty(quizObj.Question2Answer2) || string.IsNullOrEmpty(quizObj.Question2Answer3))
                    {
                        isQuizComplete = false;
                        limit--;
                        continue;
                    }
                    // Question #3 
                    if (string.IsNullOrEmpty(quizObj.Question3Answer1) || string.IsNullOrEmpty(quizObj.Question3Answer2) || string.IsNullOrEmpty(quizObj.Question3Answer3))
                    {
                        isQuizComplete = false;
                        limit--;
                        continue;
                    }
                    // Question #4
                    if (string.IsNullOrEmpty(quizObj.Question4Answer1) || string.IsNullOrEmpty(quizObj.Question4Answer2) || string.IsNullOrEmpty(quizObj.Question4Answer3))
                    {
                        isQuizComplete = false;
                        limit--;
                        continue;
                    }
                    // Question #5
                    if (string.IsNullOrEmpty(quizObj.Question5Answer1) || string.IsNullOrEmpty(quizObj.Question5Answer2) || string.IsNullOrEmpty(quizObj.Question5Answer3))
                    {
                        isQuizComplete = false;
                        limit--;
                        continue;
                    }

                    isQuizComplete = true;
                }

                // perform self-consistency check: making sure answers (source of truth) received from ChatGPT is consistent
                var isConsistent = SelfConsistencyCheck(quizObj, body, text);
                quizObj.IsConsistent = isConsistent ? 0 : 1;

                var quiz = _repository.SaveQuiz(quizObj);

                // add entry to user quiz table for host
                if(quiz.UserId == body.UserId)
                {
                    var userQuiz = new UserQuizzes() {
                        UserId = body.UserId,
                        QuizId = quiz.QuizId
                    };
                    _repository.AddUserQuiz(userQuiz);
                }

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"Quiz was successfully generated. {JsonConvert.SerializeObject(quiz.AsDto())}");
                return Ok(quiz?.AsDto());
            }
            catch(UnableToSaveQuizException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Conflict(new SafeResponse() { Message = "Quiz was not found" });
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        [HttpGet("[action]/{quizId}")]
        public async Task<ActionResult<QuizDto>> GetQuiz(int quizId)
        {
            try
            {
                if(quizId == 0)
                {
                    return NotFound();
                }

                var quiz = _repository.GetQuiz(quizId);
                await _cloudWatchClient.PublishLogToAwsCloudWatch($"Quiz was successfully retrieved. {JsonConvert.SerializeObject(quiz.AsDto())}");

                return Ok(quiz.AsDto());
            }
            catch(QuizNotFoundException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return NotFound(new SafeResponse() { Message = "Quiz was not found" });
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        [HttpGet("[action]/{userId}")]
        public async Task<ActionResult<IEnumerable<QuizDto>>> GetUserQuizzes(int userId)
        {
            try
            {
                if (userId == 0)
                {
                    return BadRequest("Null or empty userId");
                }

                var userQuizzes = _repository.GetUserQuizzes(userId)?.ToList();
                var quizzes = new List<Quiz>();
                if(userQuizzes != null)
                {
                    foreach (var userQuiz in userQuizzes)
                    {
                        var quiz = _repository.GetQuiz(userQuiz.QuizId);
                        if (quiz != null)
                        {
                            var quizResult = _repository.GetQuizResult(userQuiz.QuizId, userQuiz.UserId);
                            if (quizResult == null || quizResult?.Status != "Submitted")
                            {
                                quizzes.Add(quiz);
                            }
                        }
                    }
                }

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"{userQuizzes.Count()} quizzes were successfully retrieved for a specific user. UserId: {userId}");
                return Ok(quizzes.Select(x => x.AsDto()));
            }
            catch(QuizNotFoundException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return NotFound(new SafeResponse() { Message = "Quiz was not found" });
            }
            catch(UserQuizNotFoundException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return NotFound(new SafeResponse() { Message = "Quizzes were not found for this user" });
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        [HttpGet("[action]/{userId}")]
        public async Task<ActionResult<IEnumerable<QuizDto>>> GetUserQuizHistory(int userId)
        {
            try
            {
                if (userId == 0)
                {
                    return BadRequest("Null or empty userId");
                }

                var userQuizzes = _repository.GetUserQuizzes(userId)?.ToList();
                var quizzes = new List<Quiz>();
                if(userQuizzes != null)
                {
                    foreach (var userQuiz in userQuizzes)
                    {
                        var quiz = _repository.GetQuiz(userQuiz.QuizId);
                        if (quiz != null)
                        {
                            var quizResult = _repository.GetQuizResult(userQuiz.QuizId, userQuiz.UserId);
                            if (quizResult?.Status == "Submitted")
                            {
                                // update to date that it was submitted
                                quiz.CreateDateTime = quizResult.CreateDateTime;
                                quizzes.Add(quiz);
                            }
                        }
                    }
                }

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"Quiz history was successfully retrieved for a specific user. UserId: {userId}");
                return Ok(quizzes.Select(x => x.AsDto()));
            }
            catch (QuizNotFoundException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return NotFound(new SafeResponse() { Message = "Quiz was not found" });
            }
            catch (UserQuizNotFoundException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return NotFound(new SafeResponse() { Message = "Quizzes were not found for this user" });
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        [HttpGet("[action]/{meetingId}/{quizId}")]
        public async Task<ActionResult> DownloadQuiz(int meetingId, int quizId)
        {
            try
            {
                var quiz = _repository.GetQuiz(quizId);
                var meeting = _repository.GetMeeting(meetingId);
                var fileName = $"MeetingQuestQuiz_{meetingId}_{quizId}";
                var filePath = Path.Combine("wwwroot/PDFs", $"{fileName}.pdf");
                var pdfGenerator = new PdfGenerator();
                pdfGenerator.GenerateQuizPDF(filePath, quiz);
                var stream = new FileStream(filePath, FileMode.Open);

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"Quiz was successfully downloaded. Filename: {fileName}");
                return File(stream, "application/pdf", $"{fileName}.pdf");
            }
            catch(QuizNotFoundException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return NotFound(new SafeResponse() { Message = "Quiz was not found, when attempting to download the quiz" });
            }
            catch (MeetingNotFoundException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return NotFound(new SafeResponse() { Message = "Meeting was not found, when attempting to download the quiz" });
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        [HttpGet("[action]/{filename}")]
        public async Task<ActionResult> DownloadQuiz(string filename)
        {
            try
            {
                var filePath = Path.Combine("wwwroot/PDFs", $"{filename}.pdf");
                var stream = new FileStream(filePath, FileMode.Open);
                //return new FileStreamResult(stream, "application/pdf");

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"Quiz was successfully downloaded. File Path: {filePath}");
                return File(stream, "application/pdf", $"{filename}.pdf");
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> EmailQuiz([FromBody]SendEmail sendEmail)
        {
            try
            {
                // get quiz
                var quiz = _repository.GetQuiz(sendEmail.QuizId);
                var meeting = _repository.GetMeeting(sendEmail.MeetingId);
                var fileName = $"MeetingQuestQuiz_{Guid.NewGuid()}";
                var filePath = Path.Combine("wwwroot/PDFs", $"{fileName}.pdf");
                var pdfGenerator = new PdfGenerator();
                pdfGenerator.GenerateQuizPDF(filePath, quiz);

                if (!string.IsNullOrEmpty(meeting?.MeetingAttendeeEmails))
                {
                    // get users by email and provide access tokens
                    AssociateQuizToUserAndProvideAccessTokensAndSendEmailToUsers(meeting, quiz, filePath);
                    await _cloudWatchClient.PublishLogToAwsCloudWatch($"Quiz was successfully emailed to the following emails: {meeting?.MeetingAttendeeEmails}");
                    return Ok("Quiz sent to email list provided");
                }
                return NoContent();
            }
            catch (QuizNotFoundException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return NotFound(new SafeResponse() { Message = "Quiz was not found, when attempting to email the quiz" });
            }
            catch (MeetingNotFoundException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return NotFound(new SafeResponse() { Message = "Meeting was not found, when attempting to email the quiz" });
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> SubmitQuiz([FromBody]AddQuizResultDto addQuizResult)
        {
            try
            {
                if(addQuizResult == null)
                {
                    return BadRequest("No data was submitted");
                }

                // get user
                User user = null;
                if(addQuizResult.UserId > 0)
                {
                    user = _repository.GetUser(addQuizResult.UserId);
                }

                // get quiz
                var quiz = _repository.GetQuiz(addQuizResult.QuizId);

                // if meeting host takes the quiz (override the chatgpt answers)
                if (user?.UserId == quiz.UserId)
                {
                    var updatedQuiz = new Quiz()
                    {
                        QuizId = quiz.QuizId,
                        DocumentId = quiz.DocumentId,
                        MeetingId = quiz.MeetingId,
                        UserId = quiz.UserId,
                        Question1 = quiz.Question1,
                        Question2 = quiz.Question2,
                        Question3 = quiz.Question3,
                        Question4 = quiz.Question4,
                        Question5 = quiz.Question5,
                        Question1Answer1 = quiz.Question1Answer1,
                        Question1Answer2 = quiz.Question1Answer2,
                        Question1Answer3 = quiz.Question1Answer3,
                        Question2Answer1 = quiz.Question2Answer1,
                        Question2Answer2 = quiz.Question2Answer2,
                        Question2Answer3 = quiz.Question2Answer3,
                        Question3Answer1 = quiz.Question3Answer1,
                        Question3Answer2 = quiz.Question3Answer2,
                        Question3Answer3 = quiz.Question3Answer3,
                        Question4Answer1 = quiz.Question4Answer1,
                        Question4Answer2 = quiz.Question4Answer2,
                        Question4Answer3 = quiz.Question4Answer3,
                        Question5Answer1 = quiz.Question5Answer1,
                        Question5Answer2 = quiz.Question5Answer2,
                        Question5Answer3 = quiz.Question5Answer3,
                        Question1CorrectAnswer = addQuizResult.Question1Response,
                        Question2CorrectAnswer = addQuizResult.Question2Response,
                        Question3CorrectAnswer = addQuizResult.Question3Response,
                        Question4CorrectAnswer = addQuizResult.Question4Response,
                        Question5CorrectAnswer = addQuizResult.Question5Response,
                        UUID = quiz.UUID,
                        CreateDateTime = quiz.CreateDateTime
                    };
                    _repository.UpdateQuiz(updatedQuiz);
                }

                // set score
                int score = GetQuizScore(addQuizResult, quiz);

                // set status
                var quizResult = new QuizResult()
                {
                    QuizId = addQuizResult.QuizId,
                    MeetingId = addQuizResult.MeetingId,
                    UserId = addQuizResult.UserId,
                    Role = user?.UserId == quiz.UserId ? RoleTypes.Presenter.ToString() : RoleTypes.Listener.ToString(),
                    Email = user == null ? string.Empty : user.Email,
                    Question1Response = addQuizResult.Question1Response,
                    Question2Response = addQuizResult.Question2Response,
                    Question3Response = addQuizResult.Question3Response,
                    Question4Response = addQuizResult.Question4Response,
                    Question5Response = addQuizResult.Question5Response,
                    Score = score,
                    Status = StatusType.Submitted.ToString()
                };

                // save quiz result
                _repository.AddQuizResult(quizResult);

                // update quiz access token entry
                var accessTokenResult = _repository.GetAccessToken(addQuizResult.UserId, quiz.QuizId, quiz.MeetingId);

                if (accessTokenResult == null)
                {
                    return NotFound("On quiz submit, quiz access token entry not found");
                }

                if (addQuizResult.IsGuest)
                {
                    var updatedQuizAccessTokenEntry = new QuizAccessToken()
                    {
                        QuizAccessTokenId = accessTokenResult.QuizAccessTokenId,
                        QuizId = accessTokenResult.QuizId,
                        MeetingId = accessTokenResult.MeetingId,
                        UserId = accessTokenResult.UserId,
                        Email = accessTokenResult.Email,
                        IsUsed = 1,
                        Token = accessTokenResult.Token,
                        UUID = accessTokenResult.UUID,
                        CreateDateTime = accessTokenResult.CreateDateTime
                    };

                    _repository.UpdateAccessToken(updatedQuizAccessTokenEntry);
                }

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"Quiz was successfully submitted. {JsonConvert.SerializeObject(quizResult.AsDto())}");
                return CreatedAtAction(nameof(GetQuizResult), new { quizResultId = quizResult.QuizResultId }, quizResult.AsDto());
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        [HttpGet("[action]/{quizId}/{userId}")]
        public async Task<ActionResult<QuizResultDto>> GetQuizResult(int quizId, int userId)
        {
            try
            {
                var quizResult = _repository.GetQuizResult(quizId, userId).AsDto();
                if(quizResult == null)
                {
                    return NotFound("Quiz result not found");
                }

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"Quiz result was successfully retrieved. {JsonConvert.SerializeObject(quizResult)}");
                return Ok(quizResult);
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        [HttpGet("[action]/{userId}/{quizId}/{meetingId}")]
        public async Task<ActionResult<string>> GetAccessToken(int userId, int quizId, int meetingId)
        {
            try
            {
                var accessTokenResult = _repository.GetAccessToken(userId, quizId, meetingId).Token;
                if (string.IsNullOrEmpty(accessTokenResult))
                {
                    return NotFound("Access token not found");
                }

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"Access token was successfully retrieved. {accessTokenResult}");
                return Ok(accessTokenResult);
            }
            catch(AccessTokenNotFoundException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return NotFound(new SafeResponse() { Message = "Access token was not found"});
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        [HttpPost("[action]")]
        public async Task<ActionResult<string>> GenerateAccessToken([FromBody] QuizAddAccessTokenDto addAccessToken)
        {
            try
            { 
                if(addAccessToken == null)
                {
                    return BadRequest("No user data provided");
                }

                var accessToken = new QuizAccessToken()
                {
                    UserId = addAccessToken.UserId,
                    QuizId = addAccessToken.QuizId,
                    MeetingId = addAccessToken.MeetingId
                };

                _repository.AddAccessToken(accessToken);

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"Access token was successfully generated.");
                return CreatedAtAction(nameof(GetAccessToken), new { quizAccessTokenId = accessToken.QuizAccessTokenId }, accessToken.AsDto());
            }
            catch (UnableToSaveAccessTokenException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Conflict(new SafeResponse() { Message = "There was an error when generating and saving the access token for this specific user" });
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        [HttpGet("[action]/{token}")]
        public async Task<ActionResult<QuizAccessTokenDto>> ValidateAccessToken(string token)
        {
            try
            {
                token = token.Trim().ToUpper();
                if (string.IsNullOrEmpty(token))
                {
                    return BadRequest("Null or empty token provided");
                }

                var accessTokenResult = _repository.ValidateAccessToken(token);

                if(accessTokenResult == null)
                {
                    return Unauthorized("Incorrect access token provided, user not authorized");
                }

                if (accessTokenResult.IsUsed == 1)
                {
                    return Unauthorized(new { message = "Token has already been used, please request a new access token from meeting host" });
                }

                // update quiz access token
                var updatedQuizAccessTokenEntry = new QuizAccessToken()
                {
                    QuizAccessTokenId = accessTokenResult.QuizAccessTokenId,
                    QuizId = accessTokenResult.QuizId,
                    MeetingId = accessTokenResult.MeetingId,
                    UserId = accessTokenResult.UserId,
                    Email = accessTokenResult.Email,
                    IsUsed = 1,
                    Token = accessTokenResult.Token,
                    UUID = accessTokenResult.UUID,
                    CreateDateTime = accessTokenResult.CreateDateTime
                };
                _repository.UpdateAccessToken(updatedQuizAccessTokenEntry);

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"Access token was successfully validated. {JsonConvert.SerializeObject(accessTokenResult)}");
                return Ok(accessTokenResult);
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        // helper methods
        private string ReadFromDocument(byte[] data, string fileName)
        {
            try
            {
                var filePath = Path.Combine("wwwroot/Transcripts", $"{fileName}_{Guid.NewGuid()}");
                System.IO.File.WriteAllBytes(filePath, data);
                string contents = System.IO.File.ReadAllText(filePath);
                return contents;
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                throw;
            }
        }

        private async Task<ChatGPTResponse> ChatGPTGenerateQuiz(string prompt, string uri, string token)
        {
            ChatGPTResponse result = null; 
            var allowedAttempts = 3;
            while (allowedAttempts > 0)
            {
                try
                {
                    result = ChatGPTClient.SendPromptToChatGPTUsingRestClient(prompt, uri, token);
                    if (result != null) break;
                }
                catch (QuizNotGeneratedException ex)
                {
                    // if there's an error - wait a few seconds and try again
                    _repository.LogError(ex, Enums.ErrorTypes.Warning);
                    _logger.LogError(ex.Message);
                    await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                    Thread.Sleep(3000);
                }
                catch (Exception ex)
                {
                    _repository.LogError(ex, Enums.ErrorTypes.Error);
                    _logger.LogError(ex.Message);
                    await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                    Thread.Sleep(3000);
                }
                allowedAttempts--;
            }
            return result;
        }

        private Quiz CreateQuiz(string content, AddQuizDto body)
        {
            try
            {
                var multipleChoiceQuestions = content.Split("END");

                // Q1
                var multipleChoiceQuestion1 = multipleChoiceQuestions[0].Trim('\n').Split("\n");
                var question1 = multipleChoiceQuestion1[0].Trim();
                var question1Answer1 = multipleChoiceQuestion1[1].Replace("***", "").Trim();
                var question1Answer2 = multipleChoiceQuestion1[2].Replace("***", "").Trim();
                var question1Answer3 = multipleChoiceQuestion1[3].Replace("***", "").Trim();
                var question1CorrectAnswer = multipleChoiceQuestion1.Where(x => x.Contains("***")).FirstOrDefault()?.Replace("***", "").Trim();

                // Q2
                var multipleChoiceQuestion2 = multipleChoiceQuestions[1].Trim('\n').Split("\n");
                var question2 = multipleChoiceQuestion2[0].Trim();
                var question2Answer1 = multipleChoiceQuestion2[1].Replace("***", "").Trim();
                var question2Answer2 = multipleChoiceQuestion2[2].Replace("***", "").Trim();
                var question2Answer3 = multipleChoiceQuestion2[3].Replace("***", "").Trim();
                var question2CorrectAnswer = multipleChoiceQuestion2.Where(x => x.Contains("***")).FirstOrDefault()?.Replace("***", "").Trim();

                // Q3
                var multipleChoiceQuestion3 = multipleChoiceQuestions[2].Trim('\n').Split("\n");
                var question3 = multipleChoiceQuestion3[0].Trim();
                var question3Answer1 = multipleChoiceQuestion3[1].Replace("***", "").Trim();
                var question3Answer2 = multipleChoiceQuestion3[2].Replace("***", "").Trim();
                var question3Answer3 = multipleChoiceQuestion3[3].Replace("***", "").Trim();
                var question3CorrectAnswer = multipleChoiceQuestion3.Where(x => x.Contains("***")).FirstOrDefault()?.Replace("***", "").Trim();

                // Q4
                var multipleChoiceQuestion4 = multipleChoiceQuestions[3].Trim('\n').Split("\n");
                var question4 = multipleChoiceQuestion4[0].Trim();
                var question4Answer1 = multipleChoiceQuestion4[1].Replace("***", "").Trim();
                var question4Answer2 = multipleChoiceQuestion4[2].Replace("***", "").Trim();
                var question4Answer3 = multipleChoiceQuestion4[3].Replace("***", "").Trim();
                var question4CorrectAnswer = multipleChoiceQuestion4.Where(x => x.Contains("***")).FirstOrDefault()?.Replace("***", "").Trim();

                // Q5
                var multipleChoiceQuestion5 = multipleChoiceQuestions[4].Trim('\n').Split("\n");
                var question5 = multipleChoiceQuestion5[0].Trim();
                var question5Answer1 = multipleChoiceQuestion5[1].Replace("***", "").Trim();
                var question5Answer2 = multipleChoiceQuestion5[2].Replace("***", "").Trim();
                var question5Answer3 = multipleChoiceQuestion5[3].Replace("***", "").Trim();
                var question5CorrectAnswer = multipleChoiceQuestion5.Where(x => x.Contains("***")).FirstOrDefault()?.Replace("***", "").Trim();

                var quiz = new Quiz()
                {
                    DocumentId = body.DocumentId,
                    MeetingId = body.MeetingId,
                    UserId = body.UserId,
                    Question1 = question1,
                    Question2 = question2,
                    Question3 = question3,
                    Question4 = question4,
                    Question5 = question5,
                    Question1Answer1 = question1Answer1,
                    Question1Answer2 = question1Answer2,
                    Question1Answer3 = question1Answer3,
                    Question2Answer1 = question2Answer1,
                    Question2Answer2 = question2Answer2,
                    Question2Answer3 = question2Answer3,
                    Question3Answer1 = question3Answer1,
                    Question3Answer2 = question3Answer2,
                    Question3Answer3 = question3Answer3,
                    Question4Answer1 = question4Answer1,
                    Question4Answer2 = question4Answer2,
                    Question4Answer3 = question4Answer3,
                    Question5Answer1 = question5Answer1,
                    Question5Answer2 = question5Answer2,
                    Question5Answer3 = question5Answer3,
                    Question1CorrectAnswer = question1CorrectAnswer,
                    Question2CorrectAnswer = question2CorrectAnswer,
                    Question3CorrectAnswer = question3CorrectAnswer,
                    Question4CorrectAnswer = question4CorrectAnswer,
                    Question5CorrectAnswer = question5CorrectAnswer,
                };

                return quiz;
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                throw;
            }
        }

        private int GetQuizScore(AddQuizResultDto result, Quiz quiz)
        {
            int score = 0;

            if (result.Question1Response == quiz.Question1CorrectAnswer)
            {
                score++;
            }

            if (result.Question2Response == quiz.Question2CorrectAnswer)
            {
                score++;
            }

            if (result.Question3Response == quiz.Question3CorrectAnswer)
            {
                score++;
            }

            if (result.Question4Response == quiz.Question4CorrectAnswer)
            {
                score++;
            }

            if (result.Question5Response == quiz.Question5CorrectAnswer)
            {
                score++;
            }

            return score;
        }

        private void AssociateQuizToUserAndProvideAccessTokensAndSendEmailToUsers(Meeting meeting, Quiz quiz, string filePath)
        {
            // get users
            var attendeeEmails = meeting.MeetingAttendeeEmails?.Trim().Split(";").ToList();
            if (string.IsNullOrEmpty(attendeeEmails.Last())){
                attendeeEmails.RemoveAt(attendeeEmails.Count - 1);
            }
            var userWithAccounts = new List<User>();
            var userWithoutAccounts = new List<string>();
            foreach (var attendeeEmail in attendeeEmails)
            {
                var user = _repository.GetUserByEmail(attendeeEmail.Trim());
                if (user != null)
                {
                    userWithAccounts.Add(user);
                }
                else
                {
                    userWithoutAccounts.Add(attendeeEmail);
                }
            }

            // users with accounts
            foreach (var user in userWithAccounts)
            {
                var accessToken = new QuizAccessToken()
                {
                    UserId = user.UserId,
                    Email = user.Email,
                    QuizId = quiz.QuizId,
                    MeetingId = meeting.MeetingId
                };
                _repository.AddAccessToken(accessToken);

                var body = $"Hi,\n\nPlease navigate to {_configuration["ProdUIUrl"]}/login and sign-in. The quiz should be available under Quizzes. \n\nOtherwise, please navigate to {_configuration["ProdUIUrl"]}/guestaccessportal and use the provided access token: {accessToken.Token} to access the quiz. \n\nUpon completion, please submit the quiz. You will only have one opportunity to take this quiz. \n\nIf you encounter any issues, please reach out to the meeting host. They can resend the link to the email you provide. \n\n - MeetingQuestTeam";
                CreateAndSendEmail(user.Email, body, filePath);

                // add entry to user quizzes table
                var userQuiz = new UserQuizzes()
                {
                    UserId = user.UserId,
                    QuizId = quiz.QuizId
                };
                _repository.AddUserQuiz(userQuiz);
            }

            // users without accounts
            foreach (var userEmail in userWithoutAccounts)
            {
                var latestUserId = -1;
                var latestGuestUser = _repository.GetAccessTokens().OrderBy(x => x.UserId).FirstOrDefault();
                if(latestGuestUser != null)
                {
                    latestUserId = latestGuestUser.UserId - 1;
                }

                var accessToken = new QuizAccessToken()
                {
                    UserId = latestUserId,
                    Email = userEmail,
                    QuizId = quiz.QuizId,
                    MeetingId = meeting.MeetingId
                };
                _repository.AddAccessToken(accessToken);

                var body = $"Hi,\n\nPlease navigate to {_configuration["ProdUIUrl"]}/guestaccessportal and use the provided access token: {accessToken.Token} to access the quiz. \n\nUpon completion, please submit the quiz. You will only have one opportunity to take this quiz. \n\nIf you encounter any issues, please reach out to the meeting host. They can resend the link to the email you provide. \n\n - MeetingQuestTeam";
                CreateAndSendEmail(userEmail, body, filePath);
            }
        }

        private void CreateAndSendEmail(string email, string body, string filePath)
        {
            var createEmail = new Email()
            {
                Username = _configuration["Email"],
                Password = _configuration["EmailPassword"],
                FromEmail = _configuration["Email"],
                ToEmail = email,
                Subject = $"Quiz by MeetingQuest",
                Body = body,
                IsBodyHtml = false,
                //Attachment = new System.Net.Mail.Attachment(filePath)
            };
            EmailClient.SendGoogleEmail(createEmail);
        }

        private bool SelfConsistencyCheck(Quiz quiz, AddQuizDto body, string text)
        {
            try
            {
                var uri = _configuration["ChatGPTAPIUri"];
                var token = _configuration["OpenAIToken"];
                var prompt = $@"Given the following conversation context text and multiple choice quiz, please provide the most accurate answer to each of the provided questions.
                                And only provide the answer to each question.

                                Conversation Context Text: {text}                                
                                Multiple Choice Quiz: 
                                [{quiz.Question1} {quiz.Question1Answer1} {quiz.Question1Answer2} {quiz.Question1Answer3}], 
                                [{quiz.Question2} {quiz.Question2Answer1} {quiz.Question2Answer2} {quiz.Question2Answer3}],
                                [{quiz.Question3} {quiz.Question3Answer1} {quiz.Question3Answer2} {quiz.Question3Answer3}],
                                [{quiz.Question4} {quiz.Question4Answer1} {quiz.Question4Answer2} {quiz.Question4Answer3}], 
                                [{quiz.Question5} {quiz.Question5Answer1} {quiz.Question5Answer2} {quiz.Question5Answer3}]";
                                
                var isConsistent = true;
                var iterations = 2;
                while(iterations > 0)
                {
                    var result = ChatGPTClient.SendPromptToChatGPTUsingRestClient(prompt, uri, token);
                    var content = result.Choices.FirstOrDefault().Message.Content;
                    var answers = content.Split("\n");
                    // only expecting 5 answers (this is a constant value set by us)
                    if(answers.Length == 5)
                    {
                        if (answers[0]?.ToUpper().Replace("1)", "").Trim() != quiz.Question1CorrectAnswer?.ToUpper())
                        {
                            isConsistent = false;
                        }
                        if (answers[1]?.ToUpper().Replace("2)", "").Trim() != quiz.Question2CorrectAnswer?.ToUpper())
                        {
                            isConsistent = false;
                        }
                        if (answers[2]?.ToUpper().Replace("2)", "").Trim() != quiz.Question3CorrectAnswer?.ToUpper())
                        {
                            isConsistent = false;
                        }
                        if (answers[3]?.ToUpper().Replace("4)", "").Trim() != quiz.Question4CorrectAnswer?.ToUpper())
                        {
                            isConsistent = false;
                        }
                        if (answers[4]?.ToUpper().Replace("5)", "").Trim() != quiz.Question5CorrectAnswer?.ToUpper())
                        {
                            isConsistent = false;
                        }
                    }
                    else
                    {
                        isConsistent = false;
                    }
                    iterations--;
                }

                return isConsistent;
            }
            catch(Exception ex)
            {
                // Do Nothing
                return false;
            }
        }
    }
}
