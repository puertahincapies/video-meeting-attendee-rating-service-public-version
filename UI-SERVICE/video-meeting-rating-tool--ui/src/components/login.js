import '../css/login.css';
import { useState } from 'react';
import toast, { Toaster } from 'react-hot-toast';
import { useNavigate } from 'react-router-dom';
import Cookies from 'universal-cookie';
import {USERSESSION, LOGINURL, NAVTOHOME} from '../helpers/constants';
import {setSessionCookie} from '../helpers/utilities';

const Login = ({onLogin, setUserSession}) => {

    const [user, setUser] = useState(null);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const navigate = useNavigate();
    const cookies = new Cookies();

    const handleLogin = (e) => {
        e.preventDefault();
        
        console.log("On click submit");

        fetch(LOGINURL, {
            method: "POST",
            headers: { 'content-type': 'application/json', 'Authorization': 'Basic ' + btoa('admin' + ":" + 'scraps2023') },
            body: JSON.stringify({ token: btoa(email + ":" + password)})
        })
        .then(res => {
            if(!res.ok) {
                throw Error("Could not retrieve data from resource");
            }
            return res.json();
        })
        .then(data => {
            // console.log(data);

            if(data.message == "User has been successfully authenticated"){

                setUser(data);

                toast.success("Welcome back " + data.payload.name, {
                    duration: 5000,
                    position: 'bottom-right'
                })

                // set timeout
                setTimeout(() => { navigate(NAVTOHOME) }, 1000);

                setUserSession(data.payload);

                onLogin(data.payload, true);
            }
        })
        .catch(err => {
            console.log(err);
            toast.error("There was an error!", {
                duration: 3000,
                position: 'bottom-right'
            })
        })
    }

    // const logErrorToDb = (message) => {

    //     let error = {
    //         message: message
    //     }

    //     fetch("https://localhost:7198/api/v1/meetingquest/Error/LogUIErrors", {
    //         method: "POST",
    //         headers: { 'content-type': 'application/json'},
    //         body: JSON.stringify(error)
    //     })
    //     .then(res => {
    //         if(!res.ok){
    //             throw Error("Could not retrieve data from resource");
    //         }
    //         return res.json();
    //     })
    //     .catch(err => {
    //         toast.error("There was an error!", {
    //             duration: 3000,
    //             position: 'bottom-right'
    //         })
    //     })
    // }

    return ( 
        <div className="login-component">
            {/*Only to generate initial form: Using Bootstrap forms, reference: https://getbootstrap.com/docs/4.0/components/forms/*/}
            <form className="login-form" onSubmit={handleLogin}>
                <div className="form-group">
                    <label htmlFor="exampleInputEmail1" className="form-input-label">Email address</label>
                    <input 
                        type="email" 
                        className="form-control" 
                        id="exampleInputEmail1" 
                        aria-describedby="emailHelp" 
                        placeholder="Enter email" 
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                </div><br />
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1" className="form-input-label">Password</label>
                    <input 
                        type="password" 
                        className="form-control" 
                        id="exampleInputPassword1" 
                        placeholder="Password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                </div><br />
                <div className="form-check">
                    <input type="checkbox" className="form-check-input" id="exampleCheck1"></input>
                    <label className="form-check-label" htmlFor="exampleCheck1">Remember me</label>
                </div><br />
                <button type="submit" className="action-button">Submit</button>
            </form>
            <Toaster />
        </div>
     );
}
 
export default Login;