Sean (Product Owner): There was a production issue for Application FireDoc. We need to talk about some of the problems that our end users are facing.   
Monica (Tech Lead): Based on our logs, it looks like the issue is coming from the document upload page when the agent is uploading the statements provided by the customer. I will ask one of our developers to look into this. Please give us some time while we accurately identify the issue and we will keep you posted.    
Monica (Tech Lead): Nishil would you mind taking a look at the logs and check if this is a one time case or if this is an going issue.  
Nishil (Developer): Sure, I will take a look at this. Is this a priority item?      
Sean (Product Owner): The application is down in production due to this issue, this is a high priority item. Monica (Tech Lead): Ok, we are on it. Nishil please start looking at this ASAP.    
Sean (Product Owner): Ok thanks team, I appreciate your time. Please let me know if you have any further questions. Nishil (Developer): Sean can you please provide us with the same document that the agent tried to upload, we will try the same procedure on lower environment and check whether we can replicate the issue in our lower enviroments. Thanks.       
Sean (Producer Owner): Sure Nishil, I will have Danielle our senior data analyst provide those details right away. Monica (Tech Lead): Great I think we are all set. Anything else?     
Nishil (Developer): No, I'm all set - thanks Sean, thanks Monica.   
Sean (Producer Owner): Alright, talk soon. Bye.