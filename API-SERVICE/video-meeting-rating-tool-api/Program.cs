using BasicAuthentication.Settings;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using video_meeting_rating_tool_api.Helpers;
using video_meeting_rating_tool_api.Models;
using video_meeting_rating_tool_api.Models.Interfaces;
using video_meeting_rating_tool_api.Repository;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// SQL Server repository registration
builder.Services.AddDbContextPool<MeetingQuestContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("MeetingQuestContext")));
builder.Services.AddTransient<IMeetingQuestRepository, MeetingQuestRepository>();
builder.Services.AddScoped<IStorageService, StorageService>();

// Mongo repository registration
//builder.Services.AddSingleton<IMongoClient>(serviceProvider =>
//{
//    var settings = builder.Configuration
//                          .GetSection(nameof(MongoDbSettings))
//                          .Get<MongoDbSettings>();
//    return new MongoClient(settings.ConnectionString);
//});

const string MEETINGQUESTCORSPOLICY = "MeetingQuest_CorsPolicy";

builder.Services.AddCors(options =>
{
    options.AddPolicy(name: MEETINGQUESTCORSPOLICY,
        policy =>
        {
            //policy.WithOrigins(
            //    "http://localhost:3000",
            //    "http://localhost:44314",
            //    "http://localhost:44314/meetingquest",
            //    //"http://3.88.125.187:44314",
            //    //"http://3.88.125.187:44314/meetingquest",
            //    //"http://ec2-3-88-125-187.compute-1.amazonaws.com:44314",
            //    //"http://ec2-3-88-125-187.compute-1.amazonaws.com:44314/meetingquest",
            //    //"http://34.203.99.99:44314",
            //    //"http://34.203.99.99:44314/meetingquest",
            //    //"http://ec2-34-203-99-99.compute-1.amazonaws.com:44314",
            //    //"http://ec2-34-203-99-99.compute-1.amazonaws.com:44314/meetingquest",
            //    "http://18.210.0.2:44314",
            //    "http://ec2-18-210-0-2.compute-1.amazonaws.com:44314",
            //    "http://18.210.0.2:44314/meetingquest",
            //    "http://ec2-18-210-0-2.compute-1.amazonaws.com:44314/meetingquest"
            //    )
            //    .AllowAnyMethod()
            //    .AllowAnyHeader();
            policy.AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader();
        });
});

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();

builder.Services.AddAuthentication()
    .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>("BasicAuthentication", options => { });

builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("BasicAuthentication",
        new AuthorizationPolicyBuilder("BasicAuthentication").RequireAuthenticatedUser().Build());
});

builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "MeetingQuest", Version = "v1" });
});

builder.Services.ConfigureCors();

//==================================================================================================================================
var app = builder.Build();
var env = app.Environment;

// Configure the HTTP request pipeline.
if (env.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "MeetingQuest v1"));
}

app.UseExceptionHandler("/error");

app.UseHttpsRedirection();

app.UseRouting();

app.UseCors(MEETINGQUESTCORSPOLICY);

app.UseAuthentication();

app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
    endpoints.MapControllerRoute(
        "default", "{controller=Home}/{action=Index}/{id?}");
});

app.Run();
