import '../css/unauthorized.css';

const Unauthorized = () => {
    return ( 
        <div className="unauthorized-component">
            <h1 className="unauthorized-message">Unauthorized Access</h1>
        </div> 
    );
}
 
export default Unauthorized;