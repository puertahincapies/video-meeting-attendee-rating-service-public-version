﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace video_meeting_rating_tool_api.Models.Interfaces
{
    public interface IReport
    {        
        public int UserId { get; set; }
        public int PresenterScore { get; set; }
        public int ListenerScore { get; set; }
        public int HostMeetingCount { get; set; }
        public int QuizCount { get; set; }
        public int AttendeeMeetingCount {get; set;}
        public string SentimentScore {get; set;}
        public Dictionary<int, int> ListenerScoreQuizList {get; set;}
        public Dictionary<int, int> PresenterScoreQuizList {get; set;}
        public string PresentationFeedback {get; set;}
        public string UUID { get; set; } 
        public DateTime CreateDateTime { get; set; } 
    }
}
