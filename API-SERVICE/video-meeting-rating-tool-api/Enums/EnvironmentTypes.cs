﻿namespace video_meeting_rating_tool_api.Enums
{
    public enum EnvironmentTypes
    {
        Debug,
        Develop,
        UAT,
        Production
    }
}
