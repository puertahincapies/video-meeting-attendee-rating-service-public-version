﻿namespace video_meeting_rating_tool_api.Enums
{
    public enum UserTypes
    {
        None = 0,
        Admin = 1,
        Attendee = 2,
        Host = 3,
        Guest = 4
    }
}
