using System.Text.Json.Serialization;

namespace video_meeting_rating_tool_api.Models.Dtos.ReportDto
{
    public class ReportDto
    {        
        public int UserId { get; set; }
        public int PresenterScore { get; set; }
        public int ListenerScore { get; set; }
        public int HostMeetingCount { get; set; }
        public int QuizCount { get; set; }
        public int AttendeeMeetingCount {get; set;}
        public string SentimentScore {get; set;}
        public Dictionary<int, int> ListenerScoreQuizList {get; set;}
        public Dictionary<int, int> PresenterScoreQuizList {get; set;}
        public List<DocumentSentimentResult> DocumentSentimentResults { get; set; }
        public string PresentationFeedback {get; set;}        
        [JsonIgnore]
        public string UUID { get; set; } = Guid.NewGuid().ToString();
        [JsonIgnore]
        public DateTime CreateDateTime { get; set; } = DateTime.Now;
    }
}