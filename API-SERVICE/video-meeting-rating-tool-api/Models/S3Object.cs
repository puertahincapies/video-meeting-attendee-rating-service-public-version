﻿namespace video_meeting_rating_tool_api.Models
{
    public class S3Object
    {
        public string Name { get; set; } = string.Empty;
        public MemoryStream InputStream { get; set; }
        public string BucketName { get; set; } = string.Empty;

    }
}
