﻿namespace video_meeting_rating_tool_api.Exceptions
{
    public class UnableToSaveQuizException : Exception
    {
        public UnableToSaveQuizException() { }
        public UnableToSaveQuizException(string message) : base(message) { }
        public UnableToSaveQuizException(string message, Exception innerException) : base(message, innerException) { }
    }
}
