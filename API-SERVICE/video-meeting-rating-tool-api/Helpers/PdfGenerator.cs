﻿using ceTe.DynamicPDF;
using ceTe.DynamicPDF.PageElements;
using ceTe.DynamicPDF.PageElements.BarCoding;
using video_meeting_rating_tool_api.Models;

namespace video_meeting_rating_tool_api.Helpers
{
    public class PdfGenerator
    {
        private float x;

        public PdfGenerator()
        {
            x = 0;
        }

        public void GenerateSamplePDF(string filepath)
        {
            ceTe.DynamicPDF.Document document = new ceTe.DynamicPDF.Document();

            Page page = new Page(PageSize.Letter, PageOrientation.Portrait, 54.0f);
            document.Pages.Add(page);

            string labelText = "Hello World";
            Label label = new Label(labelText, 0, 0, 504, 100, Font.Helvetica, 18, TextAlign.Center);
            page.Elements.Add(label);

            document.Draw(filepath);
        }

        public void GenerateQuizPDF(string filepath, Quiz quiz)
        {
            try
            {
                ceTe.DynamicPDF.Document document = new ceTe.DynamicPDF.Document();

                Page page = new Page(PageSize.Letter, PageOrientation.Portrait, 54.0f);
                document.Pages.Add(page);

                // create elements: quiz template
                float width = 504;
                float height = 100;
                float y = 0;
                float answerY = 25;
                float questionFontSize = 14;
                float answerFontSize = 12;

                Label title = new Label("MeetingQuest", y, this.x, width, height, Font.TimesBold, 32, TextAlign.Center);

                var question1 = new TextArea(quiz.Question1, y, SetXValue(50), width, height, Font.TimesBold, questionFontSize, TextAlign.Left);
                var question1Answer1 = new TextArea(quiz.Question1Answer1, answerY, SetXValue(25), width, height, quiz.Question1CorrectAnswer == quiz.Question1Answer1 ? Font.TimesBold : Font.TimesRoman, answerFontSize, TextAlign.Left, quiz.Question1CorrectAnswer == quiz.Question1Answer1 ? WebColor.Red : WebColor.Black);
                var question1Answer2 = new TextArea(quiz.Question1Answer2, answerY, SetXValue(25), width, height, quiz.Question1CorrectAnswer == quiz.Question1Answer2 ? Font.TimesBold : Font.TimesRoman, answerFontSize, TextAlign.Left, quiz.Question1CorrectAnswer == quiz.Question1Answer2 ? WebColor.Red : WebColor.Black);
                var question1Answer3 = new TextArea(quiz.Question1Answer3, answerY, SetXValue(25), width, height, quiz.Question1CorrectAnswer == quiz.Question1Answer3 ? Font.TimesBold : Font.TimesRoman, answerFontSize, TextAlign.Left, quiz.Question1CorrectAnswer == quiz.Question1Answer3 ? WebColor.Red : WebColor.Black);

                var question2 = new TextArea(quiz.Question2, y, SetXValue(50), width, height, Font.TimesBold, questionFontSize, TextAlign.Left);
                var question2Answer1 = new TextArea(quiz.Question2Answer1, answerY, SetXValue(25), width, height, quiz.Question2CorrectAnswer == quiz.Question2Answer1 ? Font.TimesBold : Font.TimesRoman, answerFontSize, TextAlign.Left, quiz.Question2CorrectAnswer == quiz.Question2Answer1 ? WebColor.Red : WebColor.Black);
                var question2Answer2 = new TextArea(quiz.Question2Answer2, answerY, SetXValue(25), width, height, quiz.Question2CorrectAnswer == quiz.Question2Answer2 ? Font.TimesBold : Font.TimesRoman, answerFontSize, TextAlign.Left, quiz.Question2CorrectAnswer == quiz.Question2Answer2 ? WebColor.Red : WebColor.Black);
                var question2Answer3 = new TextArea(quiz.Question2Answer3, answerY, SetXValue(25), width, height, quiz.Question2CorrectAnswer == quiz.Question2Answer3 ? Font.TimesBold : Font.TimesRoman, answerFontSize, TextAlign.Left, quiz.Question2CorrectAnswer == quiz.Question2Answer3 ? WebColor.Red : WebColor.Black);

                var question3 = new TextArea(quiz.Question3, y, SetXValue(50), width, height, Font.TimesBold, questionFontSize, TextAlign.Left);
                var question3Answer1 = new TextArea(quiz.Question3Answer1, answerY, SetXValue(25), width, height, quiz.Question3CorrectAnswer == quiz.Question3Answer1 ? Font.TimesBold : Font.TimesRoman, answerFontSize, TextAlign.Left, quiz.Question3CorrectAnswer == quiz.Question3Answer1 ? WebColor.Red : WebColor.Black);
                var question3Answer2 = new TextArea(quiz.Question3Answer2, answerY, SetXValue(25), width, height, quiz.Question3CorrectAnswer == quiz.Question3Answer2 ? Font.TimesBold : Font.TimesRoman, answerFontSize, TextAlign.Left, quiz.Question3CorrectAnswer == quiz.Question3Answer2 ? WebColor.Red : WebColor.Black);
                var question3Answer3 = new TextArea(quiz.Question3Answer3, answerY, SetXValue(25), width, height, quiz.Question3CorrectAnswer == quiz.Question3Answer3 ? Font.TimesBold : Font.TimesRoman, answerFontSize, TextAlign.Left, quiz.Question3CorrectAnswer == quiz.Question3Answer3 ? WebColor.Red : WebColor.Black);

                var question4 = new TextArea(quiz.Question4, y, SetXValue(50), width, height, Font.TimesBold, questionFontSize, TextAlign.Left);
                var question4Answer1 = new TextArea(quiz.Question4Answer1, answerY, SetXValue(25), width, height, quiz.Question4CorrectAnswer == quiz.Question4Answer1 ? Font.TimesBold : Font.TimesRoman, answerFontSize, TextAlign.Left, quiz.Question4CorrectAnswer == quiz.Question4Answer1 ? WebColor.Red : WebColor.Black);
                var question4Answer2 = new TextArea(quiz.Question4Answer2, answerY, SetXValue(25), width, height, quiz.Question4CorrectAnswer == quiz.Question4Answer2 ? Font.TimesBold : Font.TimesRoman, answerFontSize, TextAlign.Left, quiz.Question4CorrectAnswer == quiz.Question4Answer2 ? WebColor.Red : WebColor.Black);
                var question4Answer3 = new TextArea(quiz.Question4Answer3, answerY, SetXValue(25), width, height, quiz.Question4CorrectAnswer == quiz.Question4Answer3 ? Font.TimesBold : Font.TimesRoman, answerFontSize, TextAlign.Left, quiz.Question4CorrectAnswer == quiz.Question4Answer3 ? WebColor.Red : WebColor.Black);

                var question5 = new TextArea(quiz.Question5, y, SetXValue(50), width, height, Font.TimesBold, questionFontSize, TextAlign.Left);
                var question5Answer1 = new TextArea(quiz.Question5Answer1, answerY, SetXValue(25), width, height, quiz.Question5CorrectAnswer == quiz.Question5Answer1 ? Font.TimesBold : Font.TimesRoman, answerFontSize, TextAlign.Left, quiz.Question5CorrectAnswer == quiz.Question5Answer1 ? WebColor.Red : WebColor.Black);
                var question5Answer2 = new TextArea(quiz.Question5Answer2, answerY, SetXValue(25), width, height, quiz.Question5CorrectAnswer == quiz.Question5Answer2 ? Font.TimesBold : Font.TimesRoman, answerFontSize, TextAlign.Left, quiz.Question5CorrectAnswer == quiz.Question5Answer2 ? WebColor.Red : WebColor.Black);
                var question5Answer3 = new TextArea(quiz.Question5Answer3, answerY, SetXValue(25), width, height, quiz.Question5CorrectAnswer == quiz.Question5Answer3 ? Font.TimesBold : Font.TimesRoman, answerFontSize, TextAlign.Left, quiz.Question5CorrectAnswer == quiz.Question5Answer3 ? WebColor.Red : WebColor.Black);

                // add elements
                page.Elements.Add(title);
                page.Elements.Add(question1);
                page.Elements.Add(question1Answer1);
                page.Elements.Add(question1Answer2);
                page.Elements.Add(question1Answer3);
                page.Elements.Add(question2);
                page.Elements.Add(question2Answer1);
                page.Elements.Add(question2Answer2);
                page.Elements.Add(question2Answer3);
                page.Elements.Add(question3);
                page.Elements.Add(question3Answer1);
                page.Elements.Add(question3Answer2);
                page.Elements.Add(question3Answer3);
                page.Elements.Add(question4);
                page.Elements.Add(question4Answer1);
                page.Elements.Add(question4Answer2);
                page.Elements.Add(question4Answer3);
                page.Elements.Add(question5);
                page.Elements.Add(question5Answer1);
                page.Elements.Add(question5Answer2);
                page.Elements.Add(question5Answer3);

                // build
                document.Draw(filepath);
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        private float SetXValue(int x)
        {
            this.x += x;
            return this.x;
        }
    }
}
