﻿using System.Net.Mail;

namespace video_meeting_rating_tool_api.Models
{
    public class Email
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string FromEmail { get; set; }
        public string ToEmail { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsBodyHtml { get; set; }
        public Attachment Attachment { get; set; }

    }
}
