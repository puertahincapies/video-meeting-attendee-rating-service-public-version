import '../css/aboutus.css';
import img2 from '../assets/images/aboutus-image2.jpg';
import img1 from '../assets/images/aboutus-image1.jpg';
import { useNavigate } from 'react-router-dom';
import { NAVTOHOME } from '../helpers/constants';
 
const AboutUs = () => {
  const navigate = useNavigate();

    return (
        <div className="about-us-component">
          <label className="component-header">About Us</label>
          <div className="about-us-container">
            <section className="about-us-section">
              <img className="about-us-image1" src={img1} alt="about-us-image1" />
              <div className="about-us-section-content">
                <ul className="about-us-item-list">
                  <li>Having trouble engaging with your remote staff?</li><br/>
                  <li>Looking for a way to measure the effectiveness of your meetings?</li><br/>
                  <li>Curious about improving collaboration and communication within your team?</li><br/>
                </ul>
              </div>
            </section>
            <section className="about-us-section">
              <img className="about-us-image2" src={img2} alt="about-us-image2" />
              <div className="about-us-section-content">
              <ul className="about-us-item-list">
                  <li>Meeting Quest is a premier remote meeting engagement tool.</li><br/>
                  <li>Automatically generate tests to quiz your meeting participants. </li><br/>
                  <li>Identify miscommunications and misunderstandings with the blink of an eye.</li><br/>
                  <li> Get personalized coaching on how to improve your presentation skills for your team.</li>
                </ul>
              </div>
            </section>
          </div><br/>
          <button type="button" className="action-button" id="getStartedButton" onClick={() => navigate(NAVTOHOME)}>
            Get Started Now!
          </button>
        </div>
      );
}

export default AboutUs;