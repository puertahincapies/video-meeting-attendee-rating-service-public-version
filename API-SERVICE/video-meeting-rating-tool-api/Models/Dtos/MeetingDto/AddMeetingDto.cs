﻿namespace video_meeting_rating_tool_api.Models.Dtos.MeetingDto
{
    public class AddMeetingDto
    {
        public string? MeetingUUID { get; set; }
        public string MeetingName { get; set; }
        public DateTime MeetingDateTime { get; set; }
        public string? MeetingAttendeeEmails { get; set; }
        public int UserId { get; set; }
    }
}
