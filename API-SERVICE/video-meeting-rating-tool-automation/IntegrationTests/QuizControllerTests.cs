﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using video_meeting_rating_tool_automation.Helpers;

namespace video_meeting_rating_tool_automation.IntegrationTests
{
    [TestFixture]
    public class QuizControllerTests
    {
        Settings settings;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            settings = new Settings();
        }

        [SetUp]
        public void Setup() { /*Do nothing*/ }

        [TearDown]
        public void TearDown() { /*Do nothing*/ }

        [OneTimeTearDown]
        public void OneTimeTearDown() { /*Do nothing*/ }

        [Test]
        public void Quiz_GenerateQuiz()
        {
            Assert.Ignore();
        }

        [Test]
        public void Quiz_GetQuiz()
        {
            Assert.Ignore();
        }

        [Test]
        public void Quiz_GetUserQuizzes()
        {
            Assert.Ignore();
        }

        [Test]
        public void Quiz_DownloadQuiz()
        {
            Assert.Ignore();
        }

        [Test]
        public void Quiz_DownloadQuizByFileName()
        {
            Assert.Ignore();
        }

        [Test]
        public void Quiz_EmailQuiz()
        {
            Assert.Ignore();
        }

        [Test]
        public void Quiz_SubmitQuiz()
        {
            Assert.Ignore();
        }

        [Test]
        public void Quiz_GetQuizResult()
        {
            Assert.Ignore();
        }

        [Test]
        public void Quiz_GetAccessToken()
        {
            Assert.Ignore();
        }

        [Test]
        public void Quiz_GenerateAccessToken()
        {
            Assert.Ignore();
        }

        [Test]
        public void Quiz_ValidateAccessToken()
        {
            Assert.Ignore();
        }
    }
}
