﻿namespace video_meeting_rating_tool_api.Exceptions
{
    public class QuizNotFoundException : Exception
    {
        public QuizNotFoundException() { }
        public QuizNotFoundException(string message) : base(message) { }
        public QuizNotFoundException(string message, Exception innerException) : base(message, innerException) { }
    }
}
