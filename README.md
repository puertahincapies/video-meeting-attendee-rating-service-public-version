# Getting Started with MeetingQuest
Clone Project

## Summary
Video meeting engagement tool

Generate Quizzes by providing meeting transcript

Quiz scores are used to measure attendees (listeners) engagement and hosts (presenters) effectiveness

## UI Service
Node.js, React v18, HTML5, CSS

### How to set up project
Using your preferred code editor

Navigate to UI Service directory

Navigate to where project package.json file located in your system

npm install

npm run start

## API Service
.NET6, C#, WebAPI Project, NUnit Project (integration tests), XUnit Project (unit tests)

### How to set up project
Download and install Visual Studio

Open up project, navigate to API Service and click on .sln file

Restore nuget packages

Clean Solution

Build Solution

Run Solution

## Testing Strategy
UI Functional Testing

API Unit Testing

API Integration Testing

Regression after each sprint

## Deployment Strategy
Gitlab + AWS EC2

## Team
Scott Berlow, James Hinton, Sebastian Puerta Hincapie
