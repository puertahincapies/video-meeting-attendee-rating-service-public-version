// COOKIE KEYS
export const USERSESSION = "USER_SESSION";
export const USERLOGIN = "USER_LOGIN"

// // URLS
export const BASEURI = "https://localhost:7198"; 
// export const BASEURI = "https://meetingquest.azurewebsites.net";    // (Production - Azure Cloud Service)
export const SAVEMEETINGURL = BASEURI + "/api/v1/meetingquest/Meeting/SaveMeetingDetails";
export const UPLOADDOCUMENTURL = BASEURI + "/api/v1/meetingquest/Document/UploadDocument";
export const GENERATEQUIZURL = BASEURI + "/api/v1/meetingquest/Quiz/GenerateQuiz";
export const LOGERRORSURL = BASEURI + "/api/v1/meetingquest/Error/LogUIErrors";
export const GETQUIZURL = BASEURI + "/api/v1/meetingquest/Quiz/GetQuiz/";
export const SUBMITQUIZURL = BASEURI + "/api/v1/meetingquest/Quiz/SubmitQuiz";
export const DOWNLOADQUIZURL = BASEURI + "/api/v1/meetingquest/Quiz/DownloadQuiz/";
export const EMAILQUIZURL = BASEURI + "/api/v1/meetingquest/Quiz/EmailQuiz";
export const LOGINURL = BASEURI + "/api/v1/meetingquest/User/Login";
export const GETUSERQUIZZESURL = BASEURI + "/api/v1/meetingquest/Quiz/GetUserQuizzes";
export const GETUSERQUIZHISTORYURL = BASEURI + "/api/v1/meetingquest/Quiz/GetUserQuizHistory";
export const GETQUIZRESULTURL = BASEURI + "/api/v1/meetingquest/Quiz/GetQuizResult";
export const SIGNUPURL = BASEURI + "/api/v1/meetingquest/User/SignUp";
export const VALIDATEACCESSTOKENURL = BASEURI + "/api/v1/meetingquest/Quiz/ValidateAccessToken/";
export const CHANGEUSER = BASEURI + "/api/v1/meetingquest/User/ChangeUser";
export const GETUSER = BASEURI + "/api/v1/meetingquest/User/GetUser"
export const GETUSERREPORT = BASEURI + "/api/v1/meetingquest/Report/GetUserReport"

// NAVIGATE
export const NAVTOGENERATEDQUIZ = "/meetingquest/generatedquiz";
export const NAVTOUNAUTHORIZE = "/meetingquest/unauthorized";
export const NAVTOHOME = "/meetingquest/home";
export const NAVTOLOGIN = "/meetingquest/login";
export const NAVTOUPLOAD = "/meetingquest/upload";
export const NAVTOSIGNUP = "/meetingquest/signup";
export const NAVTOQUIZZES = "/meetingquest/quizzes";
export const NAVTOQUIZHISTORY = "/meetingquest/quizhistory";
export const NAVTOQUIZRESULT = "/meetingquest/quizresults";
export const NAVTOABOUTUS = "/meetingquest/aboutus";
export const NAVTOUSERACCOUNT = "/meetingquest/useraccount";
export const NAVTOREPORT = "/meetingquest/report";

