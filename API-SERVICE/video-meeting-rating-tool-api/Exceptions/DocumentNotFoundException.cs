﻿namespace video_meeting_rating_tool_api.Exceptions
{
    public class DocumentNotFoundException : Exception
    {
        public DocumentNotFoundException() { }
        public DocumentNotFoundException(string message) : base(message) { }
        public DocumentNotFoundException(string message, Exception innerException) : base(message, innerException) { }
    }
}
