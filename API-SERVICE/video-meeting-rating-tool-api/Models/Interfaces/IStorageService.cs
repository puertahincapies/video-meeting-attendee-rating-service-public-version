﻿namespace video_meeting_rating_tool_api.Models.Interfaces
{
    public interface IStorageService
    {
        Task<S3Response> UploadFileAsync(S3Object s3Object, AwsCredentials credentials);
    }
}
