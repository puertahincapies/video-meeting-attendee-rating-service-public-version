﻿using System.Text.Json.Serialization;

namespace video_meeting_rating_tool_api.Models.Dtos.AccessTokenDto
{
    public class QuizAccessTokenDto
    {
        public int QuizAccessTokenId { get; set; }
        public int QuizId { get; set; }
        public int MeetingId { get; set; }
        public int UserId { get; set; }
        public string Email { get; set; }
        [JsonIgnore]
        public int IsUsed { get; set; }
        public string Token { get; set; }
        public string UUID { get; set; }
        public DateTime CreateDateTime { get; set; }
    }
}
