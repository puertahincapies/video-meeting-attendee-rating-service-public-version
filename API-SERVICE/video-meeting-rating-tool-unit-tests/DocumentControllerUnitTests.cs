using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using video_meeting_rating_tool_api.Controllers;
using video_meeting_rating_tool_api.Models;
using video_meeting_rating_tool_api.Models.Dtos.DocumentDto;
using video_meeting_rating_tool_api.Models.Interfaces;
using video_meeting_rating_tool_api.Repository;

namespace video_meeting_rating_tool_unit_tests
{
    public class DocumentControllerUnitTests
    {
        private readonly Mock<IMeetingQuestRepository> repositoryStub = new();
        private readonly Mock<ILogger<DocumentController>> loggerStub = new();
        private readonly Mock<IConfiguration> configurationStub = new();
        private readonly Mock<IStorageService> storageServiceStub = new();
        private readonly Random rand = new Random();
        private readonly DocumentController controller;

        public DocumentControllerUnitTests()
        {
            controller = new DocumentController(loggerStub.Object, repositoryStub.Object, configurationStub.Object, storageServiceStub.Object);
        }

        [Fact]
        public void UploadDocument_NoDocumentProvided_ReturnsBadRequest()
        {
            // Arrange
            var doc = new Document()
            {
                DocumentId = 0,
                MeetingId = 0,
                Name = "Test",
                Content = null
            };

            repositoryStub.Setup(repo => repo.UpdateDocument(doc)).Returns(doc);

            // Act
            var actionResult = controller.UploadDocument(null);

            // Assert
            var result = actionResult.Result;
            result?.Value.Should().Be("No document uploaded");
        }

        [Fact]
        public void UploadDocument_NoDocumentContentProvided_ReturnsBadRequestObject()
        {
            // Arrange
            var doc = new Document()
            {
                DocumentId = 0,
                MeetingId = 0,
                Name = "Test",
                Content = null
            };

            var docDto = new AddDocumentDto()
            {
                MeetingId = 0,
                Name = "Test",
                Content = null
            };

            repositoryStub.Setup(repo => repo.AddDocument(doc)).Returns(doc);

            // Act
            var actionResult = controller.UploadDocument(docDto);

            // Assert
            var result = actionResult.Result;
            result?.Value.Should().Be("No document content uploaded");
        }

    }
}