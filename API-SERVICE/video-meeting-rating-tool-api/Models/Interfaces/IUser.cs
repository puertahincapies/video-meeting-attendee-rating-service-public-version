﻿using System.ComponentModel.DataAnnotations;

namespace video_meeting_rating_tool_api.Models.Interfaces
{
    public interface IUser
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string? ClientKey { get; set; }
        public string? SecretKey { get; set; }
        public string UUID { get; set; } 
        public DateTime CreateDateTime { get; set; } 
    }
}
