﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using Newtonsoft.Json;

namespace video_meeting_rating_tool_api.Models
{
    [JsonObject]
    public class ChatGPTResponse
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "object")]
        public string Object { get; set; }
        [JsonProperty(PropertyName = "created")]
        public ulong Created { get; set; }
        [JsonProperty(PropertyName = "model")]
        public string Model { get; set; }
        [JsonProperty(PropertyName = "choices")]
        public List<Choice> Choices { get; set; }
        [JsonProperty(PropertyName = "usage")]
        public Usage Usage { get; set; }
    }

    public class Messages
    {
        [JsonProperty(PropertyName = "role")]
        public string Role { get; set; }
        [JsonProperty(PropertyName = "content")]
        public string Content { get; set; }
    }

    public class Choice
    {
        [JsonProperty(PropertyName = "index")]
        public int Index { get; set; }
        [JsonProperty(PropertyName = "message")]
        public Message Message { get; set; }
        [JsonProperty(PropertyName = "finish_reason")]
        public string Finish_Reason { get; set; }
    }

    public class Message
    {
        [JsonProperty(PropertyName = "role")]
        public string Role { get; set; }
        [JsonProperty(PropertyName = "content")]
        public string Content { get; set; }
    }

    public class Usage
    {
        [JsonProperty(PropertyName = "prompt_tokens")]
        public string Prompt_Tokens { get; set; }
        [JsonProperty(PropertyName = "completion_tokens")]
        public string Completion_Tokens { get; set; }
        [JsonProperty(PropertyName = "total_tokens")]
        public string Total_Tokens { get; set; }
    }
}
