import '../css/report.css';
import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import toast, { Toaster } from 'react-hot-toast';
import Cookies from 'universal-cookie';
import { USERSESSION, NAVTOUNAUTHORIZE, GETUSERREPORT } from '../helpers/constants';
import { Line } from "react-chartjs-2";
import {
    Chart as ChartJS,
    LineElement,
    CategoryScale, //x axis
    LinearScale, // y axis
    PointElement,
    Legend 
} from 'chart.js';
ChartJS.register(
    LineElement,
    CategoryScale,
    LinearScale,
    PointElement,
    Legend
)
const Report = ({ userSession }) => {
  const navigate = useNavigate();
  const cookies = new Cookies();
  const [currentUserId, setCurrentUserId] = useState(0);
  const [userReport, setUserReport] = useState([]);
  const [chatGPTReport, setChatGPTReport] = useState([]);
  const [listenerQuizScore, setListenerQuizScore] = useState([]);
  const [listenerQuizMeetingId, setListenerQuizMeetingId] = useState([]);
  const [presenterQuizScore, setPresenterQuizScore] = useState([]);
  const [presenterQuizMeetingId, setPresenterQuizMeetingId] = useState([]);
  const [sentimentAnalysisResults, setSentimentAnalysisResults] = useState([]);

  useEffect(() => {
    let user = userSession;
    if (user == null || user == undefined) {
      user = cookies.get(USERSESSION);
      if (user != null || user != undefined) {
        setCurrentUserId(user.userId);
      }
    } else {
      setCurrentUserId(user.userId);
    }

    if (user != null && user != undefined) {
      fetch(GETUSERREPORT + '/' + user.userId, {
        method: 'GET',
        headers: { 'content-type': 'application/json' },
      })
        .then((res) => {
          if (!res.ok) {
            throw Error('Could not retrieve data from resource');
          }
          return res.json();
        })
        .then((data) => {
          console.log(data);
          setUserReport(data);
          setSentimentAnalysisResults(data.documentSentimentResults);
          let meetingScores = [];
          if (data.listenerScoreQuizList.length > 0 && data.listenerScoreQuizList != null) {
            meetingScores = [...meetingScores, ...Object.values(data.listenerScoreQuizList[0])];
        }
        
        setListenerQuizScore(Object.values(data.listenerScoreQuizList));
        setListenerQuizMeetingId(Object.keys(data.listenerScoreQuizList));

        setPresenterQuizMeetingId(Object.keys(data.presenterScoreQuizList))
        setPresenterQuizScore(Object.values(data.presenterScoreQuizList))
        })
        .catch((err) => {
          console.log(err);
          toast.error('There was an error!', {
            duration: 3000,
            position: 'bottom-right',
          });
        });
    } else {
      navigate(NAVTOUNAUTHORIZE);
    }
  }, []);
 

const pData = {
    labels: presenterQuizMeetingId,
    datasets:[{
        label: 'Presenter Scores Graph',
        color:'red',
        data:presenterQuizScore,
        backgroundColor: 'purple',
        borderColor:'black',
        pointBorderColor: 'purple',
        fill:true,
        tension:0.4
    }
    ]
}
const lData = {
    labels: listenerQuizMeetingId, //x axis for x in kv[::2]
    datasets:[{
        label: 'Listener Scores Graph', 
        data:listenerQuizScore, //y axis  for x in kv[1::2]
        backgroundColor: 'purple',
        borderColor:'black',
        pointBorderColor: 'purple',
        fill:true,
        tension:0.4,
        pointRadius: 3, // Adjust the point radius

    }
    ]
}
const options = {
    plugins: {
      legend: true,
    },
    scales: {
      y: {
        min: 0,
        max: 5,
      },
      x: {
        ticks: {
            maxRotation: 90, // Adjust the maximum rotation angle here
            minRotation: 45, // Adjust the minimum rotation angle here
          },
      },
    },
    maintainAspectRatio: false, // Set to false to allow custom height
    responsive: true,
    height: 400, // Set your desired height here
  };

  return (
    
    <div className="report-component" style={{  alignItems: 'center', justifyContent: 'center' }}>
              <label className="component-header">Report</label>

    {currentUserId !== 0 && (
        <div className="report-layout">
          <div className="report-container">
          <form className="report-form" >
          <div className="report-form-group">        
          <label className="report-label">Listener Score:</label>
          <div className="form-control report-form-control">            
            {userReport.listenerScore != null ? userReport.listenerScore : 0}
            </div>
          </div>
          <div className="report-form-group">
            <label className="report-label">Presentation Score:</label>
            <div className="form-control report-form-control">            
            {userReport.presenterScore != null ? userReport.presenterScore : 0}
            </div>
        </div>
          <div className="report-form-group">        
          <label className="report-label">Overall Sentiment Analysis:</label>
          <div className="form-control report-form-control">            
            {userReport.sentimentScore != null ? userReport.sentimentScore : "Neutral"}
            </div>
          </div>
          <div className="report-form-group">        
          <label className="report-label">Quizzes Count:</label>
          <div className="form-control report-form-control">            
            {userReport.quizCount != null ? userReport.quizCount : 0}
            </div>
          </div> 
          <div className="report-form-group">        
          <label className="report-label">Meetings Hosted:</label>
          <div className="form-control report-form-control">            
            {userReport.hostMeetingCount != null ? userReport.hostMeetingCount : 0}
            </div>
          </div> 
          <div className="report-form-group">        
          <label className="report-label">Attendee Count:</label>
          <div className="form-control report-form-control">            
            {userReport.attendeeMeetingCount != null ? userReport.attendeeMeetingCount : 0}
            </div>
          </div> 
        </form>
          </div>

          <div className="graph-container">
          <div className="listener-graph" style={{ height: '275px',  width: '436px'  }}>
              <label className="graph-labels"> Score vs. QuizID </label>
              <Line
                data = {lData}
                //data = {userReport}
                options = {options}
              ></Line>
            </div><br />

            <div className="presenter-graph" style={{ height: '275px', width: '436px' }}>
            <label className="graph-labels"> Score vs. QuizID </label>
              <Line
                data = {pData}
                                /*data = {userReport}*/

                options = {options}
              ></Line>
            </div><br />

            {sentimentAnalysisResults != null && <div className="sentiment-results"> 
              <table className="sentiment-result-table">
                  <thead>
                      <tr>
                          <th>Meeting ID</th>
                          <th>Document ID</th>
                          <th>Sentiment Result</th>
                      </tr>
                  </thead>
                  <tbody>
                      {sentimentAnalysisResults.map((result) => (
                          <tr key={result.meetingId}>
                              <td>{result.meetingId}</td>
                              <td>{result.documentId}</td>
                              <td>{result.sentimentResult}</td>
                          </tr>
                      ))}
                  </tbody>
              </table>
            </div>}
          </div>
        </div>
      )}
      
      <div className="report-form-group feedback">        
          <label className="report-label">Education Tips & Feedback:</label>
          <div className="form-control report-form-control-feedback">           
            {userReport.presentationFeedback}
          </div>
      </div>
      <Toaster />
    </div>
  );
};
export default Report;
