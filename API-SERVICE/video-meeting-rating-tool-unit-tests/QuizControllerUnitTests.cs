﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using video_meeting_rating_tool_api.Controllers;
using video_meeting_rating_tool_api.Models;
using video_meeting_rating_tool_api.Models.Dtos.QuizDto;
using video_meeting_rating_tool_api.Repository;

namespace video_meeting_rating_tool_unit_tests
{
    public class QuizControllerUnitTests
    {
        private readonly Mock<IMeetingQuestRepository> repositoryStub = new();
        private readonly Mock<ILogger<QuizController>> loggerStub = new();
        private readonly Mock<IConfiguration> configurationStub = new();
        private readonly Random rand = new Random();
        private readonly QuizController controller;

        public QuizControllerUnitTests()
        {
            controller = new QuizController(loggerStub.Object, repositoryStub.Object, configurationStub.Object);
        }

        [Fact]
        public void GenerateQuiz_RequiredMeetingDocumentFieldsNotProvided_ReturnsBadRequest()
        {
            // Arrange
            AddQuizDto quiz = null;

            repositoryStub.Setup(repo => repo.SaveQuiz(It.IsAny<Quiz>())).Returns((Quiz)null);

            // Act
            var actionResult = controller.GenerateQuiz(quiz);

            // Assert
            var result = actionResult.Result;
            result?.Value.Should().Be("Required meeting and document information not provided");
        }

        [Fact]
        public void GenerateQuiz_DocumentIsNull_ReturnsNotFoundRequest()
        {
            // Arrange
            AddQuizDto quiz = new AddQuizDto()
            {
                UserId = rand.Next(1,999),
                DocumentId = rand.Next(1, 999),
                MeetingId = rand.Next(1, 999)
            };

            repositoryStub.Setup(repo => repo.SaveQuiz(It.IsAny<Quiz>())).Returns((Quiz)null);

            // Act
            var actionResult = controller.GenerateQuiz(quiz);

            // Assert
            var result = actionResult.Result;
            result?.Value.Should().Be("Document not found");
        }
    }
}
