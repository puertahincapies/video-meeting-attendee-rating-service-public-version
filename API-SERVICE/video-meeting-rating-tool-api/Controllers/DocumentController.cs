﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using video_meeting_rating_tool_api.Repository;
using video_meeting_rating_tool_api.Models.Dtos.DocumentDto;
using video_meeting_rating_tool_api.Models;
using video_meeting_rating_tool_api.Helpers;
using video_meeting_rating_tool_api.Models.Interfaces;
using Newtonsoft.Json;
using VaderSharp2;

namespace video_meeting_rating_tool_api.Controllers
{
    [ApiController]
    [EnableCors("MeetingQuest_CorsPolicy")]
    [Route("api/v1/meetingquest/[controller]")]
    public class DocumentController : ControllerBase
    {
        private readonly ILogger<DocumentController> _logger;
        private readonly IMeetingQuestRepository _repository;
        private readonly IConfiguration _configuration;
        private readonly IStorageService _storage;
        private AwsCloudWatchClient _cloudWatchClient;

        // using repository pattern
        public DocumentController(ILogger<DocumentController> logger, IMeetingQuestRepository repository, IConfiguration configuration, IStorageService storage)
        {
            _logger = logger;
            _repository = repository;
            _configuration = configuration;
            _storage = storage;
            _cloudWatchClient = new AwsCloudWatchClient(_configuration);
        }

        /// <summary>
        /// Controller to update document/file to server and database for processing
        /// </summary>
        /// <param name="uploadedDocument"></param>
        /// <returns>Document details</returns>
        [HttpPost("[action]")]
        [Consumes("multipart/form-data")]
        public async Task<ActionResult<DocumentDto>> UploadDocument([FromForm] AddDocumentDto uploadedDocument)
        {
            try
            {
                // check if form object is null
                if(uploadedDocument == null)
                {
                    return BadRequest("No document uploaded");
                }

                // limit size
                if(uploadedDocument.Content.Length > 26214400)
                {
                    return BadRequest("The size of the file is too big, please try again. Max size is 25MB");
                }

                // store file on server at specific location using filestream
                var name = uploadedDocument.Name;
                var content = uploadedDocument.Content;
                var ext = Path.GetExtension(content.Name);

                // check if document content property is null
                if(content == null)
                {
                    return BadRequest("No document content uploaded");
                }

                var fileName = $"{name.Replace(" ", "")}_{Guid.NewGuid()}.{ext}";
                string filePath =  Path.Combine("wwwroot/Transcripts", $"{fileName}");

                if (content.Length > 0)
                {
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        content.CopyTo(fileStream);
                    }
                }

                // store file (byte array) in database
                var document = new Document
                {
                    MeetingId = uploadedDocument.MeetingId,
                    Name = name,
                    Content = System.IO.File.ReadAllBytes(filePath)
                };

                var newDocument = _repository.AddDocument(document);

                // compute sentiment analysis
                if(newDocument != null)
                {
                    var sentimentAnalysisResult = ComputeSentimentAnalysis(filePath, newDocument.DocumentId);
                    _repository.AddDocumentSentimentAnalysis(sentimentAnalysisResult);
                }

                // upload file to S3
                await using var memoryStream = new MemoryStream();
                await content.CopyToAsync(memoryStream);
                var fileExt = Path.GetExtension(content.Name);
                var objName = fileName;
                var s3Object = new S3Object()
                {
                    BucketName = _configuration["AwsBucketName"],
                    InputStream = memoryStream,
                    Name = fileName
                };
                var awsCredentials = new AwsCredentials()
                {
                    AccessKey = _configuration["AwsAccessKey"],
                    SecretKey = _configuration["AwsSecretKey"]
                };
                var awsS3Response = await _storage.UploadFileAsync(s3Object, awsCredentials);
                
                // if there's a failure don't stop the process, just log it to the database
                if(awsS3Response.StatusCode != 200)
                {
                    _repository.LogError(new Exception(awsS3Response.Message), Enums.ErrorTypes.Error);
                    _logger.LogError(awsS3Response.Message);
                }

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"Document was successfully uploaded. {fileName}");
                return Ok(new { message = "Document uploaded successfully", document = newDocument.AsDto() });
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        /// <summary>
        /// Deletes all documents from server
        /// </summary>
        /// <returns></returns>
        [HttpDelete("[action]")]
        public async Task<ActionResult> DeleteAllDocumentsFromServer()
        {
            try
            {
                DirectoryInfo documentDir = new DirectoryInfo("wwwroot/Transcripts");
                foreach (FileInfo file in documentDir.GetFiles())
                {
                    // leave test file
                    if (file.Name == "test.txt") continue;
                    file.Delete();
                }
                foreach (DirectoryInfo dir in documentDir.GetDirectories())
                {
                    dir.Delete(true);
                }

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"All files were deleted from server");
                return Ok("All documents were deleted");
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        /// <summary>
        /// Delete all documents from the database
        /// </summary>
        /// <returns>Number of documents deleted</returns>
        [HttpDelete("[action]")]
        public async Task<ActionResult<int>> DeleteAllDocumentsFromDatabase()
        {
            try
            {
                var count = _repository.DeleteAllDocuments();
                await _cloudWatchClient.PublishLogToAwsCloudWatch($"All files were deleted from the database");
                return Ok($"Documents deleted: {count}");
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        /// <summary>
        /// Gets a specific document by ID
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns>documentID</returns>
        [HttpGet("[action]")]
        public async Task<ActionResult<DocumentDto>> GetDocument(int documentId)
        {
            try
            {
                if(documentId == 0)
                {
                    return NotFound("No documentId provided");
                }

                var document = _repository.GetDocument(documentId);
                await _cloudWatchClient.PublishLogToAwsCloudWatch($"Document was successfully retrieved. document: {JsonConvert.SerializeObject(document.AsDto())}");
                return Ok(document.AsDto());
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        /// <summary>
        /// Get a list of documents (only generic information) no content shown
        /// </summary>
        /// <returns>Document list</returns>
        [HttpGet("[action]")]
        public async Task<ActionResult<IEnumerable<DocumentDto>>> GetDocumentList()
        {
            try
            {
                var documents = _repository.GetAllDocuments().Select(document => document.AsDto());
                if(documents == null)
                {
                    return NotFound(new SafeResponse() { Message = "No documents found" });
                }
                await _cloudWatchClient.PublishLogToAwsCloudWatch($"{documents.Count()} document was successfully retrieved");
                return Ok();
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        /// <summary>
        /// Update a specific document by re-upload
        /// </summary>
        /// <param name="uploadedDocument"></param>
        /// <returns></returns>
        [HttpPut("[action]")]
        public async Task<ActionResult<DocumentDto>> UpdateDocument([FromForm]UpdateDocumentDto uploadedDocument)
        {
            try
            {
                if(uploadedDocument == null )
                {
                    return BadRequest("No Document Uploaded");
                }

                // store file on server at specific location using filestream
                var name = uploadedDocument.Name;
                var content = uploadedDocument.Content;
                var filePath = Path.Combine("wwwroot/Transcripts", $"{name.Replace(" ", "")}_{Guid.NewGuid()}");
                if (content.Length > 0)
                {
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        content.CopyTo(fileStream);
                    }
                }

                var existingDoc = _repository.GetDocumentByMeetingId(uploadedDocument.MeetingId);
                if (existingDoc == null)
                {
                    return NotFound("Cannot replace document that does not exist");
                }

                var newDocument = new Document()
                {
                    DocumentId = existingDoc.DocumentId,
                    MeetingId = uploadedDocument.MeetingId,
                    Name = uploadedDocument.Name,
                    Content = System.IO.File.ReadAllBytes(filePath),
                    UUID = existingDoc.UUID
                };

                var doc = _repository.UpdateDocument(newDocument);

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"Document was successfully updated. DocumentId: {existingDoc.DocumentId}");
                return Ok(new { message = "Document uploaded successfully", document = doc.AsDto() });
            }

            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        [HttpGet("[action]/{meetingId}/{userId}")]
        public async Task<ActionResult<DocumentDto>> GetTranscriptDocumentFromZoomCloud(string meetingId, int userId)
        {
            try
            {
                if (string.IsNullOrEmpty(meetingId))
                {
                    return NotFound("Please provide a correct meetingId");
                }

                if (userId == 0)
                {
                    return NotFound("Please provide a correct userId");
                }

                var user = _repository.GetUser(userId);
                //var meeting = _repository.GetMeeting(meetingId);
                ZoomCloudClient.GetMeetingTranscript(user?.ClientKey, user?.SecretKey, meetingId);

                await _cloudWatchClient.PublishLogToAwsCloudWatch($"Transcript file was successfully retrieved from zoom cloud");
                return Ok();
            }

            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        // Helpers
        private DocumentSentimentAnalysis ComputeSentimentAnalysis(string filePath, int documentId)
        {
            // read file and compute sentiment analysis
            // using this package: https://github.com/BobLd/vadersharp
            var analyzer = new SentimentIntensityAnalyzer();
            string text = System.IO.File.ReadAllText(filePath);
            var result = analyzer.PolarityScores(text);

            // calculate overall sentiment
            var sentiment = "undefined";
            if (result.Compound >= 0.05)
            {
                sentiment = "positive";
            }
            else if (result.Compound > -0.05 && result.Compound < 0.5)
            {
                sentiment = "neutral";
            }
            else
            {
                sentiment = "negative";
            }

            // The compound score is the one most commonly used for sentiment analysis by most researchers, including the authors.
            return new DocumentSentimentAnalysis()
            {
                DocumentId = documentId,
                PositiveScore = result.Positive.ToString(),
                NegativeScore = result.Negative.ToString(),
                NeutralScore = result.Neutral.ToString(),
                CompoundScore = result.Compound.ToString(),
                Sentiment = sentiment
            };
        }
    }
}
