﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using video_meeting_rating_tool_api.Exceptions;
using video_meeting_rating_tool_api.Helpers;
using video_meeting_rating_tool_api.Models.Dtos.UserDto;
using video_meeting_rating_tool_api.Models;
using video_meeting_rating_tool_api.Models.Interfaces;
using video_meeting_rating_tool_api.Repository;
using Newtonsoft.Json.Linq;
using System;
using video_meeting_rating_tool_api.Models.Dtos.ReportDto;

namespace video_meeting_rating_tool_api.Controllers
{
    [ApiController]
    [EnableCors("MeetingQuest_CorsPolicy")]
    [Route("api/v1/meetingquest/[controller]")]
    public class ReportController : ControllerBase
    {
        private readonly ILogger<ReportController> _logger;
        private readonly IMeetingQuestRepository _repository;
        private readonly IConfiguration _configuration;
        private AwsCloudWatchClient _cloudWatchClient;

        // using repository pattern
        public ReportController(ILogger<ReportController> logger, IMeetingQuestRepository repository, IConfiguration configuration)
        {
            _logger = logger;
            _repository = repository;
            _configuration = configuration;
            _cloudWatchClient = new AwsCloudWatchClient(_configuration);
        }

        [HttpGet("[action]/{userId}")]
        public async Task<ActionResult<ReportDto>> GetUserReport(int userId)
        {
            try
            {
                if(userId == 0)
                {
                    return NotFound("UserId provided was not found");
                }

                // List of all Meetings Hosted by user
                List<Meeting> hostedMeetings =  _repository.GetMeetingsByUser(userId).ToList();
                // List of quiz results taken by user                
                List<QuizResult> userQuizResults = _repository.GetQuizResultsByUser(userId);
                var (listenerScore, quizzesTakenCount) = calcListenerScore(userQuizResults);
                List<QuizResult> presenterQuizResults = CreatePresenterQuizResults(hostedMeetings);
                Dictionary<int, int> createPresenterQuizResults = CreatePresenterScoreQuizDict(presenterQuizResults);
                int presenterScore = CalcPresenterScore(presenterQuizResults);
                float averageSentiment = AverageSentiment(userId);
                string sentimentRating = IdentifySentiment(averageSentiment);
                // meetings hosted count                
                int meetingsHostedCount = hostedMeetings.Count;
                var documentSentimentResults = DocumentSentimentResults(userId);

                // ChatGPT tips and educational feedback
                var prompt = $@"Please provide the user with public speaking tips based on the following average quiz
                                scores and sentiment scores. The listener score represents how well the user performed on quizzes on 
                                the content discussed in meetings the user attended. This represents the user's level of engagement 
                                and comprehension of the topics discussed in the meeting. The presenter score represents on average 
                                how well the attendees performed on similar quizzes for the meetings the user organized. This reflects 
                                how well the user ran the meeting and effectively they communicated.  The sentiment score is the average 
                                sentiment score achieved by running the transcripts of meeting the user organized through sentiment 
                                analysis programs to identify the overall tone of the meeting. More negative scores indicate a more 
                                negative tone, positive scores a more positive tone. Score will range between -1 and 1. -0.5 to 0.5 
                                should be considered relatively neutral scores. Please keep advice short and professional. Advice 
                                should be less than 75 words at most. If the meetings attended amount is 0 then only give
                                advice based on the presentor and sentiment score. If the meetings presented amount is 0 then only
                                advice based on the listener score. If both the meetings attended and meetings presented are 0 then
                                reply with 'Please attend or present some meetings so we can give you feedback'.
                                Meetings Attended: {quizzesTakenCount}, Meetings Presented: {meetingsHostedCount}, 
                                Listener score: {listenerScore}, Presenter Score: {presenterScore}, Sentiment Score: {averageSentiment}";
                var feedback = GetEducationTipsAndFeedbackFromChatGPT(prompt);

                var report = new Report
                {                    
                    UserId = userId,
                    ListenerScore = listenerScore,
                    PresenterScore = presenterScore,
                    HostMeetingCount = meetingsHostedCount,
                    QuizCount = quizzesTakenCount,
                    AttendeeMeetingCount = presenterQuizResults.Count,
                    SentimentScore = sentimentRating,                    
                    ListenerScoreQuizList = CreateQuizScoreList(userQuizResults),
                    PresenterScoreQuizList = createPresenterQuizResults,
                    DocumentSentimentResults = documentSentimentResults,
                    PresentationFeedback = feedback
                };
                
                await _cloudWatchClient.PublishLogToAwsCloudWatch($"User report was successfully retrieved.");                
                return Ok(report.AsDto());
            }
            catch (UserNotFoundException ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Warning);
                _logger.LogError(ex.Message);
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return NotFound(new SafeResponse() { Message = "User not found" });
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError($"Error retrieving user with ID {userId}: {ex.Message}");
                await _cloudWatchClient.PublishLogToAwsCloudWatch(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }

        // helpers
        private string GetEducationTipsAndFeedbackFromChatGPT(string prompt)
        {
            try
            {
                var uri = _configuration["ChatGPTAPIUri"];
                var token = _configuration["OpenAIToken"];
                var result = ChatGPTClient.SendPromptToChatGPTUsingRestClient(prompt, uri, token);
                return result.Choices.FirstOrDefault().Message.Content;
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        private (int, int) calcListenerScore(List<QuizResult> userQuizResults)
        {
            var listenerScore = 0;
            int quizzesTakenCount = 0;
            foreach (QuizResult qr in userQuizResults)
            {
                if (qr.Role != "Presenter")
                {
                    listenerScore += qr.Score;
                    quizzesTakenCount += 1;
                }                    
            }
            listenerScore = quizzesTakenCount > 0 ? listenerScore / quizzesTakenCount : 0;            
            return (listenerScore, quizzesTakenCount);
        }

        private Dictionary<int, int> CreateQuizScoreList(List<QuizResult> userQuizResults)
        {
            // Creates list of key value pairs of meetingIds and quiz scores for the user
            Dictionary<int, int> listenerScoreQuizList = new Dictionary<int, int>();
            foreach (QuizResult qr in userQuizResults)
            {

                if (qr.Role != "Presenter")
                {
                 listenerScoreQuizList.Add(qr.MeetingId, qr.Score);
                }
            }
            return listenerScoreQuizList;
        }

        private List<QuizResult> CreatePresenterQuizResults(List<Meeting> hostedMeetings)
        {
            //List of quiz results for attendees from the user's hosted meetings
                List<QuizResult> presenterQuizResults = new List<QuizResult>();
                foreach (Meeting meeting in hostedMeetings)
                {
                    var meetingQuizResults = _repository.GetQuizResultsByMeeting(meeting.MeetingId);
                    List<QuizResult> meetingQuizResultsList = meetingQuizResults?.ToList() ?? new List<QuizResult>();
                    foreach (QuizResult qr in meetingQuizResultsList)
                    {
                        if (qr.Role != "Presenter")
                        {
                            presenterQuizResults.Add(qr);
                        }                        
                    }                    
                }
                return presenterQuizResults;                
        }

        private static Dictionary<int, int> CreatePresenterScoreQuizDict(List<QuizResult> presenterQuizResults)
        {
            // Creates Dictionary of Key Value pairs of meetingIds and scores for presentor meetings
            Dictionary<int, int> presenterScoreQuizByMeeting = new Dictionary<int, int>();
            Dictionary<int, int> presenterAttendeeCountByMeeting = new Dictionary<int, int>();
            
            int value;

            foreach (QuizResult qr in presenterQuizResults)
            {    
                if (presenterScoreQuizByMeeting.TryGetValue(qr.MeetingId, out value))
                {
                    presenterAttendeeCountByMeeting[qr.MeetingId] += 1;
                    presenterScoreQuizByMeeting[qr.MeetingId] += qr.Score;
                }
                else
                {
                    presenterAttendeeCountByMeeting.Add(qr.MeetingId, 1);
                    presenterScoreQuizByMeeting.Add(qr.MeetingId, qr.Score);
                }                
                                                        
            }

            foreach (int meetingId in presenterScoreQuizByMeeting.Keys)
            {
                presenterScoreQuizByMeeting[meetingId] = presenterScoreQuizByMeeting[meetingId] / presenterAttendeeCountByMeeting[meetingId];
            }
            return presenterScoreQuizByMeeting;
        }

        private static int CalcPresenterScore(List<QuizResult> presenterQuizResults)
        {
            // quiz scores of meeting attendees
            var presenterScore = 0;
            foreach (QuizResult qr in presenterQuizResults)
            {
                presenterScore += qr.Score;
            }
            presenterScore = presenterQuizResults.Count > 0 ? presenterScore / presenterQuizResults.Count : 0;
            return presenterScore;
        }

        private float AverageSentiment(int userId)
        {
            // overall sentiment analysis of meetings hosted                
            List<int> quizIds = _repository.GetQuizIdsByUserId(userId);
            List<int> docIds = new List<int>();
            foreach (int id in quizIds)
            {
                Quiz quiz = _repository.GetQuiz(id);
                docIds.Add(quiz.DocumentId);
            }
            List<float> sentimentScores = new List<float>();
            foreach (int id in docIds)
            {
                DocumentSentimentAnalysis sentAnal = _repository.GetDocumentSentimentAnalysis(id);
                string sentiment;
                if (sentAnal != null)
                {
                    sentiment = sentAnal.CompoundScore;
                    sentimentScores.Add(float.Parse(sentiment));
                }
                continue;
            }

            float sum = 0.0f;
            foreach (float num in sentimentScores)
            {
                sum += num;
            }
            float averageSentiment = sentimentScores.Count > 0 ? sum / sentimentScores.Count : 0.0f;
            return averageSentiment;
        }

        private string IdentifySentiment(float averageSentiment)
        {            
            string sentimentRating = "Undefined";
            if (averageSentiment >= 0.05)
            {
                sentimentRating = "Positive";
            }
            else if (averageSentiment > -0.05 &&  averageSentiment < 0.05)
            {
                sentimentRating = "Neutral";
            }
            else
            {
                sentimentRating = "Negative";
            }
            return sentimentRating;
        }

        private List<DocumentSentimentResult> DocumentSentimentResults(int userId)
        {
            var documentSentimentAnalysisResults = new List<DocumentSentimentResult>();
            var documentSentimentAnalysis = new List<DocumentSentimentAnalysis>();
            var quizzes = _repository.GetAllQuizzesByHostUser(userId).ToList();

            foreach (var quiz in quizzes)
            {
                var result = _repository.GetDocumentSentimentAnalysis(quiz.DocumentId);
                if (result != null)
                {
                    documentSentimentAnalysis.Add(result);
                }
            }

            foreach (var result in documentSentimentAnalysis)
            {
                var quiz = quizzes.FirstOrDefault(x => x.DocumentId == result.DocumentId);
                var meetingId = 0;

                if(quiz != null)
                {
                    meetingId = quiz.MeetingId;
                }

                documentSentimentAnalysisResults.Add(new DocumentSentimentResult()
                {
                    MeetingId = meetingId,
                    DocumentId = result.DocumentId,
                    SentimentResult = result.Sentiment
                });
            }

            return documentSentimentAnalysisResults;
        }
    }
}
