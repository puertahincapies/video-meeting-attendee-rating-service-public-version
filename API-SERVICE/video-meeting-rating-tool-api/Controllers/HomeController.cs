﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using video_meeting_rating_tool_api.Helpers;

namespace video_meeting_rating_tool_api.Controllers
{
    [ApiController]
    [EnableCors("MeetingQuest_CorsPolicy")]
    [Route("api/v1/meetingquest/[controller]")]
    public class HomeController : ControllerBase
    {
        [HttpGet("[action]")]
        public string ServerHealthCheck()
        {
            return "All good, server is up and running!";
        }
    }
}
