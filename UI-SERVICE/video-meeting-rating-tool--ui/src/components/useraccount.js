import '../css/useraccount.css';
import React, { useState, useEffect } from 'react';
import {USERSESSION, CHANGEUSER, NAVTOUSERACCOUNT, GETUSER, NAVTOUNAUTHORIZE} from '../helpers/constants';
import Cookies from 'universal-cookie';
import toast from 'react-hot-toast';
import { useNavigate } from 'react-router-dom';

const UserAccount = ({userSession}) => {
  const [userData, setUserData] = useState([]);
  const [editMode, setEditMode] = useState(false);  
  const cookies = new Cookies();
  const [errorMessage, setErrorMessage] = useState('');
  const navigate = useNavigate();
  const [userName, setUserName] = useState("");
  const [userId, setUserId] = useState(0);
  const [userPassword, setUserPassword] = useState("");
  const [userEmail, setUserEmail] = useState("");
  const [userClientKey, setUserClientKey] = useState("");
  const [userSecretKey, setUserSecretKey] = useState("");
  const [virtualMeetingProvider, setVirtualMeetingProvider] = useState("");

  useEffect(() => {

    let user = userSession;
    console.log("userAcccount > user: " + JSON.stringify(user));
    if(user == null || user== undefined) {
        user = cookies.get(USERSESSION);
        console.log("upload > set user using cookie session: " + JSON.stringify(user));
        if(user != null || user != undefined){
            setUserId(user.userId);
        }
    }
    else {
        setUserId(user.userId);
    }

    if(user != null && user != undefined) {
      fetch(GETUSER + `\\${user.userId}`, {
        method: "GET",
        headers: { 'content-type': 'application/json' },      
      })
      .then(res => {
          if(!res.ok) {
              throw Error("Could not retrieve data from resource");
          } 
          return res.json();
      })
      .then(data => {
          console.log(data);        
          setUserName(data.name)
          setUserEmail(data.email);
          setUserPassword(data.password);
          setUserClientKey(data.clientKey);
          setUserSecretKey(data.secretKey);
          setVirtualMeetingProvider(data.virtualMeetingProvider);
      })
      .catch(err => {
          console.log(err);
          toast.error("There was an error!", {
              duration: 3000,
              position: 'bottom-right'
          })
      })  
    }
    else {
      navigate(NAVTOUNAUTHORIZE);
    }
  }, []);  

  const updateUser = async () => {
    console.log("Initiating user name change!")    
    let user = cookies.get(USERSESSION);        
    
    try {
      fetch(CHANGEUSER + `\\${user.userId}`, {
        method: "PUT",
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify({ token: btoa(userId + ":" + userName + ":" + userPassword  + ":" + virtualMeetingProvider + ":" + userClientKey + ":" + userSecretKey)})
      })
      .then(res => {
          if(!res.ok) {
              throw Error("Could not retrieve data from resource");
          }
          return res.json();
      })
      .then(data => {
          console.log(data);
          if(data.message == "User info has been changed"){
            toast.success("User info has been updated" + data.payload.name, {
                duration: 5000,
                position: 'bottom-right'
            })
            setUserData(data.payload);
          }

          // navigate to the User Account
          setTimeout(() => { navigate(NAVTOUSERACCOUNT) }, 1000);
      })
      .catch(err => {
          console.log(err);
          toast.error("There was an error!", {
              duration: 3000,
              position: 'bottom-right'
          })
      })

    } catch (error) {
      console.error('Error during user name change:', error);
      console.log(error)
      setErrorMessage('An error occurred during signup.');
    }
  };

  const handleSaveChanges = () => {
    updateUser();    
    setEditMode(false);
  };

  let editButtonText = "Edit User Details"
  if(editMode) {
    editButtonText = "Cancel"
  }    

  return (
    <div className="user-account-component">
      {userId != 0 && editMode ?(
        <form className="user-account-form" onSubmit={handleSaveChanges}>
          <div className="form-group">        
            <label className="form-input-label">Name:</label>
              <input
                type="text"
                name="name"
                className="form-control user-account-form-control"
                value = {userName}
                onChange={(e) => setUserName(e.target.value)}
              />
          </div>
          <div className="form-group">        
            <label className="form-input-label">Password:</label>
              <input
                type="password"
                name="password"
                className="form-control user-account-form-control"
                value={userPassword}
                onChange={(e) => setUserPassword(e.target.value)}
              />
          </div>
          <div className="form-group">
            <label className="form-input-label">Virtual Meeting Provider:</label>
              <input
                type="text"
                name="virtualMeetingProvider"
                className="form-control user-account-form-control"
                value={virtualMeetingProvider}
                onChange={(e) => setVirtualMeetingProvider(e.target.value)}
              />
          </div>
          <div className="form-group">
            <label className="form-input-label">Client Key:</label>
              <input
                type="text"
                name="clientkey"
                className="form-control user-account-form-control"
                value={userClientKey}
                onChange={(e) => setUserClientKey(e.target.value)}
              />
          </div>
          <div className="form-group">
            <label className="form-input-label">Secret Key:</label>
              <input
                type="password"
                name="secretkey"
                className="form-control user-account-form-control"
                value={userSecretKey}
                onChange={(e) => setUserSecretKey(e.target.value)}
              />
          </div>  
          <button type="submit" className="action-button">Save Changes</button>      
        </form>
        ) : (
        <form className="user-account-form">
          <div className="form-group">
          <div className="form-group row">
              <label className="form-input-label">Name:</label>
              <div className="col-sm-12">
                <input type="text" readOnly className="form-control-plaintext" id="name" value={userName}/>
              </div>              
            </div>
            <div className="form-group row">
              <label className="form-input-label">Email:</label>
              <div className="col-sm-12">
                <input type="text" readOnly className="form-control-plaintext" id="email" value={userEmail}/>
              </div>              
            </div>
            <div className="form-group row">
              <label className="form-input-label">Password:</label>
              <div className="col-sm-12">
                <input type="password" readOnly className="form-control-plaintext" id="password" value={userPassword}/>
              </div>              
            </div>
            <div className="form-group row">
              <label className="form-input-label">Virtual Meeting Provider:</label>
              <div className="col-sm-12">
                <input type="text" readOnly className="form-control-plaintext" id="virtual-meeting-provider" value={virtualMeetingProvider}/>
              </div>              
            </div>
            <div className="form-group row">
              <label className="form-input-label">ClientKey:</label>
              <div className="col-sm-12">
                <input type="text" readOnly className="form-control-plaintext" id="client-key" value={userClientKey}/>
              </div>              
            </div>
            <div className="form-group row">
              <label className="form-input-label">SecretKey:</label>
              <div className="col-sm-12">
                <input type="password" readOnly className="form-control-plaintext" id="secret-key" value={userSecretKey}/>
              </div>              
            </div>            
          </div>
          <button type="button" className="action-button" onClick={() => setEditMode(!editMode)}>{editButtonText}</button>
        </form>
        )
      }
    </div>
  );
};

export default UserAccount;
