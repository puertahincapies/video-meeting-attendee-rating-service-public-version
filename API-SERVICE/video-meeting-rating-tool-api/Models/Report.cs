﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using video_meeting_rating_tool_api.Models.Interfaces;

namespace video_meeting_rating_tool_api.Models
{    
    public class Report : IReport
    {
        public int UserId { get; set; }
        public int PresenterScore { get; set; }
        public int ListenerScore { get; set; }
        public int HostMeetingCount { get; set; }
        public int QuizCount { get; set; }
        public int AttendeeMeetingCount {get; set;}
        public string SentimentScore {get; set;}
        public Dictionary<int, int> ListenerScoreQuizList {get; set;}
        public Dictionary<int, int> PresenterScoreQuizList {get; set;}
        public List<DocumentSentimentResult> DocumentSentimentResults { get; set; }
        public string PresentationFeedback {get; set;}
        public string UUID { get; set; } = Guid.NewGuid().ToString();
        public DateTime CreateDateTime { get; set; } = DateTime.Now;
    }
}
