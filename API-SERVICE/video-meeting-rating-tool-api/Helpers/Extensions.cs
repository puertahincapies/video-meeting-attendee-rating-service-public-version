﻿using System.Runtime.CompilerServices;
using video_meeting_rating_tool_api.Models;
using video_meeting_rating_tool_api.Models.Dtos.AccessTokenDto;
using video_meeting_rating_tool_api.Models.Dtos.DocumentDto;
using video_meeting_rating_tool_api.Models.Dtos.MeetingAttendeeEmailDto;
using video_meeting_rating_tool_api.Models.Dtos.MeetingDto;
using video_meeting_rating_tool_api.Models.Dtos.QuizDto;
using video_meeting_rating_tool_api.Models.Dtos.QuizResultDto;
using video_meeting_rating_tool_api.Models.Dtos.QuizzesDto;
using video_meeting_rating_tool_api.Models.Dtos.ReportDto;
using video_meeting_rating_tool_api.Models.Dtos.UserDto;

namespace video_meeting_rating_tool_api.Helpers
{
    public static class Extensions
    {
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });
        }

        public static DocumentDto AsDto(this Document document)
        {
            return new DocumentDto()
            {
                DocumentId = document.DocumentId,
                MeetingId = document.MeetingId,
                Name = document.Name,
                Content = document.Content,
                UUID = document.UUID,
                CreateDateTime = document.CreateDateTime
            };
        }

        public static MeetingDto AsDto(this Meeting meeting)
        {
            return new MeetingDto()
            {
                MeetingId = meeting.MeetingId,
                MeetingUUID = meeting.MeetingUUID,
                MeetingName = meeting.MeetingName,
                MeetingDateTime = meeting.MeetingDateTime,
                MeetingAttendeeEmails = meeting.MeetingAttendeeEmails,
                UserId = meeting.UserId,
                UUID = meeting.UUID,
                CreateDateTime = meeting.CreateDateTime
            };
        }

        public static QuizDto AsDto(this Quiz quiz)
        {
            return new QuizDto()
            {
                 QuizId = quiz.QuizId,
                 DocumentId = quiz.DocumentId,
                 UserId = quiz.UserId,
                 MeetingId = quiz.MeetingId,
                 Question1 = quiz.Question1,
                 Question2 = quiz.Question2,
                 Question3 = quiz.Question3,
                 Question4 = quiz.Question4,
                 Question5 = quiz.Question5,
                 Question1Answer1 = quiz.Question1Answer1,
                 Question1Answer2 = quiz.Question1Answer2,
                 Question1Answer3 = quiz.Question1Answer3,
                 Question2Answer1 = quiz.Question2Answer1,
                 Question2Answer2 = quiz.Question2Answer2,
                 Question2Answer3 = quiz.Question2Answer3,
                 Question3Answer1 = quiz.Question3Answer1,
                 Question3Answer2 = quiz.Question3Answer2,
                 Question3Answer3 = quiz.Question3Answer3,
                 Question4Answer1 = quiz.Question4Answer1,
                 Question4Answer2 = quiz.Question4Answer2,
                 Question4Answer3 = quiz.Question4Answer3,
                 Question5Answer1 = quiz.Question5Answer1,
                 Question5Answer2 = quiz.Question5Answer2,
                 Question5Answer3 = quiz.Question5Answer3,
                 Question1CorrectAnswer = quiz.Question1CorrectAnswer,
                 Question2CorrectAnswer = quiz.Question2CorrectAnswer,
                 Question3CorrectAnswer = quiz.Question3CorrectAnswer,
                 Question4CorrectAnswer = quiz.Question4CorrectAnswer,
                 Question5CorrectAnswer = quiz.Question5CorrectAnswer,
                 IsConsistent = quiz.IsConsistent,
                 UUID = quiz.UUID,
                 CreateDateTime = quiz.CreateDateTime, 
            };
        }

        public static UserDto AsDto(this User user)
        {
            return new UserDto()
            {
                UserId = user.UserId,
                Email = user.Email,
                Password = user.Password,
                Name = user.Name,
                VirtualMeetingProvider = user.VirtualMeetingProvider,
                ClientKey = user.ClientKey,
                SecretKey = user.SecretKey,
                UUID = user.UUID,
                CreateDateTime = user.CreateDateTime
            };
        }

        public static QuizResultDto AsDto(this QuizResult quizResult)
        {
            return new QuizResultDto()
            {
                QuizResultId = quizResult.QuizResultId,
                QuizId = quizResult.QuizId,
                MeetingId = quizResult.MeetingId,
                UserId = quizResult.UserId,
                Role = quizResult.Role,
                Email = quizResult.Email,
                Question1Response = quizResult.Question1Response,
                Question2Response = quizResult.Question2Response,
                Question3Response = quizResult.Question3Response,
                Question4Response = quizResult.Question4Response,
                Question5Response = quizResult.Question5Response,
                Score = quizResult.Score,
                Status = quizResult.Status,
                UUID = quizResult.UUID,
                CreateDateTime = quizResult.CreateDateTime
            };
        }

        public static QuizAccessTokenDto AsDto(this QuizAccessToken quizAccessToken)
        {
            return new QuizAccessTokenDto()
            {
                QuizAccessTokenId = quizAccessToken.QuizAccessTokenId,
                UserId = quizAccessToken.UserId,
                Email = quizAccessToken.Email,
                QuizId = quizAccessToken.QuizAccessTokenId,
                MeetingId = quizAccessToken.MeetingId,
                IsUsed = quizAccessToken.IsUsed,
                Token = quizAccessToken.Token,
                UUID = quizAccessToken.UUID,
                CreateDateTime = quizAccessToken.CreateDateTime
            };
        }

        public static UserQuizzesDto AsDto(this UserQuizzes userQuizzes)
        {
            return new UserQuizzesDto()
            {
                UserQuizId = userQuizzes.UserQuizId,
                UserId = userQuizzes.UserId,
                QuizId = userQuizzes.QuizId,
                UUID = userQuizzes.UUID,
                CreateDateTime = userQuizzes.CreateDateTime
            };
        }

        public static MeetingAttendeeEmailDto AsDto(this MeetingAttendeeEmail meetingAttendeeEmail)
        {
            return new MeetingAttendeeEmailDto()
            {
                MeetingAttendeeEmailId = meetingAttendeeEmail.MeetingAttendeeEmailId,
                UserId = meetingAttendeeEmail.UserId,
                AttendeeEmail = meetingAttendeeEmail.AttendeeEmail,
                UUID = meetingAttendeeEmail.UUID,
                CreateDateTime = meetingAttendeeEmail.CreateDateTime
            };
        }

        public static ReportDto AsDto(this Report report)
        {
            return new ReportDto()
            {                
                UserId = report.UserId,
                PresenterScore = report.PresenterScore,
                ListenerScore = report.ListenerScore,
                HostMeetingCount = report.HostMeetingCount,
                QuizCount = report.QuizCount,
                AttendeeMeetingCount = report.AttendeeMeetingCount,
                SentimentScore = report.SentimentScore,
                ListenerScoreQuizList = report.ListenerScoreQuizList,
                PresenterScoreQuizList = report.PresenterScoreQuizList,
                DocumentSentimentResults = report.DocumentSentimentResults,
                PresentationFeedback = report.PresentationFeedback,
                UUID = report.UUID,
                CreateDateTime = report.CreateDateTime,
            };
        }
    }
}
