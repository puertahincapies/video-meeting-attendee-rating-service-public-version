﻿using Microsoft.Data.SqlClient;
using video_meeting_rating_tool_api.Enums;
using video_meeting_rating_tool_api.Models;
using Document = video_meeting_rating_tool_api.Models.Document;
using video_meeting_rating_tool_api.Exceptions;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace video_meeting_rating_tool_api.Repository
{
    public class MeetingQuestRepository : IMeetingQuestRepository
    {
        private readonly MeetingQuestContext db;
        public MeetingQuestRepository(MeetingQuestContext db)
        {
            this.db = db;
        }

        #region "User"
        public IEnumerable<User> GetUsers()
        {
            try
            {
                var result = db.Users;
                if(result == null)
                {
                    throw new UserNotFoundException("Users not found");
                }
                return result;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public User GetUser(int userId)
        {
            try
            {
                var result = db.Users.Where(x => x.UserId == userId).SingleOrDefault();
                if (result == null)
                {
                    throw new UserNotFoundException($"User not found. UserId: {userId}");
                }
                return result;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public User SignUp(User user)
        {
            try
            {
                // Check if the email is already taken
                if (GetUserByEmail(user.Email) != null)
                {
                    // Handle duplicate email
                    throw new DuplicateEmailException("Email is already registered");
                }

                // Add the user to the repository
                db.Add(user);
                db.SaveChanges();                

                var result = db.Users.Where(x => x.UserId == user.UserId).SingleOrDefault();
                if(result == null)
                {
                    throw new UnableToSaveUserException($"User was not found. User was not properly saved in the database. User: {JsonConvert.SerializeObject(user)}");
                }
                // Return the added user
                return result;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public User GetUserByEmail(string email)
        {
            try
            {
                var result = db.Users.Where(x => x.Email == email).SingleOrDefault();
                return result;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public User UserLogin(string email, string password)
        {
            try
            {
                var result = db.Users.Where(x => x.Email == email && x.Password == password).SingleOrDefault();
                if (result == null)
                {
                    throw new UserNotFoundException($"User not found. Email: {email}, Password: {password}");
                }
                return result;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public User ChangeUser(int userId, string newUserName, string newPassword, string virtualMeetingProvider, string newClientKey, string newSecretKey)
        {
            try
            {
                var user = db.Users.Where(x => x.UserId == userId).SingleOrDefault();
                if(user != null)
                {
                    var updateUser = new User()
                    {
                        UserId = userId,
                        Email = user.Email,
                        Name = newUserName,
                        Password = newPassword,
                        VirtualMeetingProvider = virtualMeetingProvider,
                        ClientKey = newClientKey,
                        SecretKey = newSecretKey
                    };
                    db.Entry(user).CurrentValues.SetValues(updateUser);
                    db.SaveChanges();
                    return db.Users.Where(x => x.UserId == userId).SingleOrDefault();
                }
                return user;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }        

        public void SaveChanges()
        {
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                // Handle database update exception
                throw ex;
            }
            catch (Exception ex)
            {
                // Handle other exceptions
                throw ex;
            }
        }
        # endregion

        #region "Document"
        public Document AddDocument(Document document)
        {
            try
            {
                db.Add(document);
                db.SaveChanges();
                return db.Documents.Where(x => x.DocumentId == document.DocumentId).SingleOrDefault();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int DeleteAllDocuments()
        {
            try
            {
                var count = db.Documents.Count();
                if (db.Documents.Any())
                {
                    db.RemoveRange(db.Documents.ToList());
                    db.SaveChanges();
                }
                return count;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void DeleteDocument(int documentId)
        {
            try
            {
                var existingDoc = db.Documents.Where(x => x.DocumentId == documentId).SingleOrDefault();
                db.Documents.Remove(existingDoc);
                db.SaveChanges();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IEnumerable<Document> GetAllDocuments()
        {
            try
            {
                return db.Documents;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Document GetDocument(int documentId)
        {
            try
            {
                return db.Documents.Where(x => x.DocumentId == documentId).SingleOrDefault();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Document GetDocumentByMeetingId(int meetingId)
        {
            try
            {
                return db.Documents.Where(x => x.MeetingId == meetingId).SingleOrDefault();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Document UpdateDocument(Document document)
        {
            try
            {
                var existingDoc = db.Documents.Where(x => x.DocumentId == document.DocumentId).SingleOrDefault();
                db.Entry<Document>(existingDoc).CurrentValues.SetValues(document);
                db.SaveChanges();
                return existingDoc;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region "Meeting"
        public Meeting AddMeeting(Meeting meeting)
        {
            try
            {
                db.Add(meeting);
                db.SaveChanges();
                var result = db.Meetings.Where(x => x.MeetingId == meeting.MeetingId).SingleOrDefault();
                if(result == null)
                {
                    throw new UnableToSaveMeetingException($"No meeting found, the meeting was not properly saved in the database. Meeting: {JsonConvert.SerializeObject(meeting)}");
                }                

                return result;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Meeting GetMeeting(int meetingId)
        {
            try
            {
                var result = db.Meetings.Where(x => x.MeetingId == meetingId).SingleOrDefault();
                if(result == null)
                {
                    throw new MeetingNotFoundException($"Meeting was not found. MeetingId: {meetingId}");
                }
                return result;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IEnumerable<Meeting> GetMeetingsByUser(int userId)
        {
            try
            {
                var result = db.Meetings.Where(x => x.UserId == userId);
                
                return result;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IEnumerable<Meeting> GetMeetings()
        {
            try
            {
                var result = db.Meetings;
                if(result == null)
                {
                    throw new MeetingNotFoundException("No meetings found");
                }
                return result;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region "Quiz"
        public Quiz SaveQuiz(Quiz quiz)
        {
            try
            {
                db.Add(quiz);
                db.SaveChanges();
                var result = db.Quizzes.Where(x => x.QuizId == quiz.QuizId).SingleOrDefault();
                if(result == null)
                {
                    throw new UnableToSaveQuizException($"Quiz not found. Quiz was not properly saved in the database. Quiz: {JsonConvert.SerializeObject(quiz)}");
                }
                return result;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Quiz GetQuiz(int quizId)
        {
            try
            {
                var result = db.Quizzes.Where(x => x.QuizId == quizId).SingleOrDefault();
                if (result == null)
                {
                    throw new QuizNotFoundException($"Quiz was not found. QuizId: {quizId}");
                }
                return result;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<int> GetQuizIdsByUserId(int userId)
        {
            try
            {
                // Assuming Quizzes table has a foreign key relationship with Users table
                var result = db.Quizzes
                    .Where(quiz => quiz.UserId == userId)
                    .Select(quiz => quiz.QuizId)
                    .ToList();
                if (result == null)
                {
                    throw new QuizNotFoundException($"Quizzes for this user were not found. UserId: {userId}");
                }
                return result;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Quiz GetQuizByMeetingId(int meetingId)
        {
            try
            {
                var result = db.Quizzes.Where(x => x.MeetingId == meetingId).SingleOrDefault();
                if(result == null)
                {
                    throw new QuizNotFoundException($"Quiz was not found for the meetingId provided. MeetingId: {meetingId}");
                }
                return result;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void UpdateQuiz(Quiz updatedQuiz)
        {
            try
            {
                var existingQuiz = db.Quizzes.Where(x => x.QuizId == updatedQuiz.QuizId).SingleOrDefault();
                if (existingQuiz == null)
                {
                    throw new QuizNotFoundException($"Quiz was not found for the quizId provided. QuizId: {updatedQuiz.QuizId}");
                }
                db.Entry(existingQuiz).CurrentValues.SetValues(updatedQuiz);
                db.SaveChanges();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IEnumerable<Quiz> GetAllQuizzes()
        {
            try
            {
                return db.Quizzes;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IEnumerable<Quiz> GetAllQuizzesByHostUser(int userId)
        {
            try
            {
                return db.Quizzes.Where(x => x.UserId == userId);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region "QuizResults"

        public QuizResult GetQuizResult(int quizId, int userId)
        {
            try
            {
                var result = db.QuizResults.Where(x => (x.QuizId == quizId && x.UserId == userId)).SingleOrDefault();
                return result;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<QuizResult> GetQuizResultsByUser(int userId)
        {
            try
            {
                var result = db.QuizResults.Where(x => x.UserId == userId);
                var result2 = result.ToList<QuizResult>();
                return result2;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IEnumerable<QuizResult> GetQuizResultsByMeeting(int meetingId)
        {
            try
            {
                var result = db.QuizResults.Where(x => x.MeetingId == meetingId);
                return result;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        

        public QuizResult AddQuizResult(QuizResult quizResult)
        {
            try
            {
                db.Add(quizResult);
                db.SaveChanges();
                var result = db.QuizResults.Where(x => x.QuizResultId == quizResult.QuizResultId).SingleOrDefault();
                if(result == null)
                {
                    throw new UnableToSubmitQuizResulException("Quiz result was not submitted");
                } 
                return result;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion

        #region "Errors"
        public void LogError(Exception exception, ErrorTypes errorType)
        {
            try
            {
                var error = new Error()
                {
                    ErrorMessage = exception.Message,
                    ErrorType = errorType.ToString(),
                    StackTrace = exception.StackTrace
                };
                db.Add(error);
                db.SaveChanges();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void LogUIError(Error error)
        {
            try
            {
                db.Add(error);
                db.SaveChanges();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region "QuizAccessTokens"
        public QuizAccessToken AddAccessToken(QuizAccessToken quizAccessToken)
        {
            try
            {
                db.Add(quizAccessToken);
                db.SaveChanges();
                var result = db.QuizAccessTokens.Where(x => x.QuizAccessTokenId == quizAccessToken.QuizAccessTokenId).SingleOrDefault();
                if (result == null)
                {
                    throw new UnableToSaveAccessTokenException($"Generated token was not properly stored. Details: {JsonConvert.SerializeObject(quizAccessToken)}");
                }
                return result;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public QuizAccessToken GetAccessToken(int userId, int quizId, int meetingId)
        {
            try
            {
                var result = db.QuizAccessTokens.Where(x => x.UserId == userId && x.QuizId == quizId && x.MeetingId == meetingId).SingleOrDefault();
                if(result == null)
                {
                    throw new AccessTokenNotFoundException($"No access token for this user for a specific quiz/meeting. UserId: {userId}, QuizId: {quizId}, MeetingId: {meetingId}");
                }
                return result;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IEnumerable<QuizAccessToken> GetAccessTokens()
        {
            try
            {
                return db.QuizAccessTokens;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public QuizAccessToken ValidateAccessToken(string token)
        {
            try
            {
                var result = db.QuizAccessTokens.Where(x => x.Token == token).SingleOrDefault();
                return result;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void UpdateAccessToken(QuizAccessToken quizAccessToken)
        {
            try
            {
                var existingQuizAccessToken = db.QuizAccessTokens.Where(x => x.QuizAccessTokenId == quizAccessToken.QuizAccessTokenId).SingleOrDefault();
                db.Entry(existingQuizAccessToken).CurrentValues.SetValues(quizAccessToken);
                db.SaveChanges();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region "UserQuizzes"
        public IEnumerable<UserQuizzes> GetUserQuizzes(int userId)
        {
            try
            {
                var result = db.UserQuizzes.Where(x => x.UserId == userId);
                return result;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void AddUserQuiz(UserQuizzes userQuiz)
        {
            try
            {
                db.Add(userQuiz);
                db.SaveChanges();
                var result = db.UserQuizzes.Where(x => x.UserId == userQuiz.UserId);
                if (result == null)
                {
                    throw new UnableToSaveUserQuizException($"User quiz was not properly saved for this specific user. UserId: {userQuiz.UserId}");
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region "MeetingAttendeeEmails"
        public void AddMeetingAttendeeEmail(MeetingAttendeeEmail emailEntry)
        {
            try
            {
                db.Add(emailEntry);
                db.SaveChanges();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public MeetingAttendeeEmail GetMeetingAttendeeEmail(string email, int userId)
        {
            try
            {
                return db.MeetingAttendeeEmails.Where(x => x.AttendeeEmail == email && x.UserId == userId).SingleOrDefault();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IEnumerable<MeetingAttendeeEmail> GetMeetingAttendeeEmails(int userId)
        {
            try
            {
                return db.MeetingAttendeeEmails.Where(x => x.UserId == userId);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region "DocumentSentimentAnalysis"
        public void AddDocumentSentimentAnalysis(DocumentSentimentAnalysis documentSentimentAnalysis)
        {
            try
            {
                db.Add(documentSentimentAnalysis);
                db.SaveChanges();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DocumentSentimentAnalysis GetDocumentSentimentAnalysis(int documentId)
        {
            try
            {
                return db.DocumentSentimentAnalysis.FirstOrDefault(x => x.DocumentId == documentId);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IEnumerable<DocumentSentimentAnalysis> GetAllDocumentSentimentAnalysis()
        {
            try
            {
                return db.DocumentSentimentAnalysis;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion
        
    }
}
