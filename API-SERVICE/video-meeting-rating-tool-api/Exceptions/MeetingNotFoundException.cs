﻿namespace video_meeting_rating_tool_api.Exceptions
{
    public class MeetingNotFoundException : Exception
    {
        public MeetingNotFoundException() { }
        public MeetingNotFoundException(string message) : base(message) { }
        public MeetingNotFoundException(string message, Exception innerException) : base(message, innerException) { }
    }
}
