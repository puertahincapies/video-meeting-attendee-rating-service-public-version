﻿using Dapper;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using video_meeting_rating_tool_api.Models;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace video_meeting_rating_tool_automation.Helpers
{
    public class DbHelper
    {
        private SqlConnection Connection;

        private readonly string CONNECTION_STRING = "Server=(LocalDb)\\MSSQLLocalDB; Database=CRS; Integrated Security=True";

        public User AddUser(User user)
        {
            User? item = null;
            Connection = new SqlConnection(CONNECTION_STRING);
            try
            {
                using (Connection)
                {
                    if (Connection == null || Connection.State == ConnectionState.Closed)
                    {
                        Connection.Open();
                    }

                    // add user
                    using (SqlCommand command = new SqlCommand(Constants.AddUser(user), Connection))
                    {
                        command.ExecuteNonQuery();
                    }

                    // get user
                    using (SqlCommand command = new SqlCommand(Constants.GetLastUser(), Connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            var parser = reader.GetRowParser<User>(typeof(User));
                            while (reader.Read())
                            {
                                item = parser(reader);
                            }

                            return item;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
