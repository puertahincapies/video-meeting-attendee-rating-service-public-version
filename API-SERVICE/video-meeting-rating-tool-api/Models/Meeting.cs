﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using video_meeting_rating_tool_api.Models.Interfaces;

namespace video_meeting_rating_tool_api.Models
{
    [Table("Meetings")]
    public class Meeting : IMeeting
    {
        [Key]
        public int MeetingId { get; set; }
        public string? MeetingUUID { get; set; }
        public string MeetingName { get; set; }
        public DateTime MeetingDateTime { get; set; }
        public int UserId { get; set; }
        public string? MeetingAttendeeEmails { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();
        public DateTime CreateDateTime { get; set; } = DateTime.Now;
    }
}
