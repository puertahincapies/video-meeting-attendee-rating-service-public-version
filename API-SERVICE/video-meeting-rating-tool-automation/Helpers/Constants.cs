﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using video_meeting_rating_tool_api.Models;

namespace video_meeting_rating_tool_automation.Helpers
{
    public class Constants
    {
        public static string AddUser(User user) => $@"INSERT INTO MeetingQuest.dbo.Users (Email, Password, Name, VirtualMeetingProvider, ClientKey, SecretKey, UUID, CreateDateTime) 
                                                        VALUES ('{user.Email}', '{user.Password}', '{user.Name}', '{user.VirtualMeetingProvider}', '{user.ClientKey}', '{user.SecretKey}', '{user.UUID}', '{user.CreateDateTime}')";
        public static string GetLastUser() => "SELECT TOP 1 * FROM MeetingQuest.dbo.Users ORDER BY 1 DESC";
    }
}
