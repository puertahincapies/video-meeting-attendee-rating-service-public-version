﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using RestSharp;
using System;
using video_meeting_rating_tool_api.Models;
using System.Text;
using Microsoft.AspNetCore.DataProtection;

namespace video_meeting_rating_tool_api.Helpers
{
    public class ZoomCloudClient
    {
        public static ZoomAuthentication GetAccessToken(string clientKey, string secretKey)
        {
            try
            {
                //var client = new RestClient("https://zoom.us/oauth/token");
                //var request = new RestRequest("", Method.Post);
                ////var token = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{clientKey}:{secretKey}"));
                //var token = Convert.ToBase64String(Encoding.UTF8.GetBytes("9NF3dh8WRCeRqgTEm_gWNA:qiKACQmRCEVD79ACEH5wAbh6mAcd7z8y"));
                //request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                //request.AddHeader("Accept", "application/json");
                //request.AddHeader("Authorization", $"Basic {token}");
                //request.AddParameter("grant_type", "authorization_code");
                //request.AddParameter("code", "");
                //request.AddParameter("redirect_uri", "https://oauth.pstmn.io/v1/callback");
                //var response = client.Execute(request);

                var client = new RestClient("https://zoom.us/oauth/token");
                var request = new RestRequest("", Method.Post);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddParameter("application/x-www-form-urlencoded", "grant_type=client_credentials&scope=all&client_id=" + clientKey + "&client_secret=" + secretKey, ParameterType.RequestBody);
                RestResponse response = client.Execute(request);
                return JsonConvert.DeserializeObject<ZoomAuthentication>(response?.Content);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static void GetUsers(string clientKey, string secretKey, string user)
        {
            try
            {
                var token = GetAccessToken(clientKey, secretKey);
                var client = new RestClient($"https://api.zoom.us/v2/users/{user}");
                var request = new RestRequest("", Method.Get);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Authorization", $"Bearer {token}");
                var response = client.Execute(request);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static string GetMeetingTranscript(string clientKey, string secretKey, string meetingId = "")
        {
            try
            {
                var zoomAuthentication = GetAccessToken(clientKey, secretKey);
                var token = zoomAuthentication.Access_Token;
                meetingId = "5306963030";
                var client = new RestClient($"https://api.zoom.us/v2/meetings/{meetingId}/recordings");
                var request = new RestRequest("", Method.Get);
                request.AddHeader("Authorization", $"Bearer {token}");
                request.AddHeader("cache-control", "no-cache");
                var response = client.Execute(request);
                return null;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
