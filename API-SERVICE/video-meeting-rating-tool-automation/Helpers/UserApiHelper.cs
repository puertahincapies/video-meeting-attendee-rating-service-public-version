﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using video_meeting_rating_tool_api.Models;

namespace video_meeting_rating_tool_automation.Helpers
{
    public class UserApiHelper
    {
        private readonly Settings _settings;

        private const string path = "/api/v1/meetingquest/User";

        private string login = $"{path}/Login";
        private string getUsers = $"{path}/GetUsers";
        private string getUserById = $"{path}/GetUser/{0}";
        private string signUp = $"{path}/Signup";

        public UserApiHelper(Settings settings)
        {
            _settings = settings;
        }

        public User Login(string token)
        {
            return null;
        }

        public List<User> GetUsers()
        {
            return null;
        }

        public User GetUserById(int userId)
        {
            return null;
        }

        public User Signup(User user)
        {
            return null;
        }
    }
}
