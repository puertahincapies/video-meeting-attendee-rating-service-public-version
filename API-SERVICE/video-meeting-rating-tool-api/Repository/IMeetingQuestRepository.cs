﻿using video_meeting_rating_tool_api.Enums;
using video_meeting_rating_tool_api.Models;
using video_meeting_rating_tool_api.Models.Dtos.DocumentDto;
using video_meeting_rating_tool_api.Models.Interfaces;

namespace video_meeting_rating_tool_api.Repository
{
    public interface IMeetingQuestRepository
    {
        #region "User"        
        //public Report GetReports(int userId);
        public IEnumerable<User> GetUsers();
        public User GetUser(int userId);
        public User GetUserByEmail(string email);
        public User UserLogin(string email, string password);
        public User SignUp(User user);
        public User ChangeUser(int userId, string newUserName, string newPassword, string virtualMeetingProvider, string newClientKey, string newSecretKey);
        
        void SaveChanges();
        #endregion

        #region "Document"
        public Document GetDocument(int documentId);
        public Document GetDocumentByMeetingId(int meetingId);
        public IEnumerable<Document> GetAllDocuments();
        public Document AddDocument(Document document);
        public Document UpdateDocument(Document document);
        public void DeleteDocument(int documentId);
        public int DeleteAllDocuments();

        #endregion

        #region "Meeting"
        public Meeting AddMeeting(Meeting meeting);
        public Meeting GetMeeting(int meetingId);
        public IEnumerable<Meeting> GetMeetingsByUser(int userId);
        public IEnumerable<Meeting> GetMeetings();

        #endregion

        #region "Quiz"
        public Quiz SaveQuiz(Quiz quiz);
        public Quiz GetQuiz(int quizId);
        public Quiz GetQuizByMeetingId(int meetingId);
        public List<int> GetQuizIdsByUserId(int userId);
        public void UpdateQuiz(Quiz updateQuiz);
        public IEnumerable<Quiz> GetAllQuizzes();
        public IEnumerable<Quiz> GetAllQuizzesByHostUser(int userId);
        #endregion

        #region "QuizResult"
        public QuizResult GetQuizResult(int quizId, int userId);
        public QuizResult AddQuizResult(QuizResult quizResult);
        public List<QuizResult> GetQuizResultsByUser(int userId);
        public IEnumerable<QuizResult> GetQuizResultsByMeeting(int meetingId);
        #endregion       

        #region "Errors"
        public void LogError(Exception exception, ErrorTypes errorType);
        public void LogUIError(Error error);
        #endregion

        #region "QuizAccessTokens"
        public QuizAccessToken AddAccessToken(QuizAccessToken quizAccessToken);
        public QuizAccessToken GetAccessToken(int userId, int quizId, int meetingId);
        public IEnumerable<QuizAccessToken> GetAccessTokens();
        public QuizAccessToken ValidateAccessToken(string token);
        public void UpdateAccessToken(QuizAccessToken quizAccessToken);
        #endregion

        #region "UserQuizzes"
        public IEnumerable<UserQuizzes> GetUserQuizzes(int userId);
        public void AddUserQuiz(UserQuizzes userQuiz);
        #endregion

        #region "MeetingAttendeeEmails"
        public void AddMeetingAttendeeEmail(MeetingAttendeeEmail emailEntry);
        public MeetingAttendeeEmail GetMeetingAttendeeEmail(string email, int userId);
        public IEnumerable<MeetingAttendeeEmail> GetMeetingAttendeeEmails(int userId);
        #endregion

        #region "DocumentSentimentAnalysis"
        public void AddDocumentSentimentAnalysis(DocumentSentimentAnalysis documentSentimentAnalysis);
        public DocumentSentimentAnalysis GetDocumentSentimentAnalysis(int documentId);
        public IEnumerable<DocumentSentimentAnalysis> GetAllDocumentSentimentAnalysis();
        #endregion
    }
}
