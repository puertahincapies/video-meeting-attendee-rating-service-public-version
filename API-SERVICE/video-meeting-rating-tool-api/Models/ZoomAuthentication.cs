﻿namespace video_meeting_rating_tool_api.Models
{
    public class ZoomAuthentication
    {
        public string Access_Token { get; set; }
        public string Token_Type { get; set; }
        public string Expires_In { get; set; }
        public string Scope { get; set; }
    }
}
