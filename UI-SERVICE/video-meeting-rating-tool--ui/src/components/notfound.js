import '../css/notfound.css';

const NotFound = () => {
    return ( 
        <div className="not-found-component">
            <h1 className="not-found-message">Page Not Found</h1>
        </div>
     );
}
 
export default NotFound;