﻿namespace video_meeting_rating_tool_api.Models
{
    public class SendEmail
    {
        public int QuizId { get; set; } 
        public int UserId { get; set; }
        public int MeetingId { get; set; }
    }
}
