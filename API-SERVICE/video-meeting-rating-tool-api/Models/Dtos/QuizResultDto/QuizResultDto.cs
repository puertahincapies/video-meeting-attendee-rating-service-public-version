﻿namespace video_meeting_rating_tool_api.Models.Dtos.QuizResultDto
{
    public class QuizResultDto
    {
        public int QuizResultId { get; set; }
        public int QuizId { get; set; }
        public int MeetingId { get; set; }
        public int UserId { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }
        public string Question1Response { get; set; }
        public string Question2Response { get; set; }
        public string Question3Response { get; set; }
        public string Question4Response { get; set; }
        public string Question5Response { get; set; }
        public int Score { get; set; }
        public string Status { get; set; }
        public string UUID { get; set; }
        public DateTime CreateDateTime { get; set; }
    }
}
