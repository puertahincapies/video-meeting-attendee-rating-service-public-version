﻿using System;
using System.Net.Http.Headers;
using System.Net;
using System.Text;

namespace video_meeting_rating_tool_automation.Helpers.ApiHelpers
{
    public class HttpBase
    {
        protected HttpClient HttpClient(string baseUri, ICredentials? credentials = null)
        {
            var handler = new HttpClientHandler();

            if (credentials != null)
            {
                handler.Credentials = credentials;
            }

            var client = new HttpClient(handler);
            client.BaseAddress = new Uri(baseUri);
            client.Timeout = TimeSpan.FromSeconds(30);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            return client;
        }
        protected HttpRequestMessage HttpRequest(string requestUri,
                                                 HttpMethod method,
                                                 string contentType,
                                                 string? cookie = null,
                                                 string? authenticationType = null,
                                                 string? authenticationString = null,
                                                 string? token = null)
        {
            var request = new HttpRequestMessage(method, requestUri);
            request.Headers.Add("Accept", contentType);
            request.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            request.Headers.Add("Accept-Language", "en-US,en;q=0.9");
            request.Headers.Add("Connection", "keep-alive");

            switch (authenticationType)
            {
                case "Basic":
                    var encodedAuthenticationString = Convert.ToBase64String(Encoding.ASCII.GetBytes(authenticationString));
                    request.Headers.Authorization = new AuthenticationHeaderValue(authenticationType, encodedAuthenticationString);
                    break;
                case "Bearer":
                    request.Headers.Authorization = new AuthenticationHeaderValue(authenticationType, token);
                    break;
            }
            return request;
        }
        public HttpResponseMessage HttpGet(string baseUri,
                                            string requestUri,
                                            string contentType,
                                            ICredentials? credentials = null,
                                            string? cookie = null,
                                            string? authenticationType = null,
                                            string? authenticationString = null,
                                            string? token = null)
        {
            var client = HttpClient(baseUri, credentials);
            var request = HttpRequest(requestUri, HttpMethod.Get, contentType, cookie, authenticationType, authenticationString, token);

            try
            {
                return client.Send(request); ;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public async Task<HttpResponseMessage> HttpGetAsync(string baseUri,
                                                            string requestUri,
                                                            string contentType,
                                                            ICredentials? credentials = null,
                                                            string? cookie = null,
                                                            string? authenticationType = null,
                                                            string? authenticationString = null,
                                                            string? token = null)
        {
            var client = HttpClient(baseUri, credentials);
            var request = HttpRequest(requestUri, HttpMethod.Get, contentType, cookie, authenticationType, authenticationString, token);

            HttpResponseMessage? response = null;
            try
            {
                response = await client.SendAsync(request);
                return response;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public HttpResponseMessage HttpPost(string baseUri,
                                            string requestUri,
                                            string contentType,
                                            ICredentials? credentials = null,
                                            HttpContent? content = null,
                                            string? cookie = null,
                                            string? authenticationType = null,
                                            string? authenticationString = null,
                                            string? token = null)
        {
            var client = HttpClient(baseUri, credentials);
            var request = HttpRequest(requestUri, HttpMethod.Post, contentType, cookie, authenticationType, authenticationString, token);

            if (content != null)
            {
                request.Content = content;
            }

            try
            {
                return client.Send(request); ;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public async Task<HttpResponseMessage> HttpPostAsync(string baseUri,
                                                                string requestUri,
                                                                string contentType,
                                                                ICredentials? credentials = null,
                                                                HttpContent? content = null,
                                                                string? cookie = null,
                                                                string? authenticationType = null,
                                                                string? authenticationString = null,
                                                                string? token = null)
        {
            var client = HttpClient(baseUri, credentials);
            var request = HttpRequest(requestUri, HttpMethod.Post, contentType, cookie, authenticationType, authenticationString, token);

            if (content != null)
            {
                request.Content = content;
            }

            try
            {
                return await client.SendAsync(request);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public HttpResponseMessage HttpPut(string baseUri,
                                            string requestUri,
                                            string contentType,
                                            ICredentials? credentials = null,
                                            HttpContent? content = null,
                                            string? cookie = null,
                                            string? authenticationType = null,
                                            string? authenticationString = null,
                                            string? token = null)
        {
            var client = HttpClient(baseUri, credentials);
            var request = HttpRequest(requestUri, HttpMethod.Put, contentType, cookie, authenticationType, authenticationString, token);

            if (content != null)
            {
                request.Content = content;
            }

            try
            {
                return client.Send(request); ;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public async Task<HttpResponseMessage> HttpPutAsync(string baseUri,
                                                            string requestUri,
                                                            string contentType,
                                                            ICredentials? credentials = null,
                                                            HttpContent? content = null,
                                                            string? cookie = null,
                                                            string? authenticationType = null,
                                                            string? authenticationString = null,
                                                            string? token = null)
        {
            var client = HttpClient(baseUri, credentials);
            var request = HttpRequest(requestUri, HttpMethod.Put, contentType, cookie, authenticationType, authenticationString, token);

            if (content != null)
            {
                request.Content = content;
            }

            try
            {
                return await client.SendAsync(request); ;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public HttpResponseMessage HttpDelete(string baseUri,
                                                string requestUri,
                                                string contentType,
                                                ICredentials? credentials = null,
                                                string? cookie = null,
                                                string? authenticationType = null,
                                                string? authenticationString = null,
                                                string? token = null)
        {
            var client = HttpClient(baseUri, credentials);
            var request = HttpRequest(requestUri, HttpMethod.Delete, contentType, cookie, authenticationType, authenticationString, token);

            try
            {
                return client.Send(request); ;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public async Task<HttpResponseMessage> HttpDeleteAsync(string baseUri,
                                                                string requestUri,
                                                                string contentType,
                                                                ICredentials? credentials = null,
                                                                string? cookie = null,
                                                                string? authenticationType = null,
                                                                string? authenticationString = null,
                                                                string? token = null)
        {
            var client = HttpClient(baseUri, credentials);
            var request = HttpRequest(requestUri, HttpMethod.Delete, contentType, cookie, authenticationType, authenticationString, token);

            try
            {
                return await client.SendAsync(request); ;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
