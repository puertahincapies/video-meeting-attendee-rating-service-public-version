﻿namespace video_meeting_rating_tool_api.Enums
{
    public enum RoleTypes
    {
        None = 0,
        Presenter = 1,
        Listener = 2
    }
}
