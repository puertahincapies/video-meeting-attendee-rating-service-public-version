﻿using Amazon.CloudWatchLogs;
using Amazon.CloudWatchLogs.Model;
using Amazon.Runtime;
using video_meeting_rating_tool_api.Models;

namespace video_meeting_rating_tool_api.Helpers
{
    public class AwsCloudWatchClient
    {
        private readonly IConfiguration _configuration;

        public AwsCloudWatchClient(IConfiguration configuration) 
        {
            _configuration = configuration;
        }

        public async Task PublishLogsToAwsCloudWatch(List<string> logs, AwsCredentials credentials)
        {
            try
            {
                var logGroupName = _configuration["AwsCloudWatchLogGroupName"];
                var basicAwsCredentials = new BasicAWSCredentials(_configuration["AwsAccessKey"], _configuration["AwsSecretKey"]);
                var config = new AmazonCloudWatchLogsConfig()
                {
                    RegionEndpoint = Amazon.RegionEndpoint.USEast1
                };

                using var cloudWatchClient = new AmazonCloudWatchLogsClient(basicAwsCredentials, config);

                var logStream = Guid.NewGuid().ToString();

                // check for existing group
                var existing = await cloudWatchClient.DescribeLogGroupsAsync(new DescribeLogGroupsRequest() { LogGroupNamePrefix = logGroupName });
                var logGroupExists = existing.LogGroups.Any(x => x.LogGroupName == logGroupName);
                if (!logGroupExists)
                {
                    await cloudWatchClient.CreateLogGroupAsync(new CreateLogGroupRequest(logGroupName));
                }
                await cloudWatchClient.CreateLogStreamAsync(new CreateLogStreamRequest(logGroupName, logStream));

                var events = new List<InputLogEvent>();
                foreach (var log in logs)
                {
                    events.Add(new InputLogEvent() { Message = log, Timestamp = DateTime.UtcNow });
                }

                await cloudWatchClient.PutLogEventsAsync(new PutLogEventsRequest()
                {
                    LogGroupName = logGroupName,
                    LogStreamName = logStream,
                    LogEvents = events
                });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task PublishLogToAwsCloudWatch(string log)
        {
            try
            {
                var logGroupName = _configuration["AwsCloudWatchLogGroupName"];
                var basicAwsCredentials = new BasicAWSCredentials(_configuration["AwsAccessKey"], _configuration["AwsSecretKey"]);
                var config = new AmazonCloudWatchLogsConfig()
                {
                    RegionEndpoint = Amazon.RegionEndpoint.USEast1
                };
                using var cloudWatchClient = new AmazonCloudWatchLogsClient(basicAwsCredentials, config);

                var logStream = Guid.NewGuid().ToString();

                // check for existing group
                var existing = await cloudWatchClient.DescribeLogGroupsAsync(new DescribeLogGroupsRequest() { LogGroupNamePrefix = logGroupName });
                var logGroupExists = existing.LogGroups.Any(x => x.LogGroupName == logGroupName);
                if (!logGroupExists)
                {
                    await cloudWatchClient.CreateLogGroupAsync(new CreateLogGroupRequest(logGroupName));
                }
                await cloudWatchClient.CreateLogStreamAsync(new CreateLogStreamRequest(logGroupName, logStream));
                await cloudWatchClient.PutLogEventsAsync(new PutLogEventsRequest()
                {
                    LogGroupName = logGroupName,
                    LogStreamName = logStream,
                    LogEvents = new List<InputLogEvent>()
                {
                    new InputLogEvent()
                    {
                        Message = log,
                        Timestamp = DateTime.UtcNow
                    }
                }
                });
            }
            catch(Exception ex)
            {
                throw;
            }
        }
    }
}
