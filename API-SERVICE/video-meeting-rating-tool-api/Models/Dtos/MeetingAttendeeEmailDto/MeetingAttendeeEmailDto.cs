﻿namespace video_meeting_rating_tool_api.Models.Dtos.MeetingAttendeeEmailDto
{
    public class MeetingAttendeeEmailDto
    {
        public int MeetingAttendeeEmailId { get; set; }
        public int UserId { get; set; }
        public string AttendeeEmail { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();
        public DateTime CreateDateTime { get; set; } = DateTime.Now;
    }
}
