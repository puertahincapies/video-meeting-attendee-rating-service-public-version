﻿namespace video_meeting_rating_tool_api.Models
{
    public class S3Response
    {
        public int StatusCode { get; set; }
        public string Message { get; set; } = string.Empty;
    } 
}
