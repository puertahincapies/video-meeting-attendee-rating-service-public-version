﻿namespace video_meeting_rating_tool_api.Exceptions
{
    public class InconsistentQuizResultException : Exception
    {
        public InconsistentQuizResultException() { }
        public InconsistentQuizResultException(string message) : base(message) { }
        public InconsistentQuizResultException(string message, Exception innerException) : base(message, innerException) { }
    }
}
