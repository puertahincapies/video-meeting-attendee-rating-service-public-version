﻿using System.Net.Http.Json;
using video_meeting_rating_tool_api.Models;
using video_meeting_rating_tool_api.Models.Dtos.MeetingDto;
using video_meeting_rating_tool_automation.Helpers.ApiHelpers;

namespace video_meeting_rating_tool_automation.Helpers
{
    public class MeetingApiHelper : HttpBase
    {
        private readonly Settings _settings;

        private const string path = "/api/v1/meetingquest/Meeting";

        private string saveMeetingDetails = $"{path}/SaveMeetingDetails";
        private string getMeeting = $"{path}/GetMeeting/{0}";
        private string getMeetings = $"{path}/GetMeetings";
        private string getAttendeeEmails = $"{path}/GetAttendeeEmails/{0}";

        public MeetingApiHelper(Settings settings)
        {
            _settings = settings;
        }

        public MeetingDto SaveMeetingDetails(AddMeetingDto meeting)
        {
            var jsonContent = JsonContent.Create(meeting);
            var response = HttpPostAsync(baseUri: _settings.WebApiBaseUri, requestUri: saveMeetingDetails, contentType: "application/json",content: jsonContent);
            var result = response.Result.Content.ReadFromJsonAsync<MeetingDto>();
            return result.Result;
        }

        public Meeting GetMeeting(int meetingId)
        {
            return null;
        }

        public List<Meeting> GetMeetings()
        {
            return null;
        }

        public List<string> GetAttendeeEmails(int userId)
        {
            return null;
        }
    }
}
