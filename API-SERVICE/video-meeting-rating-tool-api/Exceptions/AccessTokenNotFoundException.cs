﻿namespace video_meeting_rating_tool_api.Exceptions
{
    public class AccessTokenNotFoundException : Exception
    {
        public AccessTokenNotFoundException() { }
        public AccessTokenNotFoundException(string message) : base(message) { }
        public AccessTokenNotFoundException(string message, Exception innerException) : base(message, innerException) { }
    }
}
