﻿using System.Text.Json.Serialization;

namespace video_meeting_rating_tool_api.Models.Dtos.UserDto
{
    public class UserDto
    {
        public int UserId { get; set; }
        public string Email { get; set; }        
        public string Password { get; set; }
        public string Name { get; set; }
        public string? VirtualMeetingProvider { get; set; } = string.Empty;
        
        public string? ClientKey { get; set; } = string.Empty;

        public string? SecretKey { get; set; } = string.Empty;
        [JsonIgnore]
        public string UUID { get; set; } = Guid.NewGuid().ToString();
        [JsonIgnore]
        public DateTime CreateDateTime { get; set; } = DateTime.Now;
    }
}
