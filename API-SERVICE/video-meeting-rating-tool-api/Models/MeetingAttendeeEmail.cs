﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace video_meeting_rating_tool_api.Models
{
    [Table("MeetingAttendeeEmails")]
    public class MeetingAttendeeEmail
    {
        [Key]
        public int MeetingAttendeeEmailId { get; set; }
        public int UserId { get; set; }
        public string AttendeeEmail { get; set; }
        public string UUID { get; set; } = Guid.NewGuid().ToString();
        public DateTime CreateDateTime { get; set; } = DateTime.Now;
    }
}
