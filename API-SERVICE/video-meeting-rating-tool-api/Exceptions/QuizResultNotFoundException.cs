﻿namespace video_meeting_rating_tool_api.Exceptions
{
    public class QuizResultNotFoundException : Exception
    {
        public QuizResultNotFoundException() { }
        public QuizResultNotFoundException(string message) : base(message) { }
        public QuizResultNotFoundException(string message, Exception innerException) : base(message, innerException) { }
    }
}
