﻿using Newtonsoft.Json;
using RestSharp;
using video_meeting_rating_tool_api.Exceptions;
using video_meeting_rating_tool_api.Models;

namespace video_meeting_rating_tool_api.Helpers
{
    public class ChatGPTClient
    {
        //private ChatGPTResponse SendPromptToChatGptUsingHttpClient(string prompt, string uri, string token)
        //{
        //    try
        //    {
        //        var httpBase = new HttpBase();
        //        var _uri = new Uri(uri);
        //        var requestBody = new ChatGPTRequest()
        //        {
        //            Messages = new List<Messages>()
        //            {
        //                new Messages()
        //                {
        //                    Role = "user",
        //                    Content = prompt
        //                }
        //            },
        //            Model = "gpt-3.5-turbo"
        //        };
        //        var jsonContent = JsonContent.Create(requestBody);

        //        var response = httpBase.HttpPostAsync(
        //            baseUri: $"{_uri.Scheme}://{_uri.Host}", 
        //            requestUri:_uri.AbsolutePath, 
        //            contentType: "application/json", 
        //            content: jsonContent, 
        //            authenticationType: "Bearer", 
        //            token: token).Result;

        //        var result = response.Content.ReadAsStringAsync().Result;
        //        return JsonConvert.DeserializeObject<ChatGPTResponse>(result);
        //    }
        //    catch(Exception ex)
        //    {
        //        _repository.LogError(ex, Enums.ErrorTypes.Error);
        //        _logger.LogError(ex.Message);
        //        throw;
        //    }
        //}

        public static ChatGPTResponse SendPromptToChatGPTUsingRestClient(string prompt, string uri, string token)
        {
            try
            {
                var client = new RestClient(uri);
                var request = new RestRequest("", Method.Post);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Authorization", $"Bearer {token}");

                var requestBody = new
                {
                    messages = new[] {
                        new
                        {
                            role = "user",
                            content = prompt
                        }
                },
                    model = "gpt-3.5-turbo"
                };

                request.AddJsonBody(JsonConvert.SerializeObject(requestBody));
                var response = client.Execute(request);

                ChatGPTResponse jsonResponse = null;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    jsonResponse = JsonConvert.DeserializeObject<ChatGPTResponse>(response.Content ?? string.Empty);
                }

                return jsonResponse;
            }
            catch(Exception ex)
            {
                throw;
            }
        }
    }
}
