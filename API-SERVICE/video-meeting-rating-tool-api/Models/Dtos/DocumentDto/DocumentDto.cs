﻿using System.Text.Json.Serialization;

namespace video_meeting_rating_tool_api.Models.Dtos.DocumentDto
{
    public class DocumentDto
    {
        public int DocumentId { get; set; }
        public int MeetingId { get; set; }
        public string MeetingUUID { get; set; }
        public string Name { get; set; }
        [JsonIgnore]
        public byte[] Content { get; set; }
        public string UUID { get; set; } 
        public DateTime CreateDateTime { get; set; } 
    }
}
