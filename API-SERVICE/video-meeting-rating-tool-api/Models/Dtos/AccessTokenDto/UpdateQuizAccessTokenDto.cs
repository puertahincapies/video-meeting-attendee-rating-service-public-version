﻿namespace video_meeting_rating_tool_api.Models.Dtos.AccessTokenDto
{
    public class UpdateQuizAccessTokenDto
    {
        public int QuizId { get; set; } 
        public int MeetingId { get; set; }
        public int UserId { get; set; }
    }
}
