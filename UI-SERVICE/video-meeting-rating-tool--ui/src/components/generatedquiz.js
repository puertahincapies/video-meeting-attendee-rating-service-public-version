import '../css/generatedquiz.css';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import toast, { Toaster } from 'react-hot-toast';
import { useLocation } from 'react-router-dom';
import Cookies from 'universal-cookie';
import {generateGuid} from '../helpers/utilities';
import {USERSESSION,USERLOGIN,GETQUIZURL,NAVTOUNAUTHORIZE,SUBMITQUIZURL,DOWNLOADQUIZURL,EMAILQUIZURL} from '../helpers/constants';

const GeneratedQuiz = ({userSession}) => {
    const [quiz, setQuiz] = useState([]);
    const [quizSelection, setQuizSelection] = useState(null);
    const [quizResult, setQuizResult] = useState(null);
    const [isSubmitted, setIsSubmitted] = useState(false);
    const location = useLocation();
    const cookies = new Cookies();
    const [currentUserId, setCurrentUserId] = useState(0);
    const navigate = useNavigate();

    useEffect(() => {
        let user = userSession;
        // console.log("generatedquiz > user: " + JSON.stringify(user));
        if(user == null || user== undefined) {
            user = cookies.get(USERSESSION);
            // console.log("generatedquiz > set user using cookie session: " + JSON.stringify(user));
            if(user != null || user != undefined){
                setCurrentUserId(user.userId);
            }
        }
        else {
            setCurrentUserId(user.userId);
        }

        if(user != null || user != undefined) {
            fetch(GETQUIZURL + location.state.quizId, {
                method: "GET",
                headers: { 'content-type': 'application/json'},
            })
            .then(res => {
                if(!res.ok) {
                    throw Error("Could not retrieve data from resource");
                }
                return res.json();
            })
            .then(data => {
                // console.log(data); 
                setQuiz(data);
            })
            .catch(err => {
                console.log(err);
                toast.error("There was an error!", {
                    duration: 3000,
                    position: 'bottom-right'
                })
            })
        }
        else {
            // check for when it's a guest user
            if(location.state != undefined && location.state != null) {
                if(location.state.isGuest) {
                    setCurrentUserId(location.state.userId);

                    fetch(GETQUIZURL + location.state.quizId, {
                        method: "GET",
                        headers: { 'content-type': 'application/json'},
                    })
                    .then(res => {
                        if(!res.ok) {
                            throw Error("Could not retrieve data from resource");
                        }
                        return res.json();
                    })
                    .then(data => {
                        // console.log(data); 
                        setQuiz(data);
                    })
                    .catch(err => {
                        console.log(err);
                        toast.error("There was an error!", {
                            duration: 3000,
                            position: 'bottom-right'
                        })
                    })
                }
            }
            else {
                navigate(NAVTOUNAUTHORIZE)
            }
        }
    }, []);

    const onSubmitQuiz = (e) => {
        console.log("On click submit quiz");
        e.preventDefault();

        let selection = []

        for(let i = 1; i <= 5; i++) {
            var quizselection = document.getElementsByName(`quizselection${i}`);
            quizselection.forEach(e => {
                if (e.checked){
                    selection.push(e.value)
                }
            })
        }

        // user w/ account vs. guest user
        if(location.state.isGuest) {
            setCurrentUserId(location.state.userId);
        }

        let result = {
            quizId: quiz.quizId,
            meetingId: quiz.meetingId,
            userId: currentUserId,
            question1Response: selection[0],
            question2Response: selection[1],
            question3Response: selection[2],
            question4Response: selection[3],
            question5Response: selection[4],
            isGuest: location.state.isGuest
        };
        setQuizSelection(result);

        // make request to submit quiz
        fetch(SUBMITQUIZURL, {
            method: "POST",
            headers: { 'content-type': 'application/json'},
            body: JSON.stringify(result)
        })
        .then(res => {
            if(!res.ok) {
                throw Error("Could not retrieve data from resource");
            }
            return res.json();
        })
        .then(data => {
            // console.log(data); 
            setQuizResult(data);
            if(data.status == "Submitted") {
                setIsSubmitted(true);
            }
            toast.success("Quiz was successfully submitted", {
                duration: 3000,
                position: 'bottom-right'
            })
        })
        .catch(err => {
            console.log(err);
            toast.error("There was an error!", {
                duration: 3000,
                position: 'bottom-right'
            })
        })
    }

    const OnClickDownloadQuiz = (e) => {
        console.log("On click download quiz");
        fetch(DOWNLOADQUIZURL + quiz.meetingId + "/" + quiz.quizId, {
            method: "GET"
        })
        .then(res => {
            if(!res.ok) {
                throw Error("Could not retrieve data from resource");
            }
            return res.blob();
        })
        .then(data => {
            // console.log(data); 
            const url = window.URL.createObjectURL(new Blob([data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', `MQ_Quiz_${new Date().toLocaleDateString() + "_" + generateGuid()}.pdf`);
            document.body.appendChild(link);
            link.click();
            link.parentNode.removeChild(link);

            toast.success("Quiz was successfully downloaded", {
                duration: 3000,
                position: 'bottom-right'
            })
        })
        .catch(err => {
            console.log(err);
            toast.error("There was an error!", {
                duration: 3000,
                position: 'bottom-right'
            })
        })
    }

    const onClickEmailQuiz = (e) => {
        console.log("On click email quiz");
        let body = {
            meetingId: quiz.meetingId,
            quizId: quiz.quizId,
            userId: quiz.userId     // who it's from -- host user
        };
        fetch(EMAILQUIZURL, {
            method: "POST",
            headers: { 'content-type': 'application/json'},
            body: JSON.stringify(body)
        })
        .then(res => {
            if(!res.ok) {
                throw Error("Could not retrieve data from resource");
            }
            return res;
        })
        .then(data => {
            // console.log(data); 
            toast.success("Quiz was successfully emailed to meeting attendees", {
                duration: 3000,
                position: 'bottom-right'
            })
        })
        .catch(err => {
            console.log(err);
            toast.error("There was an error!", {
                duration: 3000,
                position: 'bottom-right'
            })
        })
    }

    return ( 
        <div className="generatedquiz-component">
            {currentUserId != 0 && quiz != null && <form className="generate-quiz-form" onSubmit={onSubmitQuiz}>
                <section className="multiple-choice-container">
                    <h3>{quiz.question1}</h3><br />
                    <div className="form-check">
                        <input type="radio" className="form-check-input" id="radio1" name="quizselection1" value={quiz.question1Answer1} disabled={isSubmitted} required/>
                        <label className="form-check-label" htmlFor="radio1">{quiz.question1Answer1}</label>
                    </div>
                    <div className="form-check">
                        <input type="radio" className="form-check-input" id="radio2" name="quizselection1" value={quiz.question1Answer2} disabled={isSubmitted} required/>
                        <label className="form-check-label" htmlFor="radio2">{quiz.question1Answer2}</label>
                    </div>
                    <div className="form-check">
                        <input type="radio" className="form-check-input" id="radio3" name="quizselection1" value={quiz.question1Answer3} disabled={isSubmitted} required/>
                        <label className="form-check-label" htmlFor="radio3">{quiz.question1Answer3}</label>
                    </div>
                </section><br />
                <section className="multiple-choice-container">
                    <h3>{quiz.question2}</h3><br />
                    <div className="form-check">
                        <input type="radio" className="form-check-input" id="radio1" name="quizselection2" value={quiz.question2Answer1} disabled={isSubmitted} required/>
                        <label className="form-check-label" htmlFor="radio1">{quiz.question2Answer1}</label>
                    </div>
                    <div className="form-check">
                        <input type="radio" className="form-check-input" id="radio2" name="quizselection2" value={quiz.question2Answer2} disabled={isSubmitted} required/>
                        <label className="form-check-label" htmlFor="radio2">{quiz.question2Answer2}</label>
                    </div>
                    <div className="form-check">
                        <input type="radio" className="form-check-input" id="radio3" name="quizselection2" value={quiz.question2Answer3} disabled={isSubmitted} required/>
                        <label className="form-check-label" htmlFor="radio3">{quiz.question2Answer3}</label>
                    </div>
                </section><br />
                <section className="multiple-choice-container">
                    <h3>{quiz.question3}</h3><br />
                    <div className="form-check">
                        <input type="radio" className="form-check-input" id="radio1" name="quizselection3" value={quiz.question3Answer1} disabled={isSubmitted} required/>
                        <label className="form-check-label" htmlFor="radio1">{quiz.question3Answer1}</label>
                    </div>
                    <div className="form-check">
                        <input type="radio" className="form-check-input" id="radio2" name="quizselection3" value={quiz.question3Answer2} disabled={isSubmitted} required/>
                        <label className="form-check-label" htmlFor="radio2">{quiz.question3Answer2}</label>
                    </div>
                    <div className="form-check">
                        <input type="radio" className="form-check-input" id="radio3" name="quizselection3" value={quiz.question3Answer3} disabled={isSubmitted} required/>
                        <label className="form-check-label" htmlFor="radio3">{quiz.question3Answer3}</label>
                    </div>
                </section><br />
                <section className="multiple-choice-container">
                    <h3>{quiz.question4}</h3><br />
                    <div className="form-check">
                        <input type="radio" className="form-check-input" id="radio1" name="quizselection4" value={quiz.question4Answer1} disabled={isSubmitted} required/>
                        <label className="form-check-label" htmlFor="radio1">{quiz.question4Answer1}</label>
                    </div>
                    <div className="form-check">
                        <input type="radio" className="form-check-input" id="radio2" name="quizselection4" value={quiz.question4Answer2} disabled={isSubmitted} required/>
                        <label className="form-check-label" htmlFor="radio2">{quiz.question4Answer2}</label>
                    </div>
                    <div className="form-check">
                        <input type="radio" className="form-check-input" id="radio3" name="quizselection4" value={quiz.question4Answer3} disabled={isSubmitted} required/>
                        <label className="form-check-label" htmlFor="radio3">{quiz.question4Answer3}</label>
                    </div>
                </section><br />
                <section className="multiple-choice-container">
                    <h3>{quiz.question5}</h3><br />
                    <div className="form-check">
                        <input type="radio" className="form-check-input" id="radio1" name="quizselection5" value={quiz.question5Answer1} disabled={isSubmitted} required/>
                        <label className="form-check-label" htmlFor="radio1">{quiz.question5Answer1}</label>
                    </div>
                    <div className="form-check">
                        <input type="radio" className="form-check-input" id="radio2" name="quizselection5" value={quiz.question5Answer2} disabled={isSubmitted} required/>
                        <label className="form-check-label" htmlFor="radio2">{quiz.question5Answer2}</label>
                    </div>
                    <div className="form-check">
                        <input type="radio" className="form-check-input" id="radio3" name="quizselection5" value={quiz.question5Answer3} disabled={isSubmitted} required/>
                        <label className="form-check-label" htmlFor="radio3">{quiz.question5Answer3}</label>
                    </div>
                </section><br />
                {quiz.isConsistent == 0 && <label className="alert-message">The correct answers generated by ChatGPT are inconstent, we recommend that you take the quiz as the meeting host and set the correct answers. </label>}
                <section className="generated-quiz-action-buttons">
                    {isSubmitted && <button type="button" className="download-quiz action-button" onClick={OnClickDownloadQuiz}>Download Quiz</button>}
                    {!location.state.isGuest && !location.state.isAttendee && <button type="button" className="email-quiz action-button" onClick={onClickEmailQuiz}>Email Quiz</button>}
                    <button type="submit" className="submit-quiz action-button" id="submitquiz" hidden={isSubmitted}>Submit Quiz</button>
                </section>
            </form>}
            <Toaster />
        </div>
     );
}
 
export default GeneratedQuiz;