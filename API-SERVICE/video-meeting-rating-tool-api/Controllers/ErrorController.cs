﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using video_meeting_rating_tool_api.Enums;
using video_meeting_rating_tool_api.Helpers;
using video_meeting_rating_tool_api.Models;
using video_meeting_rating_tool_api.Models.Dtos.QuizDto;
using video_meeting_rating_tool_api.Repository;

namespace video_meeting_rating_tool_api.Controllers
{
    [ApiController]
    [EnableCors("MeetingQuest_CorsPolicy")]
    [Route("api/v1/meetingquest/[controller]")]

    public class ErrorController : ControllerBase
    {
        private readonly ILogger<ErrorController> _logger;
        private readonly IMeetingQuestRepository _repository;

        // using repository pattern
        public ErrorController(ILogger<ErrorController> logger, IMeetingQuestRepository repository)
        {
            _logger = logger;
            _repository = repository;
        }

        [HttpPost("[action]")]
        public ActionResult LogUIErrors([FromBody] LogError body)
        {
            try
            {
                var error = new Error()
                {
                    ErrorMessage = body.Message,
                    ErrorType = ErrorTypes.Error.ToString(),
                };

                _repository.LogUIError(error);
                return NoContent();
            }
            catch (Exception ex)
            {
                _repository.LogError(ex, Enums.ErrorTypes.Error);
                _logger.LogError(ex.Message);
                return Problem("There was an error processing your request, we are looking into it. Sorry for the inconvenience.");
            }
        }
    }
}
