﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using video_meeting_rating_tool_api.Models.Interfaces;

namespace video_meeting_rating_tool_api.Models
{
    [Table("QuizResults")]
    public class QuizResult : IQuizResult
    {
        [Key]
        public int QuizResultId { get; set; }
        public int QuizId { get; set; }
        public int MeetingId { get; set; }
        public int UserId { get; set; }
        public string Role { get; set; } 
        public string Email { get; set; } 
        public string Question1Response { get; set; } 
        public string Question2Response { get; set; } 
        public string Question3Response { get; set; } 
        public string Question4Response { get; set; } 
        public string Question5Response { get; set; } 
        public int Score { get; set; }
        public string Status { get; set; } 
        public string UUID { get; set; } = Guid.NewGuid().ToString();
        public DateTime CreateDateTime { get; set; } = DateTime.Now;
    }
}
