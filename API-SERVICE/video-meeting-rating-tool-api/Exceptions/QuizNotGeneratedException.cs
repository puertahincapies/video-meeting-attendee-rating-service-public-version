﻿namespace video_meeting_rating_tool_api.Exceptions
{
    public class QuizNotGeneratedException : Exception
    {
        public QuizNotGeneratedException() { }
        public QuizNotGeneratedException(string message) : base(message) { }
        public QuizNotGeneratedException(string message, Exception innerException) : base(message, innerException) { }
    }
}
